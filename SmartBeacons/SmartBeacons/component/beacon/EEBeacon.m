//
//  EEBeacon.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-1.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEBeacon.h"

static NSString * const kUUID = @"00000888-8800-0008-8888-000008888800";
static NSString * const kIdentifier = @"ShiSource";
static NSString * const kCellIdentifier = @"BeaconCell";

@implementation EEBeacon

static EEBeacon *_beacon = nil;

//
//#import "NSDate+convenience.h"
//static Global *_global = nil;
//
//@implementation Global
//
//id tmpObject	= NULL;
//
//EEImageView *tmpImgView	= NULL;
//EEButton *tmpButtom	= NULL;
//EELabel *tmpLabel	= NULL;
//EEView *tmpView	= NULL;
//
//+ (Global*) sharedGlobal
//

+(void)startListener:(id)bdelegte{
    if (_beacon==nil) {
        _beacon = [[EEBeacon alloc] init];
    }
    [_beacon startBroadcast:bdelegte];
}

//开始广播
-(void)startBroadcast:(id)bdelegte{
    
    if (SystemVersion<7.0) {
        EEAlert(@"您是7.0以下的系统，将无法使用定位功能");
        return;
    }
    
    self.isListener = YES;
    self.delegate = bdelegte;
    
    if (self.locationManager) {//说明已经创建了
        return;
    }
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.activityType = CLActivityTypeFitness;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    if([[[UIDevice currentDevice] systemVersion]floatValue] >= 8.0){
         [self.locationManager requestWhenInUseAuthorization];
    }
    
    
    if (![CLLocationManager isRangingAvailable]) {
        NSLog(@"Couldn't turn on ranging: Ranging is not available");
        return;
    }
    
    if (self.locationManager.rangedRegions.count > 0) {
        NSLog(@"Didn't turn on ranging: Ranging already on");
        return;
    }
    
    //注册BeaconRegion
    if (self.beaconRegion)
        return;
    
    NSUUID *proximityUUID = [[NSUUID alloc] initWithUUIDString:kUUID];
    self.beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:proximityUUID identifier:kIdentifier];
    [self.locationManager startRangingBeaconsInRegion:self.beaconRegion];

}



#pragma mark - Beacon ranging delegate methods

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region {
    if ([beacons count] == 0) {
        //NSLog(@"No beacons found nearby.");
    } else {
        //排序
        NSArray *sortArrays = [beacons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSInteger rssi1 = ((CLBeacon *)obj1 ).rssi;
            NSInteger rssi2 = ((CLBeacon *)obj2 ).rssi;

            
            if (rssi1==0) {
                return NSOrderedDescending;
            }
            
            if (rssi1<rssi2) {
                return NSOrderedDescending;
            }else if(rssi1==rssi2){
                return NSOrderedSame;
            }else{
                return NSOrderedAscending;
            }
        }];

        if ([self.delegate respondsToSelector:@selector(eeBeaconNoticeMainbeacon:)]) {
            CLBeacon *beacon1 = [sortArrays objectAtIndex:0];
            NSInteger rssi1 = ((CLBeacon *)beacon1 ).rssi;
            
            
//            //取出信号最强的3个ibeacon
//            for(int i=0;i<3;i++){
//                CLBeacon *beacon = [sortArrays objectAtIndex:i];
//                NSInteger rssi = ((CLBeacon *)beacon ).rssi;
//                NSNumber *major = ((CLBeacon *)beacon ).major;
//                NSNumber *minor = ((CLBeacon *)beacon ).minor;
//            }
            
//            trace(@"__  %ld",(long)rssi1);
            if (rssi1>=-55 && rssi1!=0) {
                [self.delegate performSelector:@selector(eeBeaconNoticeMainbeacon:) withObject:beacon1];
            }
        }
        
//        [sortArrays enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
//            
//            NSInteger rssi1 = ((CLBeacon *)obj ).rssi;
//            trace(@"%d",rssi1);
//
//        }];
        
//        if ([self.delegate respondsToSelector:@selector(eeBeaconNoticeMainbeacon:)]) {
//            CLBeacon *beacon1 = [sortArrays objectAtIndex:0];
//            unsigned int proximity1 = [(CLBeacon *)beacon1 proximity];
//            NSInteger aa = ((CLBeacon *)beacon1 ).rssi;
//            //trace(@"__  %ld",(long)aa);
//            if (proximity1==CLProximityNear || proximity1==CLProximityImmediate) {
//                [self.delegate performSelector:@selector(eeBeaconNoticeMainbeacon:) withObject:beacon1];
//            }
//        }
        /*
        NSArray *sortArrays = [beacons sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            
            NSInteger aa = ((CLBeacon *)obj1 ).rssi;
            trace(@"%ld",(long)aa);
            
            unsigned int proximity1 = [(CLBeacon *)obj1 proximity];
            unsigned int proximity2 = [(CLBeacon *)obj2 proximity];
            if (proximity1<proximity2) {
                return NSOrderedAscending;
            }else if(proximity1==proximity2){
                return NSOrderedSame;
            }else{
                return NSOrderedDescending;
            }
        }];
        

        if ([self.delegate respondsToSelector:@selector(eeBeaconNoticeMainbeacon:)]) {
            CLBeacon *beacon1 = [sortArrays objectAtIndex:0];
            unsigned int proximity1 = [(CLBeacon *)beacon1 proximity];
            NSInteger aa = ((CLBeacon *)beacon1 ).rssi;
            trace(@"__  %ld",(long)aa);
            if (proximity1==CLProximityNear || proximity1==CLProximityImmediate) {
                [self.delegate performSelector:@selector(eeBeaconNoticeMainbeacon:) withObject:beacon1];
            }

        }*/
    }
}



@end
