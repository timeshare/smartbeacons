//
//  EEBeacon.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-1.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol EEBeaconDelegate <NSObject>

-(void)eeBeaconNoticeMainbeacon:(CLBeacon *)nearestBeacon;

@end

@interface EEBeacon : NSObject<CLLocationManagerDelegate>{
    
}

@property(nonatomic,weak)id delegate;
@property(nonatomic)BOOL isListener;

@property(nonatomic,strong)CLLocationManager *locationManager;
@property(nonatomic,strong)CLBeaconRegion *beaconRegion;


+(void)startListener:(id)bdelegte;
    
@end
