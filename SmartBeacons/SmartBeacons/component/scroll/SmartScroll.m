//
//  SmartScroll.m
//  Zume100
//
//  Created by hong pai on 14-2-17.
//
//

#import "SmartScroll.h"
#import "UIImage+Extras.h"
#import <QuartzCore/QuartzCore.h>
#import "PageControl.h"

#define MaxPageNum 13

@implementation SmartScroll


@synthesize delegate;

int txtBgGroundheight = 23;

-(id)initWithFrame:(CGRect)frame withData:(NSMutableArray *)_sliderImage{
    return [self initWithFrame:frame withData:_sliderImage alwaysToFill:YES];
}

- (id)initWithFrame:(CGRect)frame withData:(NSMutableArray *)sliderImages1 alwaysToFill:(BOOL)isFill
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.sliderImages = sliderImages1;
        
        isAlwaysToFill = isFill;
        
        if (self.sliderImages==nil || [self.sliderImages count]<=0) {
            EEAlert(@"slider images 为空");
            return self;
        }
        
        //创建targetScrollView
        CGSize selfSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
        targetScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, selfSize.width, selfSize.height)];
        [self addSubview:targetScrollView];
        
        //刷新内容
        [self ee_refreshContent];

        //创建下方文字背景
        txtBgGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, selfSize.height-txtBgGroundheight, selfSize.width, txtBgGroundheight)];
        //[txtBgGroundView setBackgroundColor:[UIColor whiteColor]];
        [txtBgGroundView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"slides_text_bg.png"]]];
        //txtBgGroundView.alpha = 0.85;
        [self addSubview:txtBgGroundView];
        
        //创建PageControl
        sliderPageControl = [[PageControl alloc] initWithFrame:CGRectMake(0, 0, 15*self.sliderImages.count, txtBgGroundheight)];
        [sliderPageControl setBackgroundColor:[UIColor clearColor]];
        sliderPageControl.userInteractionEnabled = NO;
        sliderPageControl.numberOfPages = [self.sliderImages count];
        sliderPageControl.layer.anchorPoint = CGPointMake(0.5, 0.5);
        sliderPageControl.center = CGPointMake(selfSize.width*0.5, txtBgGroundView.center.y);
        [self addSubview:sliderPageControl];
        
        if ([self.sliderImages count]<=1) {
            [sliderPageControl setHidden:YES];
        }

        
        //创建显示文字  UILabel
        sliderTxtLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 220, txtBgGroundheight)];
        sliderTxtLabel.layer.anchorPoint = CGPointMake(0, 0.5);
        [sliderTxtLabel setTextColor:[UIColor whiteColor]];
        [sliderTxtLabel setFont:[UIFont systemFontOfSize:12]];
        [sliderTxtLabel setBackgroundColor:[UIColor clearColor]];
        sliderTxtLabel.center = CGPointMake(10, txtBgGroundView.center.y);
//        sliderTxtLabel.text = @"放大飞的放大";
        [self addSubview:sliderTxtLabel];
        
    }
    return self;
}

#pragma mark - 刷新

-(void)ee_refreshContent{
    //清空
    
    [self clearAllSlider];
    
    //设置图片
    unsigned  long len = [self.sliderImages count];
    
    CGSize selfSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    
    [self.sliderImages enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        //imgWidth
        //得到UIImage
        
//        newImage = [UIImage image:showImg fitInSize:self.frame.size];
        
//        if (width>=self.frame.size.width && height>=self.frame.size.height) {
//              newImage = [showImg imageScale_to_Width:self.frame.size.width height: self.frame.size.height];
//        }else if(width>=self.frame.size.width ){
//            newImage = [showImg imageScale_to_Width:self.frame.size.width height: height];
//        }else if(height>=self.frame.size.height){
//            newImage = [showImg imageScale_to_Width:width height: self.frame.size.height];
//        }
        
        
//        double delayInSeconds = 3.0;
//        //        dispatch_time_t stopTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
//        dispatch_after(stopTime, dispatch_get_main_queue(), ^(void){
//
//            [self stopRecording];
//            NSLog(@"Movie completed");
//            
//            [videoCamera.inputCamera lockForConfiguration:nil];
//            [videoCamera.inputCamera setTorchMode:AVCaptureTorchModeOff];
//            [videoCamera.inputCamera unlockForConfiguration];
        
        double delayInSeconds = 0.5;
        if (idx==0) {//第一页，立即加载
            delayInSeconds=0;
        }

        dispatch_time_t stopTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(stopTime,dispatch_get_main_queue(), ^{
            
            NSString *shotPath = [[self.sliderImages objectAtIndex:idx] objectForKey:@"posterURL"];
            NSString *filePath = [[NSBundle mainBundle] pathForResource:shotPath ofType:nil];
            UIImage *showImg = [UIImage imageWithContentsOfFile:filePath];
            CGFloat oldWidth  = showImg.size.width;
            CGFloat oldHeight = showImg.size.height;
            
            //转换UIImage尺寸
            UIImageView *imageView = [[UIImageView alloc] initWithImage:showImg];
            [imageView setFrame:self.bounds];
            if(oldWidth<self.frame.size.width && oldHeight<self.frame.size.height){
                
                [imageView setContentMode:UIViewContentModeCenter];
                
            }else{
                
                [imageView setContentMode:UIViewContentModeScaleAspectFit];
                
            }
            imageView.center = CGPointMake(selfSize.width * 0.5f + selfSize.width * idx, selfSize.height*0.5f);
            [targetScrollView addSubview:imageView];
        });
        
        
    }];
    
    [targetScrollView setContentSize:CGSizeMake(len*selfSize.width, selfSize.height)];
    [targetScrollView setPagingEnabled:YES];
    targetScrollView.showsHorizontalScrollIndicator = NO;
    targetScrollView.delegate = self;
    
    if (self.sliderImages!=nil && [self.sliderImages count]>=1) {
        //刷新文字
        [self ee_refreshLabel:0];
    }
}

-(void)ee_refreshLabel:(int)index{
    NSDictionary *dictionary = [self.sliderImages objectAtIndex:index];
    sliderTxtLabel.text = [dictionary objectForKey:@"summary"];
}

//转跳
-(void)ee_gotopage:(int)index{
    [targetScrollView setContentOffset:CGPointMake(1024*(index-1), 0) animated:YES];
}

#pragma mark - 清空所有
-(void)clearAllSlider{
    //sliderImages
}

-(void)ee_showPageControl:(BOOL)isshow{
    if (isshow) {
        sliderPageControl.hidden = NO;
    }else{
        sliderPageControl.hidden = YES;
    }
}

-(void)ee_showTxtLable:(BOOL)isshow{
    if (isshow) {
        txtBgGroundView.hidden = NO;
        sliderTxtLabel.hidden = NO;
    }else{
        txtBgGroundView.hidden = YES;
        sliderTxtLabel.hidden = YES;
    }
}

#pragma mark -- 调整子UI位置 --
-(void)ee_setPageControllCenter:(CGPoint)p{
    sliderPageControl.center = p;
}

#pragma mark - UIScrollView label -

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    trace(@"%f  %f",scrollView.contentOffset.x, scrollView.frame.size.width);
    int index = fabs((scrollView.contentOffset.x) / scrollView.frame.size.width);   //当前是第几个视图
    
    sliderPageControl.currentPage = index;
    
    [self ee_refreshLabel:index];
    
    if (self.delegate!=nil && [self.delegate respondsToSelector:@selector(SmartScroll_PageNumScrollTo:)]) {
        [self.delegate performSelector:@selector(SmartScroll_PageNumScrollTo:) withObject:[NSNumber numberWithInt:(index+1)]];
    }
}

-(void)dealloc{    
    self.delegate = nil;
    self.sliderImages = nil;
}


@end



