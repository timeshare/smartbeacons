//
//  SmartScroll.h
//  Zume100
//
//  Created by hong pai on 14-2-17.
//
//

#import <UIKit/UIKit.h>
@class PageControl;

@protocol SmartScrollDelegate <NSObject>

-(void)SmartScroll_PageNumScrollTo:(NSNumber *)pageNum;

@end


@interface SmartScroll : UIView<UIScrollViewDelegate>{
    
    
    UIScrollView *targetScrollView;
    UIView *txtBgGroundView;
    PageControl *sliderPageControl;
    
    UILabel *sliderTxtLabel;
    
    BOOL isAlwaysToFill;//alwaysToFill : 是否总是盛满屏幕，要么横向撑满。要么纵向撑满。比率正确的话，就全部撑满
}

@property(nonatomic,strong)NSMutableArray *sliderImages;

@property(nonatomic,weak)id<SmartScrollDelegate> delegate;

-(id)initWithFrame:(CGRect)frame withData:(NSMutableArray *)_sliderImage;
- (id)initWithFrame:(CGRect)frame withData:(NSMutableArray *)sliderImages1 alwaysToFill:(BOOL)isFill;

-(void)ee_showPageControl:(BOOL)isshow;
-(void)ee_showTxtLable:(BOOL)isshow;
-(void)ee_gotopage:(int)index;

-(void)ee_refreshContent;
-(void)ee_setPageControllCenter:(CGPoint)p;

@end