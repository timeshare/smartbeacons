//
//  MSSearchBar.m
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "MSSearchBar.h"

@implementation MSSearchBar

-(id)initWithFrame:(CGRect)frame andBorderColor:(UIColor *)color andBorderWidth:(CGFloat)width{
    self = [super init];
    if(self){
    self.frame = frame;
        
    UITextField *searchField = nil;
        NSLog(@"%@",self.subviews);
    for (UIView *subview in self.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    if (searchField) {
        searchField.borderStyle = UITextBorderStyleBezel;
    }
    // 设置搜索栏输入框边框颜色
    searchField.layer.cornerRadius = 18.0f;
    searchField.layer.masksToBounds = YES;
    searchField.layer.borderColor = color.CGColor;
//        [[UIColor colorWithRed:118.0f/255.0f green:193.0f/255.0f blue:220.0f/255.0f alpha:1.0f]CGColor]?
    searchField.layer.borderWidth = width;
    
}
    
    return self;
}


-(id)initWithFrame:(CGRect)frame{
    self = [super init];
    if(self){
        self.frame = frame;
        NSLog(@"%@",self.subviews);
        for (UIView *subview in self.subviews) {
            
            if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
                [[subview.subviews objectAtIndex:0] removeFromSuperview];
                break;
            }
        }
    }
    return self;
}
@end
