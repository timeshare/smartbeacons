//
//  MSSearchBar.h
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSSearchBar : UISearchBar


-(id)initWithFrame:(CGRect)frame andBorderColor:(UIColor *)color andBorderWidth:(CGFloat)width;
-(id)initWithFrame:(CGRect)frame;

@end
