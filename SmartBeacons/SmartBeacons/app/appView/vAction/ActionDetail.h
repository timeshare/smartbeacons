//
//  ActionDetail.h
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface ActionDetail : EEView<UISearchBarDelegate>

@property(nonatomic, strong)NSMutableDictionary *configData;

@property(nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic, strong) UIScrollView *scrollView;
@end
