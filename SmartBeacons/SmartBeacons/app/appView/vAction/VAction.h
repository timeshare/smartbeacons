//
//  VAction.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import "ZBarSDK.h"

#import <UIKit/UIKit.h>

@interface VAction : EEView<ZBarReaderDelegate,UIImagePickerControllerDelegate>

@property(nonatomic,strong)NSMutableArray *configList;

@property(nonatomic, strong) ZBarReaderViewController *reader;

@end;
