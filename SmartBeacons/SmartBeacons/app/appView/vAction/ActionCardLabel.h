//
//  ActionCardLabel.h
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionCardLabel : UILabel

-(id)initWithFrame:(CGRect)frame andData:(NSMutableDictionary *)dataDictionary;

@end
