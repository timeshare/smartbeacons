//
//  ActionListView.m
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "ActionListView.h"
#import "ActionListCell.h"
#import "ActionDetail.h"

@implementation ActionListView

-(instancetype)init{
    self = [super init];
    if(self){
        [self initView];
    }
    return self;
}


-(void)initView{
    self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    self.frame = CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight);
    
    self.contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight-AppTopHeight) style:UITableViewStylePlain];
    self.contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.contentTable.showsVerticalScrollIndicator = NO;
    self.contentTable.delegate = self;
    self.contentTable.dataSource = self;
    [self addSubview:self.contentTable];
    
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"assets/action/ActionList.plist" ofType:nil];
    _listArray = [[NSMutableArray alloc]initWithContentsOfFile:filePath];
}


#pragma mark tableviewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor colorWithRed:0.96 green:0.96 blue:0.96 alpha:1];
    [view addSubview:[Global createImage:@"assets/action/timeicon.png" center:CGPointMake(130, 15)]];
    NSString *date = [NSString stringWithFormat:@"%@", [[_listArray objectAtIndex:section]objectForKey:@"date"]];
   
    
    
    [view addSubview:[Global createLabel:CGRectMake(150, 0, 80, 30) label:date lines:1 fontSize:14 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
    return view;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSString *str = @"11";
    return str;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(cell == nil){
        cell = [[ActionListCell alloc]initCellByData:[[[_listArray objectAtIndex:indexPath.section]objectForKey:@"action"]objectAtIndex:indexPath.row]];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0 && indexPath.row == 1){
        ActionDetail *detail = [ActionDetail instanceByData:nil];
        [detail addTransition:1];
        [[RootController sharedRootViewController].view addSubview:detail];
    }
}

@end
