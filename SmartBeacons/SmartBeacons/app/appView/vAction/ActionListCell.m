//
//  ActionListCell.m
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "ActionListCell.h"

@implementation ActionListCell{
    UIColor *color;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initCellByData:(NSDictionary *)dataDictionary{
    self = [super init];
    if(self){
        id icon = [dataDictionary objectForKey:@"icon"];
        NSString *title = [dataDictionary objectForKey:@"title"];
        NSString *time = [dataDictionary objectForKey:@"time"];
        
        [self addSubview:[Global createImage:icon center:CGPointMake(25, 25)]];
        [self addSubview:[Global createLabel:CGRectMake(50, 10, 200, 30) label:title lines:1 fontSize:14 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
        if([time isEqualToString:@"已结束"]){
            color = [UIColor grayColor];
        }else if([time isEqualToString:@"正在进行"]){
            color = [UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1];
        }else{
            color = [UIColor blackColor];
        }
        
        [self addSubview:[Global createLabel:CGRectMake(260, 10, 50, 30) label:time lines:1 fontSize:13 fontName:nil textcolor:color align:NSTextAlignmentRight]];
        
        [self addSubview:[Global createViewByFrame:CGRectMake(0, 49.7, 320, 0.3) color:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]]];
    }
    return self;
}

@end
