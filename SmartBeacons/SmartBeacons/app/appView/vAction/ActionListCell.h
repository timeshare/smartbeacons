//
//  ActionListCell.h
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActionListCell : UITableViewCell

@property(nonatomic,strong)NSDictionary *configData;

-(id)initCellByData:(NSDictionary *)dataDictionary;

@end
