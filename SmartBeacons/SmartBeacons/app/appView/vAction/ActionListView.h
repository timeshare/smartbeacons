//
//  ActionListView.h
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface ActionListView : EEView<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSDictionary *configData;
@property(nonatomic,strong)NSMutableArray *listArray;
@property(nonatomic,strong)UITableView *contentTable;
@end
