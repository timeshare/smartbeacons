//
//  ActionDetail.m
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "ActionDetail.h"
#import <ShareSDK/ShareSDK.h>

@implementation ActionDetail

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(id)instanceByData:(id)configData{
    ActionDetail *detail = [[ActionDetail alloc]init];
    detail.configData = configData;
    [detail initView];
    
    return detail;
}

-(void)initView{
    [self setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppTopHeight)];
    self.backgroundColor = [UIColor whiteColor];
    
    //顶部条
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(11, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight)];
    _scrollView.contentSize = CGSizeMake(View_Size.width, 503);
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.bounces = NO;
    [self addSubview:_scrollView];
    
    
    [_scrollView addSubview:[Global createImage:@"assets/action/actionDetailBack.png" frame:CGRectMake(0, 0, View_Size.width, 503)]];
    
    [_scrollView addSubview:[Global createImage:@"assets/action/back.png" center:CGPointMake(View_Size.width/2, 410-AppTopHeight)]];
    
    [_scrollView addSubview:[Global createBtn:CGRectMake(203, 240, 20.5, 19) normal:@"assets/action/collection.png" down:nil target:self action:nil]];
    
     [_scrollView addSubview:[Global createBtn:CGRectMake(260, 240, 19.5, 17.5) normal:@"assets/action/share.png" down:nil target:self action:@selector(shareContent)]];
    
    [_scrollView addSubview:[Global createBtn:CGRectMake(85, 430-AppTopHeight, 70.5, 70.5) normal:@"assets/action/joinon.png" down:nil target:self action:@selector(join)]];
    
    [_scrollView addSubview:[Global createBtn:CGRectMake(165, 430-AppTopHeight, 70.5, 70.5) normal:@"assets/action/liveoff.png" down:nil target:self action:@selector(live)]];
    
    [_scrollView addSubview:[Global createLabel:CGRectMake(40, 300-AppTopHeight, 250, 65) label:@"销售总监乔治·布兰肯希普现场演讲介绍新款车型" lines:2 fontSize:18 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    [_scrollView addSubview: tmpLabel = [Global createLabel:CGRectMake(110, 350-AppTopHeight, 250, 70) label:@"正在进行..." lines:1 fontSize:22 fontName:nil textcolor:[UIColor colorWithRed:241/255.0 green:139/255.0 blue:26/255.0 alpha:1] align:NSTextAlignmentLeft]];
    tmpLabel.font = [UIFont boldSystemFontOfSize:20];
}


- (void)shareContent
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK"  ofType:@"jpg"];
    NSArray *shareList = [ShareSDK getShareListWithType:
                     //     ShareTypeQQ,
                          ShareTypeWeixiSession,
                          ShareTypeWeixiTimeline,
                          ShareTypeSinaWeibo,
                     //     ShareTypeQQSpace,
                          
                          nil];
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:@"浦东国际汽车展,Welcome~~~~"
                                       defaultContent:@"分享内容"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"ShareSDK"
                                                  url:@"http://www.sharesdk.cn"
                                          description:@"这是一条测试信息"
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    
                                }
                            }];
}

-(void)join{
    
}
-(void)live{
    
}
-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}
@end
