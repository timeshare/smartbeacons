//
//  VAction.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "VAction.h"
#import "RecentActionView.h"
#import "ActionListView.h"
#import "ActionDetail.h"

@implementation VAction{
    UIButton *actBtn,*listBtn;
    BOOL _isAct;
    RecentActionView *recentActionView;
    ActionListView *actionListView;
}

+(id)instance{
    VAction *view  = [[VAction alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}

-(void)initView{
    [self setBackgroundColor:[UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //添加两个按钮
    _isAct = YES;
    actBtn = [[UIButton alloc]initWithFrame:CGRectMake(60, 9, 90, 25)];
    actBtn.layer.cornerRadius = 12;
    [actBtn setTitle:@"最近活动" forState:UIControlStateNormal];
    actBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [actBtn addTarget:self action:@selector(goAction) forControlEvents:UIControlEventTouchUpInside];
    actBtn.backgroundColor = [UIColor whiteColor];
    [actBtn setTitleColor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
    
    listBtn = [[UIButton alloc]initWithFrame:CGRectMake(160, 9, 90, 25)];
    listBtn.layer.cornerRadius = 12;
    [listBtn setTitle:@"活动列表" forState:UIControlStateNormal];
    listBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [listBtn addTarget:self action:@selector(goList) forControlEvents:UIControlEventTouchUpInside];
    listBtn.backgroundColor = [UIColor clearColor];
    
    [self addSubview:actBtn];
    [self addSubview:listBtn];
    
    [self addAction];
}


-(void)goAction{
    if(_isAct == NO){
        actBtn.backgroundColor = [UIColor whiteColor];
        listBtn.backgroundColor = [UIColor clearColor];
        [actBtn setTitleColor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
        [listBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _isAct = YES;
        [self addAction];
    }
}

-(void)goList{
    if(_isAct == YES){
        actBtn.backgroundColor = [UIColor clearColor];
        listBtn.backgroundColor = [UIColor whiteColor];
        [actBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [listBtn setTitleColor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
        _isAct = NO;
        [self addList];
    }
}

-(void)addAction{
    [actionListView removeFromSuperview];
    recentActionView = [[RecentActionView alloc]init];
    [self addSubview:recentActionView];
}
-(void)addList{
    [recentActionView removeFromSuperview];
    actionListView = [[ActionListView alloc]init];
    [self addSubview:actionListView];
}

#pragma mark 二维码扫描
-(void)scan{
   [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}


-(void)pullToRefresh{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


-(void)dealloc{
    track();
}

@end
