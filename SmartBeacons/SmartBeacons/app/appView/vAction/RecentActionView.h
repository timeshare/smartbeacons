//
//  RecentActionView.h
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface RecentActionView : EEView

@property(nonatomic, strong)NSMutableArray *actionArray;
@end
