//
//  RecentActionView.m
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "RecentActionView.h"
#import "ActionCardLabel.h"
@implementation RecentActionView

-(instancetype)init{
    self = [super init];
    if(self){
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
    self.frame = CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight);
    
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"assets/action/RecentAction.plist" ofType:nil];
    _actionArray = [NSMutableArray arrayWithContentsOfFile:filePath];

    [self addCard];
}

-(void)addCard{
    UIScrollView *scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight)];
    scrollView.contentSize = CGSizeMake(View_Size.width, 575);
    scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:scrollView];
    
    ActionCardLabel *actionCard = [[ActionCardLabel alloc]initWithFrame:CGRectMake(10, 15, 300, 265) andData:[_actionArray objectAtIndex:0]];
    [scrollView addSubview:actionCard];
    
    ActionCardLabel *actionCard1 = [[ActionCardLabel alloc]initWithFrame:CGRectMake(10, 15+265+15, 300, 265) andData:[_actionArray objectAtIndex:1]];
    [scrollView addSubview:actionCard1];

}

@end
