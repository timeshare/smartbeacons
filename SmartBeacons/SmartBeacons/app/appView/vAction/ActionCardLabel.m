//
//  ActionCardLabel.m
//  SmartBeacons
//
//  Created by Alex on 14/11/5.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "ActionCardLabel.h"
#import "ActionDetail.h"

@implementation ActionCardLabel

-(id)initWithFrame:(CGRect)frame andData:(NSMutableDictionary *)dataDictionary{
    self = [super init];
    if(self){
        self.frame = frame;
        //可以交互
        self.userInteractionEnabled = YES;
        
        //设置背景图片
        UIImage *imgName = [UIImage imageNamed:@"assets/mainpage/guideCard2.png"];
        UIImageView *img = [[UIImageView alloc]initWithImage:imgName];
        img.frame = CGRectMake(0, 0, 300, 265);
        [self addSubview:img];
        
        id icon = [dataDictionary objectForKey:@"icon"];
        NSString *title = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"title"]];
        NSString *time = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"time"]];
        NSString *content = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"content"]];
        id pic = [dataDictionary objectForKey:@"pic"];
        
        [self addSubview:[Global createImage:icon center:CGPointMake(15, 15)]];
        [self addSubview:[Global createLabel:CGRectMake(45, 5, 250, 25) label:title lines:1 fontSize:15 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
        [self addSubview:[Global createLabel:CGRectMake(45, 20, 250, 25) label:time lines:1 fontSize:11 fontName:nil textcolor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] align:NSTextAlignmentLeft]];
        [self addSubview:[Global createLabel:CGRectMake(10, 45, 285, 40) label:content lines:2 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        
        UIImageView *iv = [[UIImageView alloc]init];
        [self addSubview: iv = [Global createImage:pic center:CGPointMake(View_Size.width/2-10, 170)]];
        
        UIButton *btn = [[UIButton alloc]initWithFrame:img.frame];
//        btn.backgroundColor = [UIColor blueColor];
        [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
    }

    return self;
}

-(void)click{
    NSLog(@"点击了");
    ActionDetail *detail = [ActionDetail instanceByData:nil];
    [detail addTransition:1];
    [[RootController sharedRootViewController].view addSubview:detail];
}


@end
