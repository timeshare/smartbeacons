//
//  AppView.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-19.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VMap.h"
#import "ZBarSDK.h"

@class EEButtonGroup;
@interface AppView : EEView<ZBarReaderDelegate>{
    EEButtonGroup *group;
}

//@property(nonatomic) BOOL isAdd;
@property(nonatomic,weak) EEView *selectedView;

@property(nonatomic,strong) VMap *staticVMap;

@property(nonatomic, strong) ZBarReaderViewController *reader;

-(void)showMap;
-(void)showAppView:(NSString *)viewClassName;

@end
