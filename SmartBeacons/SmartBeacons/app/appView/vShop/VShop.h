//
//  VShop.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import "ZBarSDK.h"

@interface VShop : EEView<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,ZBarReaderDelegate>{
    BOOL _isSearch;
}

@property(nonatomic, strong)NSMutableArray *configList;
//@property(nonatomic, strong)NSMutableDictionary *dic;
@property(nonatomic, strong)NSMutableArray *indexArray;
@property(nonatomic, strong)NSMutableArray *keys;

@property(nonatomic, strong)UITableView *contentTable;

@property(nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic, strong) ZBarReaderViewController *reader;

@end
