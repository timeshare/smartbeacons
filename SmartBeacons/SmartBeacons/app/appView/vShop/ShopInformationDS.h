//
//  ShopInformationDS.h
//  SmartBeacons
//
//  Created by Alex on 15/1/8.
//  Copyright (c) 2015年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface ShopInformationDS : EEView<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong)NSMutableArray *configList;
@property(nonatomic,strong)NSDictionary *configData;

@property(nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic, strong)UITableView *contentTable;

@end
