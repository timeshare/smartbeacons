//
//  ShopInformationCell.m
//  SmartBeacons
//
//  Created by Alex on 14/11/17.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "ShopInformationCell.h"

@implementation ShopInformationCell{
    UIColor *aColor;
    UIColor *tColor;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initCellByData:(NSDictionary *)dataDictionary{
    self = [super init];
    if (self) {
        self.configData = dataDictionary;
        
        NSString *action = [self.configData objectForKey:@"action"];
        NSString *time = [self.configData objectForKey:@"time"];
        
        if([action isEqualToString:@"展位活动"]){
            aColor = [UIColor blackColor];
        }else{
            aColor = [UIColor grayColor];
        }
        
        if([time isEqualToString:@"活动时间"]){
            tColor = [UIColor blackColor];
        }else if([time isEqualToString:@"送完即止"] || [time isEqualToString:@"即将开始"]){
            tColor = [UIColor colorWithRed:111/255.0 green:174/255.0 blue:69/255.0 alpha:1];
        }else{
            tColor = [UIColor grayColor];
        }
        
        [self addSubview:[Global createLabel:CGRectMake(14, 5, 200, 35) label:action lines:0 fontSize:15 fontName:nil textcolor:aColor align:NSTextAlignmentLeft]];
        [self addSubview:[Global createLabel:CGRectMake(245, 5, 60, 35) label:time lines:0 fontSize:15 fontName:nil textcolor:tColor align:NSTextAlignmentLeft]];
    }
    return self;
}

@end
