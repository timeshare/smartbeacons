//
//  VShop.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "VShop.h"
#import "ShopCell.h"
#import "ShopInforMation.h"
#import "ShopInformationDS.h"

@implementation VShop

+(id)instance{
    VShop *view  = [[VShop alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}


-(void)initView{
    [self setBackgroundColor:[UIColor whiteColor]];
    
    //顶部条
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //取到数据
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/shop/shopname.plist" ofType:nil];
    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
    _configList = arrayList;
//    NSLog(@"_configList_____%@",_configList);
    
    _indexArray =[NSMutableArray array];
    for(NSDictionary *dic in _configList){
        [_indexArray addObject:[dic objectForKey:@"index"]];
    }
//    _indexArray = [_indexArray sortedArrayUsingSelector:@selector(compare:)];
    
    self.contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppButtomHeight-AppTopHeight) style:UITableViewStylePlain];
    self.contentTable.delegate = self;
    self.contentTable.dataSource = self;
    self.contentTable.sectionIndexBackgroundColor = [UIColor clearColor];
    [self addSubview:self.contentTable];
    
    //给最外层的view添加一个手势响应UITapGestureRecognizer
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGr];
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_searchBar resignFirstResponder];
}

-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}


#pragma mark UISearchBar搜索
-(void)search{
    if(_isSearch == NO){
        //添加UITableView下移的动画效果
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.35];
        
        //添加searchBar
        self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 44, 320, 40)];
        _searchBar.placeholder = @"请输入关键字...";
        _searchBar.delegate = self;
//        _searchBar.showsCancelButton = YES;
        _searchBar.searchBarStyle = UISearchBarStyleDefault;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        [self addSubview:self.searchBar];
        [self.searchBar becomeFirstResponder];
        _isSearch = YES;
        
        self.contentTable.frame = CGRectMake(0, 84, View_Size.width, View_Size.height-AppButtomHeight-84);
        
        [UIView commitAnimations];
        
    }else if (_isSearch == YES)
    {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.35];
        
        [_searchBar removeFromSuperview];
        _isSearch = NO;
        
        self.contentTable.frame = CGRectMake(0, 44, View_Size.width, View_Size.height-AppButtomHeight-44);
        
        [UIView commitAnimations];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"搜索");
    [_searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"取消");
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    
    [self.searchBar removeFromSuperview];
    _isSearch = NO;
    
    self.contentTable.frame = CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppButtomHeight-AppTopHeight);
    
    [UIView commitAnimations];
    
    [_searchBar resignFirstResponder];
}

#pragma mark tableviewDataSource
//加入索引
-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    _keys = [NSMutableArray arrayWithObjects:@"★", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", nil];
    return _keys;
//    return _indexArray;
}

-(NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index{
    
    //在屏幕中间显示点击的字母
    NSString *letter = [_keys objectAtIndex:index];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    label.center = CGPointMake(View_Size.width/2, View_Size.height/2);
    label.text = letter;
    label.alpha = 0.5;
    label.textColor = [UIColor blueColor];
    label.font = [UIFont fontWithName:nil size:30];
    [self addSubview:label];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    label.alpha = 0;
    [UIView commitAnimations];
    
    int flag = 0;
    for (int j=index; j>=0; j--) {
        NSString *key = [_keys objectAtIndex:j];
        NSLog(@"j--%d ,key--%@",j,key);
        for(int i=0; i<_indexArray.count; i++){
            NSLog(@"i--%d",i);
            if([[_indexArray objectAtIndex:i]isEqualToString:key]){
                index = i;
                NSLog(@"index--%d",index);
                flag = 1;
                break;
            }
        }
        if(flag == 1){
            break;
        }
    }
    // 获取所点目录对应的indexPath值
    NSIndexPath *selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:index];
    // 让table滚动到对应的indexPath位置
    [_contentTable scrollToRowAtIndexPath:selectIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    return index;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return [_indexArray objectAtIndex:section];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _indexArray.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *cars = [[_configList objectAtIndex:section]objectForKey:@"Cars"];
    return cars.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //取到每行的内容
    NSArray *cars = [[_configList objectAtIndex:indexPath.section]objectForKey:@"Cars"];
    NSDictionary *dictionary = [cars objectAtIndex:indexPath.row];
    
    if(cell == nil){
        cell = [[ShopCell alloc]initCellByData:dictionary];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"section:%d,row:%d",indexPath.section,indexPath.row);
    if(indexPath.row == 0 && indexPath.section == 0){
        ShopInforMation *shopInformation = [ShopInforMation instanceByData:[self.configList objectAtIndex:indexPath.row]];
        [shopInformation addTransition:1];
        [[RootController sharedRootViewController].view addSubview:shopInformation];
    }else{
        ShopInformationDS *shopInformation = [ShopInformationDS instanceByData:[self.configList objectAtIndex:indexPath.row]];
        [shopInformation addTransition:1];
        [[RootController sharedRootViewController].view addSubview:shopInformation];
    }
    
    
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)dealloc{
    track();
    self.configList = nil;
}

@end
