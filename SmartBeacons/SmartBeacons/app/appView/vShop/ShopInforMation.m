//
//  ShopInforMation.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "ShopInforMation.h"
#import "AppView.h"
#import "SmartScroll.h"
#import "ShopInformationCell.h"

@implementation ShopInforMation

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(id)instanceByData:(id)configData{
    ShopInforMation *shopInforMation = [[ShopInforMation alloc] init];
    shopInforMation.configData = configData;
    [shopInforMation initView];
    
    return shopInforMation;
}


-(void)initView{
    [self setBackgroundColor:CWhite];
    [self setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    
    //顶部条
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(11, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //取到数据
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/shop/shopinformation.plist" ofType:nil];
    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
    _configList = arrayList;

    
    [self addContent];
    
    //给最外层的view添加一个手势响应UITapGestureRecognizer
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGr];
}

-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_searchBar resignFirstResponder];
}

-(void)addContent{
    [self addSubview:[Global createImage:@"assets/shop/backcar.png" frame:CGRectMake(0, AppTopHeight, View_Size.width, 243)]];
    [self addSubview:[Global createImage:@"assets/shop/tesla.png" frame:CGRectMake(20, AppTopHeight+10, 60, 65)]];
    [self addSubview:[Global createLabel:CGRectMake(100, AppTopHeight+20, 280, 40) label:@"TESLA 特斯拉" lines:1 fontSize:20 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
    [self addSubview:[Global createBtn:CGRectMake(255, AppTopHeight+25, 57, 29) normal:@"assets/shop/download.png" down:nil target:self action:nil]];
    
    [self addSubview:[Global createBtn:CGRectMake(20, AppTopHeight+243+10, 85.5, 85.5) normal:@"assets/shop/sendcardon.png" down:nil target:self action:nil]];
    [self addSubview:[Global createBtn:CGRectMake(120, AppTopHeight+243+10, 85.5, 85.5) normal:@"assets/shop/goplaceoff.png" down:nil target:self action:@selector(goMap)]];
    [self addSubview:[Global createBtn:CGRectMake(220, AppTopHeight+243+10, 85.5, 85.5) normal:@"assets/shop/collectoff.png" down:nil target:self action:nil]];
    
    _contentTable = [[UITableView alloc]initWithFrame:CGRectMake(0, 395, View_Size.width, 150) style:UITableViewStylePlain];
    _contentTable.delegate = self;
    _contentTable.dataSource = self;
    _contentTable.showsVerticalScrollIndicator = NO;
    _contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_contentTable];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _configList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSDictionary *dic = [_configList objectAtIndex:indexPath.row];
    
    if(cell == nil){
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell = [[ShopInformationCell alloc]initCellByData:dic];
    }
    //设置cell的背景颜色
    if (indexPath.row%2 == 0) {
        cell.backgroundColor = [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [_contentTable deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)goMap{
    [self eeRemoveRight];
    [[Global sharedGlobal].appView showMap];
    
}
-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}

//-(void)clickEnterMap{
//    track();
//    //移除
//    [self eeRemoveRight];
//    //记住配置信息
//    
//    //移除地图
//    [[Global sharedGlobal].appView showMap];
//}

-(void)dealloc{
    self.configData = nil;
}




@end
