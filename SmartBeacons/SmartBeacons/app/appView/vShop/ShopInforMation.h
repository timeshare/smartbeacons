//
//  ShopInforMation.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"

@interface ShopInforMation : EEView<UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate>

@property(nonatomic, strong)NSMutableArray *configList;
@property(nonatomic,strong)NSDictionary *configData;

@property(nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic, strong)UITableView *contentTable;

@end
