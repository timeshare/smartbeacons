//
//  ShopInformationCell.h
//  SmartBeacons
//
//  Created by Alex on 14/11/17.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopInformationCell : UITableViewCell

-(id)initCellByData:(NSDictionary *)dataDictionary;

@property(nonatomic,strong)NSDictionary *configData;

@end
