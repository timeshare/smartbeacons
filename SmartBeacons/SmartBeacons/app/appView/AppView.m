//
//  AppView.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-19.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "AppView.h"
#import "EEButtonGroup.h"
#import "EEBeacon.h"
#import "MapNoticeManager.h"
#import "EEMap.h"

@implementation AppView


-(void)initView{
    [Global sharedGlobal].appView = self;
    
    NSArray *sourceArray =
    @[ @{@"normal": @"assets/image/btn1mainpage.png",@"select": @"assets/image/btn1mainpageH.png",@"title":@""},
       @{@"normal": @"assets/image/btn2zhanshang.png",@"select": @"assets/image/btn2zhanshangH.png",@"title":@""},
       @{@"normal": @"assets/image/btn3map.png",@"select": @"assets/image/btn3mapH.png",@"title":@""},
       @{@"normal": @"assets/image/btn4action.png",@"select": @"assets/image/btn4actionH.png",@"title":@""},
       @{@"normal": @"assets/image/btn5user.png",@"select": @"assets/image/btn5userH.png",@"title":@""},
       ];

    group = [[EEButtonGroup alloc] initWithSourceArray:sourceArray positionArray:nil normalColor:nil selectColor:nil fontSize:18 isTransparentArea:YES];
    group.pdelegate = self;
    group.layer.anchorPoint = CGPointMake(0.5, 1);
    [self addSubview:group];
    group.center = CGPointMake(View_Size.width*0.5, View_Size.height);
    [group setSelectIndex:0];
    
    [EEBeacon startListener:self];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(scan) name:@"scan" object:nil];
}

-(void)EEButtonGroup_SelectIndex:(int)index group:(id)btnGroup{
    NSArray *viewClassName = @[@"VMainPage",@"VShop",@"VMap",@"VAction",@"PersonalCenter"];
    [self showAppView:[viewClassName objectAtIndex:index]];
}

#pragma mark --- beacon delegate ---
-(void)eeBeaconNoticeMainbeacon:(CLBeacon *)nearestBeacon{
//    track();
    //MapNoticeManager 这个是专门用来管理通知信息的
    [[MapNoticeManager shareInstance] catchIbeaconNotice:nearestBeacon];
    
}

#pragma mark - 导航： 点击导航按钮，切换程序视图--
-(void)showAppView:(NSString *)viewClassName{
    if (self.selectedView) {
            [self.selectedView removeFromSuperview];
        self.selectedView = nil;
    }
    
    Class targetClass = NSClassFromString(viewClassName);
    id modelView = [targetClass instance];
//     
//    
//    if ([viewClassName isEqualToString:@"VMap"]) {
//        self.staticVMap = modelView;
//    }
    
    [self addSubview:modelView];
    [self sendSubviewToBack:modelView];
    self.selectedView = modelView;
}

-(void)showMap{
    [group setSelectIndex:2];
}

#pragma mark 二维码扫描
-(void)scan{
    _reader = [ZBarReaderViewController new];
    _reader.readerDelegate = self;
    //支持屏幕所有方向
//   _reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    //非全屏
    _reader.wantsFullScreenLayout = NO;
    //隐藏底部控制按钮
    _reader.showsZBarControls = NO;
    //自定义界面
    [self setOverlayPickerView:_reader];
    
    ZBarImageScanner *scanner = _reader.scanner;
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    //右上角为(0,0) 左下角为(1,1)
    _reader.scanCrop = CGRectMake(0.25, 0.25, 0.75, 0.75);
    [[RootController sharedRootViewController].view addSubview:_reader.view];
}

#pragma mark 自定义扫描界面
- (void)setOverlayPickerView:(ZBarReaderViewController *)reader{
    //清除原有控件
    for (UIView *temp in [reader.view subviews]) {
        for (UIButton *button in [temp subviews]) {
            if ([button isKindOfClass:[UIButton class]]) {
                [button removeFromSuperview];
            }
        }
        for (UIToolbar *toolbar in [temp subviews]) {
            if ([toolbar isKindOfClass:[UIToolbar class]]) {
                [toolbar setHidden:YES];
                [toolbar removeFromSuperview];
            }
        }
    }
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [reader.view addSubview:imgView];
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"二维码/条码";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [reader.view addSubview:title];
    
    //返回按钮
    [reader.view addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(dismissOverlayView:)]];
    
    //中间的view
    UIView* midView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    midView.center = CGPointMake(View_Size.width/2, View_Size.height/2);
    midView.alpha = 0.5;
    midView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"assets/image/point.png"]];
    [reader.view addSubview:midView];
    
    //最上部view
    UIView* upView = [[UIView alloc] initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height/2-120-AppTopHeight)];//70+44-AppTopHeight
    upView.alpha = 0.5;
    upView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:upView];
    
    //左侧的view
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, View_Size.height/2-120, View_Size.width/2-120, 240)];
    leftView.alpha = 0.5;
    leftView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:leftView];
    
    //右侧的view
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(View_Size.width/2+120, View_Size.height/2-120, View_Size.width/2-120, 240)];
    rightView.alpha = 0.5;
    rightView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:rightView];
    
    //底部view
    UIView * downView = [[UIView alloc] initWithFrame:CGRectMake(0, View_Size.height/2+120, View_Size.width, View_Size.height)];
    downView.alpha = 0.5;
    downView.backgroundColor = [UIColor blackColor];
    [reader.view addSubview:downView];
    
    //边上的框
    UIView* frameView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 240, 240)];
    frameView.center = CGPointMake(View_Size.width/2, View_Size.height/2);
    frameView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"assets/image/frame.png"]];
    [reader.view addSubview:frameView];
    
    //用于说明的label
    UILabel * labIntroudction= [[UILabel alloc] init];
    labIntroudction.backgroundColor = [UIColor clearColor];
    labIntroudction.frame=CGRectMake(15, 380, 290, 50);
    labIntroudction.numberOfLines=1;
    labIntroudction.textColor=[UIColor whiteColor];
    labIntroudction.text=@"将二维码/条码放入框内,即可自动扫描";
    [reader.view addSubview:labIntroudction];
}

#pragma mark 取消button方法
- (void)dismissOverlayView:(id)sender{
    [_reader.view removeFromSuperview];
}

#pragma mark 获得扫描的信息 并且跳转
- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    id <NSFastEnumeration> results = [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results){
        break;
    }
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:symbol.data]];
    NSLog(@"%@",symbol.data);
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


@end