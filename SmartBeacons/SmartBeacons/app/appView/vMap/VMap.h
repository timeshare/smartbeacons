//
//  VMap.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import "EEMap.h"
#import "EEBeacon.h"
#import "MapNoticeManager.h"


@interface VMap : EEView<EEMapDelegate,EEBeaconDelegate,UISearchBarDelegate>{
    EEMap *eeMap;
    BOOL _isSearch;
}

@property (nonatomic, strong) UIWebView *webview;
@property (nonatomic,strong) UISearchBar *searchBar;

-(void)setuserPosition:(CGPoint)toPoiont;
-(void)setuserPositionBuilderId:(int)bid isAuto:(BOOL)isauto;


@end
