//
//  SlidingScrollView.m
//  SmartBeacons
//
//  Created by Alex on 14-10-14.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "SlidingScrollView.h"

@implementation SlidingScrollView


-(BOOL)touchesShouldCancelInContentView:(UIView *)view{
    if([view isKindOfClass:[UIButton class]]){
        return YES;
    }
    return NO;
}

- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view{
    if ([view isKindOfClass:[UIButton class]])
    {
        return YES;
    }
    else{
        return NO;
    }
}


//传递touch事件
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event{
    if(!self.dragging)
    {
        [[self nextResponder]touchesBegan:touches withEvent:event];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!self.dragging)
    {
        [[self nextResponder]touchesMoved:touches withEvent:event];
    }
    [super touchesMoved:touches withEvent:event];
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    if(!self.dragging)
    {
        [[self nextResponder]touchesEnded:touches withEvent:event];
    }
    [super touchesEnded:touches withEvent:event];
}



@end
