//
//  SlidingScrollView.h
//  SmartBeacons
//
//  Created by Alex on 14-10-14.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlidingScrollView : UIScrollView


@property(nonatomic, assign) NSTimeInterval touchTimer;

@end
