//
//  EEMapContainer.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-21.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapContainer.h"

@implementation EEMapContainer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithSource:(NSString *)fileName
{
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}


-(void)initView{
    //地图地图
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"map2@2x.jpg" ofType:nil];
    mapView = [[UIImageView alloc] initWithImage:[UIImage imageWithContentsOfFile:filePath]];
    [self addSubview:mapView];
    
    //设置自身宽高度
    [self setFrame:CGRectMake(0, 0, mapView.width, mapView.height)];
    
    //房屋区域热点
    self.mapPathLayer = [[EEMapPathLayer alloc] initWithFrame:self.bounds];
    [self addSubview:self.mapPathLayer];
}

-(void)tapStageGesture:(UITapGestureRecognizer *)gesture{
    //查找点击的热点区域
    [self.mapPathLayer clickStage:[gesture locationInView:self.mapPathLayer]];
}

-(void)userLocateToPoint:(CGPoint)point{
    self.userCurrentLocation = point;
    
    if (userLight==nil) {
        userLight = [UserLight instance];
        userLight.alpha = 0.95;
        [self addSubview:userLight];
    }
    userLight.center = point;
}

-(CGPoint)userLocateToBuidId:(int)bid{
    //@[@{@"name":@"长安马自达",@"id":@"41"},@[@"127",@"178"],@[@"213",@"193"],@[@"199",@"270"],@[@"112",@"256"]]
    NSArray *targetBuildConfig = [self.mapPathLayer getBuildConfigById:bid];
    
    //得到中心点
    self.userCurrentLocation = ccp(([[[targetBuildConfig objectAtIndex:1] objectAtIndex:0] intValue]+[[[targetBuildConfig objectAtIndex:3] objectAtIndex:0] intValue])*0.5,
                                   ([[[targetBuildConfig objectAtIndex:1] objectAtIndex:1] intValue]+[[[targetBuildConfig objectAtIndex:3] objectAtIndex:1] intValue])*0.5
                                   );
    //设置用户现在处的位置的id
    self.mapPathLayer.userLocateMarkId = bid;
    
    if (userLight==nil) {
        userLight = [UserLight instance];
        userLight.alpha = 0.95;
        [self.delegate addSubview:userLight];
    }
    [UIView animateWithDuration:0.25 animations:^{
        userLight.center = self.userCurrentLocation;
    }];
    
    return self.userCurrentLocation;
}

#pragma mark --- 开始寻路 ---
//寻路
-(void)findPathFromUserLocateTo:(int)toBid{
    [self.mapPathLayer findPathFromUserLocateTo:toBid];
}


-(void)resetUserLightAnimation{
    [userLight scaleRange];
}

- (void)dealloc
{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
    track();
}

@end
