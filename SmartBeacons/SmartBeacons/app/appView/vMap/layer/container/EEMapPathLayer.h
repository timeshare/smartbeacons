//
//  EEMapAreaHotLayer.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import "FWTPopoverView.h"
#import "EEMapPathPoint.h"
#import "LayerSearchPath.h"
#import "EEMapContainer.h"

@interface EEMapPathLayer : EEView{
    id lastClickHotAreaConfig;
    int index;
    
    LayerSearchPath *searchPathLayer;
    
    //寻路变量
    EEMapPathPoint *endPathPoint;
    BOOL isFinded;
    
    //起点 终点
    EEImageView *ico_startMark;
    EEImageView *ico_endMark;
    int startMarkId;
    int endMarkId;
}

@property(nonatomic, strong)NSArray *hotAreas;
@property(nonatomic,strong)FWTPopoverView *popoverView;;

@property(nonatomic,strong)NSArray *pathPoints;

@property(nonatomic)int userLocateMarkId;//当前用户所在的markid


-(void)popOneInforWindow:(NSArray *)inforData point:(CGPoint)point;
-(void)clickStage:(CGPoint)point;

-(void) checkPathPoint:(id)sender;

-(void)findPathFromUserLocateTo:(int)bid;
-(void)searchPath:(int)index1 toPoint:(int)index2;


-(NSArray *)getBuildConfigById:(int)bid;
@end
