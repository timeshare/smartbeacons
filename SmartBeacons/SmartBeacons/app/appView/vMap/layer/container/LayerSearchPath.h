//
//  LayerSearchPath.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-27.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface LayerSearchPath : CALayer{
    
}

@property(nonatomic,strong)NSMutableArray *pathPoints;

-(void)drawPath:(NSMutableArray *)pathsPoint;



@end
