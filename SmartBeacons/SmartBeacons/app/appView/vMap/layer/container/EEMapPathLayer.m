//
//  EEMapAreaHotLayer.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapPathLayer.h"
#import "Tools_math.h"
#import "EEMapTipCanvas.h"


@implementation EEMapPathLayer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setBackgroundColor:[UIColor clearColor]];
        
        NSArray *array = @[@[@{@"name":@"斯巴鲁",@"id":@"100"},@[@"321",@"636"],@[@"406",@"713"],@[@"455",@"660"],@[@"369",@"584"]],
                           @[@{@"name":@"欧宝",@"id":@"101"},@[@"424",@"728"],@[@"469",@"766"],@[@"515",@"714"],@[@"471",@"674"]],
                           @[@{@"name":@"大众",@"id":@"102"},@[@"473",@"770"],@[@"530",@"822"],@[@"576",@"768"],@[@"519",@"718"]],
                           @[@{@"name":@"保时捷",@"id":@"103"},@[@"300",@"510"],@[@"358",@"560"],@[@"408",@"502"],@[@"352",@"451"]],
                           @[@{@"name":@"宾利",@"id":@"104"},@[@"377",@"575"],@[@"411",@"606"],@[@"458",@"552"],@[@"426",@"522"]],
                           @[@{@"name":@"玛莎拉蒂",@"id":@"105"},@[@"415",@"610"],@[@"461",@"650"],@[@"509",@"598"],@[@"463",@"556"]],
                           @[@{@"name":@"法拉利",@"id":@"106"},@[@"479",@"668"],@[@"521",@"706"],@[@"569",@"654"],@[@"527",@"615"]],
                           @[@{@"name":@"DS",@"id":@"107"},@[@"526",@"710"],@[@"582",@"762"],@[@"629",@"709"],@[@"572",@"656"]],
                           
                           @[@{@"name":@"奔驰",@"id":@"108"},@[@"361",@"442"],@[@"413",@"496"],@[@"478",@"438"],@[@"428",@"380"]],
                           @[@{@"name":@"Jeep",@"id":@"109"},@[@"439",@"370"],@[@"484",@"430"],@[@"544",@"387"],@[@"500",@"325"]],
                           @[@{@"name":@"沃尔沃",@"id":@"112"},@[@"525",@"310"],@[@"559",@"377"],@[@"626",@"345"],@[@"593",@"277"]],
                           @[@{@"name":@"欧宝",@"id":@"115"},@[@"618",@"267"],@[@"638",@"340"],@[@"709",@"320"],@[@"691",@"248"]],
                           @[@{@"name":@"劳斯莱斯",@"id":@"110"},@[@"524",@"479"],@[@"563",@"546"],@[@"618",@"514"],@[@"580",@"448"]],
                           @[@{@"name":@"阿斯顿马丁",@"id":@"113"},@[@"585",@"444"],@[@"622",@"510"],@[@"693",@"470"],@[@"656",@"404"]],
                           
                           @[@{@"name":@"兰博基尼",@"id":@"111"},@[@"600",@"576"],@[@"626",@"623"],@[@"669",@"600"],@[@"642",@"550"]],
                           @[@{@"name":@"雷克萨斯",@"id":@"114"},@[@"648",@"549"],@[@"675",@"596"],@[@"719",@"572"],@[@"690",@"524"]],
                           @[@{@"name":@"摄影图片展",@"id":@"116"},@[@"746",@"238"],@[@"746",@"310"],@[@"834",@"310"],@[@"834",@"238"]],
                           @[@{@"name":@"宝马",@"id":@"117"},@[@"746",@"327"],@[@"746",@"407"],@[@"834",@"407"],@[@"834",@"327"]],
                           @[@{@"name":@"Mini",@"id":@"118"},@[@"746",@"418"],@[@"746",@"492"],@[@"834",@"492"],@[@"834",@"418"]],
                           @[@{@"name":@"凯迪拉克",@"id":@"119"},@[@"746",@"497"],@[@"746",@"593"],@[@"834",@"593"],@[@"834",@"497"]]];
        self.hotAreas = array;
        
        //取得plist文件的内容
        NSArray *parray = [NSArray arrayWithContentsOfFile:[Global assets:@"assets/map/mappath3.plist"]];
        
        NSArray *pathPoints = [self parsePthPoints:parray];
        
        self.pathPoints = pathPoints;
        
        
        index = 0;
        
        
    }
    return self;
}

#pragma mark --- 把配置文件 转成 PathPoint 数组 ---

-(NSMutableArray *)parsePthPoints:(NSArray *)xmlArray{
    //创建一个数组来存放节点
    NSMutableArray *pathPointArray = [NSMutableArray array];
    
    for (int i=0; i<[xmlArray count]; i++) {
        //用一个字典来接收信息
        NSDictionary *oneXML = [xmlArray objectAtIndex:i];
        
        //创建一个EEMapPathPoint类 存放节点信息
        EEMapPathPoint *pathPoint = [[EEMapPathPoint alloc] init];
        pathPoint.x = [[oneXML objectForKey:@"x"] floatValue];
        pathPoint.y = [[oneXML objectForKey:@"y"] floatValue];
        pathPoint.sid = [[oneXML objectForKey:@"id"] intValue];
        pathPoint.isBoxPoint = [[oneXML objectForKey:@"isBoxPoint"] boolValue];
        
        //用一个数组来存放该节点的可达点信息 存放的是NSString 不是节点
        NSMutableArray *xmlFriendArray = [NSMutableArray array];
        for (int j=0; j<[[oneXML objectForKey:@"friend"] count]; j++) {
            NSString *indexString = [[oneXML objectForKey:@"friend"] objectAtIndex:j];
            [xmlFriendArray addObject:indexString];
        }
        pathPoint.friends = xmlFriendArray;
        
        //将节点依次放入数组中
        [pathPointArray addObject:pathPoint];
    }
    
    //遍历节点数组
    for (int i=0; i<[pathPointArray count]; i++) {
        EEMapPathPoint *pathPoint = [pathPointArray objectAtIndex:i];
        NSInteger friendsCount = [pathPoint.friends count];
        
        //遍历该节点的可达点数组 获取sid
        for (int j=0; j<friendsCount; j++) {
            int sid = [[pathPoint.friends objectAtIndex:j] intValue];
            
            //再次遍历节点数组 若节点数组中节点的sid与可达点数组中的sid相同 用节点替换
            //原来pathPoint.friends存放的是NSString 现在存放节点
            for (int m=0; m<[pathPointArray count] ; m++) {
                EEMapPathPoint *p = [pathPointArray objectAtIndex:m];
                if (p.sid == sid) {
                    [pathPoint.friends replaceObjectAtIndex:j withObject:p];
                    continue;
                }
            }
        }
    }
    
    return pathPointArray;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    track();
    
    //画面
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGContextSetLineCap(c, kCGLineCapRound);
    CGContextSetLineWidth(c, 3);
    CGContextSetRGBStrokeColor(c, 1, 0, 0, 0.8);
    
    CGContextSetFillColorWithColor(c, [[UIColor redColor] colorWithAlphaComponent:0.05].CGColor);
    
    for (NSArray *onerect in self.hotAreas) {
        NSInteger count = [onerect count];
        for (int i=0; i<count; i++) {
            if (i==0) {
                //基本信息
                continue;
            }
            if (i==1) {
                CGContextMoveToPoint(c,[[[onerect objectAtIndex:i] objectAtIndex:0] integerValue], [[[onerect objectAtIndex:i] objectAtIndex:1] integerValue]);
            }else{
                CGContextAddLineToPoint(c,[[[onerect objectAtIndex:i] objectAtIndex:0] integerValue], [[[onerect objectAtIndex:i] objectAtIndex:1] integerValue]);
            }
        }
        CGContextFillPath(c);
    }
    
    //画点
    CGContextSetFillColorWithColor(c, [[UIColor blueColor] colorWithAlphaComponent:0.1].CGColor);
    for (EEMapPathPoint *pathPoint in self.pathPoints) {
        float px = pathPoint.x;
        float py = pathPoint.y;
        CGContextFillEllipseInRect(c, CGRectMake(px-4, py-4, 8, 8));
    }
    
    
//    //画线
//    CGContextSetStrokeColorWithColor(c, [UIColor orangeColor].CGColor);
//    if (index<[[self pathPoints] count]) {
//        
//        EEMapPathPoint *pathPoint = [self.pathPoints objectAtIndex:index];
//        float x = pathPoint.x;
//        float y = pathPoint.y;
//        
//        NSArray *friend = pathPoint.friends;
//
//        CGContextBeginPath(c);
//        for (int i=0; i<[friend count]; i++) {
//            CGContextMoveToPoint(c,x,y);
//            
//            EEMapPathPoint *fpoint = [friend objectAtIndex:i];
//            
//            float xx = fpoint.x;
//            float yy = fpoint.y;
//            CGContextAddLineToPoint(c, xx, yy);
//        }
//        CGContextStrokePath(c);
//        
//        index++;
//    }
    
    UIGraphicsEndImageContext();
}


-(EEMapPathPoint *)getPathPointById:(int)pointId{
    for (int m=0; m<[self.pathPoints count] ; m++) {
        EEMapPathPoint *p = [self.pathPoints objectAtIndex:m];
        if (p.sid == pointId) {
            return p;
        }
    }
    return nil;
}

#pragma mark --- 点击事件 ---

-(void)clickStage:(CGPoint)point{
    trace(@"%f %f",point.x,point.y);
    BOOL isFind = NO;
    for (NSArray *onerect in self.hotAreas) {
        NSArray *array = [NSArray arrayWithObjects:[onerect objectAtIndex:1],[onerect objectAtIndex:2],[onerect objectAtIndex:3],[onerect objectAtIndex:4], nil];
        isFind = [Tools_math ee_isPointInRect2:point pints:array];
        if (isFind) {
            trace(@"发现一个%@",[[onerect objectAtIndex:0] objectForKey:@"name"]);
            [self popOneInforWindow:onerect point:point];
            break;
        }
    }
    
    if (!isFind) {//没有找到
        if (self.popoverView) {
            [self.popoverView dismissPopoverAnimated:YES];
        }
    }
}


-(void)popOneInforWindow:(NSArray *)inforData point:(CGPoint)point{
    
    lastClickHotAreaConfig = inforData;
    
    //EEView *contentView = [Global createViewByFrame:R(5,5,120,40) color:CWhite];
    
    EEMapTipCanvas *tipCanvas = [[EEMapTipCanvas alloc] initWithData:[inforData objectAtIndex:0]];
    tipCanvas.delegate = self;

    if (self.popoverView) {
        [[self popoverView] dismissPopoverAnimated:YES];
    }
    self.popoverView = [[FWTPopoverView alloc] init];
    [self.popoverView.contentView addSubview:tipCanvas];
    self.popoverView.contentSize = CGSizeMake(tipCanvas.width+10, tipCanvas.height+10);
//    self.popoverView.suggestedEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
//suggestedEdgeInsets, edgeInsets;
//    __block typeof(self) myself = self;
//    self.popoverView.didDismissBlock = ^(FWTPopoverView *av){ myself.popoverView = nil; };
    CGColorRef fillColor = [UIColor whiteColor].CGColor;
    self.popoverView.backgroundHelper.fillColor = fillColor;
    [self.popoverView presentFromRect:CGRectMake(point.x, point.y, 1.0f, 1.0f)
                               inView:self
              permittedArrowDirection:FWTPopoverArrowDirectionDown
                             animated:YES];
    
}

-(void)disPopOneInforWindow{
    if (self.popoverView) {
        [[self popoverView] dismissPopoverAnimated:YES];
    }
}

/**
 *  设为起点
 *
 *  @param sender sender
 */
-(void)clickStartPoint:(id)sender{
    track();
    [self addStartMarkPoint:lastClickHotAreaConfig];
    //取消tip窗口
    [self disPopOneInforWindow];
}


/**
 *  设为终点
 *
 *  @param sender sender
 */
-(void)clickEndPoint:(id)sender{
    track();

    [self addEndMarkPoint:lastClickHotAreaConfig];
    //取消tip窗口
    [self disPopOneInforWindow];
}

/**
 *  显示详细页面
 *
 *  @param sender sender
 */
-(void)clickDetailInfor:(id)sender{
//    [[NSNotificationCenter defaultCenter] postNotificationName:MapNotice object:@"showOneTipInformation" userInfo:[lastClickHotAreaConfig objectAtIndex:0]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:MapNotice object:@"showOneTipInformation"];
    
    //取消tip窗口
    [self disPopOneInforWindow];
}


-(void)addStartMarkPoint:(NSArray *)arrays{
    if(arrays==nil)return;
    
    [ico_startMark removeFromSuperview];ico_startMark=nil;
    
    CGPoint startMark = [self getCenterPosition:arrays];
    
    int sid = [[[arrays objectAtIndex:0] objectForKey:@"id"] intValue];
    
    startMarkId = sid;
    
    [self addSubview:tmpImgView = [Global createImage:@"assets/image/ico_mapmark1.png"  center:startMark]] ;ico_startMark = tmpImgView;
    //开始寻路
    [self startSearchToEnd];
}

-(void)addEndMarkPoint:(NSArray *)arrays{
    if(arrays==nil)return;
    
    [ico_startMark removeFromSuperview];ico_startMark=nil;
    [ico_endMark removeFromSuperview];ico_endMark=nil;
    
    //设置为中点的话，则永远是以用户当前所在点开始查找
    startMarkId = self.userLocateMarkId;
    
    CGPoint startMark = [self getCenterPosition:arrays];
    
    int sid = [[[arrays objectAtIndex:0] objectForKey:@"id"] intValue];
    endMarkId = sid;

    [self addSubview:tmpImgView = [Global createImage:@"assets/image/ico_mapmark2.png" center:startMark]] ;ico_endMark = tmpImgView;
    //开始寻路
    [self startSearchToEnd];
}

-(void)startSearchToEnd{
//    if (ico_endMark==nil || ico_startMark==nil) {
//        return;
//    }
    if (endMarkId==startMarkId) {
        return;
    }
    
    [self searchPath:startMarkId toPoint:endMarkId];
}
#pragma mark --- 校验点 ---
-(void) checkPathPoint:(id)sender{
    track();
    [self setNeedsDisplay];
    if (index<[self.pathPoints count]) {
        EEMapPathPoint *pathPoint = [self.pathPoints objectAtIndex:index];
        NSMutableDictionary *postPoint = [NSMutableDictionary dictionaryWithObject:pathPoint forKey:@"post"];
        [[NSNotificationCenter defaultCenter] postNotificationName:MapNotice object:@"mapCenterToChanged" userInfo:postPoint];
    }else{
        index=0;
    }
}


#pragma mark --- 寻路算法 ---
-(void)findPathFromUserLocateTo:(int)bid{
    startMarkId = self.userLocateMarkId;
    endMarkId = bid;
    
    [ico_startMark removeFromSuperview];ico_startMark=nil;
    [ico_endMark removeFromSuperview];ico_endMark=nil;
    
    
    CGPoint startMark = [self getCenterPosition:[self getBuildConfigById:bid]];
    [self addSubview:tmpImgView = [Global createImage:@"assets/image/ico_mapmark2.png" center:startMark]] ;ico_endMark = tmpImgView;
    
    [self searchPath:startMarkId toPoint:endMarkId];
}


-(void)resetSearch{
    endPathPoint = nil;
    isFinded = NO;
    
    [self.pathPoints enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        EEMapPathPoint *pathPoint = (EEMapPathPoint *)obj;
        pathPoint.isInSearch = NO;
        pathPoint.parent = nil;
    }];
}

-(void)searchPath:(int)index1 toPoint:(int)index2{
    trace(@"fromindex：%d toindex：%d",index1,index2);
    
    startMarkId = index1;
    endMarkId = index2;
    
    [self resetSearch];
    
    endPathPoint   = [self getPathPointById:index2];
    
    [self findNext:index1];
}

//递归的寻址操作方法
-(void)findNext:(int)fromIndex{
    track();
    if (isFinded) {
        return;
    }
    EEMapPathPoint *startPathPoint = [self getPathPointById:fromIndex];
    
    //排序
    NSMutableArray *friends = startPathPoint.friends;
    
    //遍历可达点   排序数组
    [friends sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        
        EEMapPathPoint *f1PathPoint = (EEMapPathPoint *)obj1;
        EEMapPathPoint *f2PathPoint = (EEMapPathPoint *)obj2;
        
        CGPoint point1 = CGPointMake(f1PathPoint.x,f1PathPoint.y);
        CGPoint point2 = CGPointMake(f2PathPoint.x,f2PathPoint.y);
        
        //过滤优先算法  
        float f1ToEnd = [self distance:point1 to:CGPointMake(endPathPoint.x, endPathPoint.y)] +[self distance:point1 to:CGPointMake(startPathPoint.x, startPathPoint.y)];
        float f2ToEnd = [self distance:point2 to:CGPointMake(endPathPoint.x, endPathPoint.y)] +[self distance:point2 to:CGPointMake(startPathPoint.x, startPathPoint.y)];
        
        if (f1PathPoint.sid==endPathPoint.sid) {
            return NSOrderedAscending;
        }
        if (f2PathPoint.sid==endPathPoint.sid) {
            return NSOrderedDescending;
        }

        
        BOOL f1bool = (f1PathPoint.isBoxPoint && f1PathPoint.sid!=endPathPoint.sid);
        BOOL f2bool = (f2PathPoint.isBoxPoint && f2PathPoint.sid!=endPathPoint.sid);
        
        if (f1bool && f2bool) {
            if (f1ToEnd>f2ToEnd) {
                return NSOrderedDescending;
            }else if(f1ToEnd==f2ToEnd){
                return NSOrderedSame;
            }else{
                return NSOrderedAscending;
            }
        }else if(f1bool && !f2bool){
            return NSOrderedDescending;
        }else if(!f1bool && f2bool){
            return NSOrderedAscending;
        }
        
        if (f1ToEnd>f2ToEnd) {
            return NSOrderedDescending;
        }else if(f1ToEnd==f2ToEnd){
            return NSOrderedSame;
        }else{
            return NSOrderedAscending;
        }

    }];
    
    
    for (int i=0; i<[friends count]; i++) {
        EEMapPathPoint *mappoint = [friends objectAtIndex:i];
        if (mappoint.isInSearch) {
            continue;
        }
        //赋值 环状引用
        mappoint.parent = startPathPoint;
        mappoint.isInSearch = YES;
        
        if (mappoint.sid == endPathPoint.sid) {
            isFinded = YES;
            trace(@"findpath");
            BOOL ttt = YES;
            NSMutableArray *pathPointArray = [NSMutableArray array];//建立数组  储存查询点
            [pathPointArray addObject:[NSValue valueWithCGPoint:CGPointMake(mappoint.x, mappoint.y)]];//终点
            while (ttt) {
                EEMapPathPoint *tmppoint = mappoint.parent;
                [pathPointArray addObject:[NSValue valueWithCGPoint:CGPointMake(tmppoint.x, tmppoint.y)]];
                trace(@"%@",tmppoint);
                mappoint = tmppoint;
                if (tmppoint.parent==nil) {
                    ttt = NO;
                }
            }
            
            //对查询到的路径划线
            [self drawSearchPath:pathPointArray];
        }
        
        [self findNext:mappoint.sid];
    }
}



-(float)distance:(CGPoint)fromPoint to:(CGPoint)endPoint{
    float distance;
    //下面就是高中的数学，不详细解释了
    CGFloat xDist = (endPoint.x - fromPoint.x);
    CGFloat yDist = (endPoint.y - fromPoint.y);
    distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance;
}

/**
 *  划线
 *
 *  @param pathsPoint 点数组
 */
-(void)drawSearchPath:(NSMutableArray *)pathsPoint{
    if (searchPathLayer==nil) {
        searchPathLayer= [[LayerSearchPath alloc] init];
        searchPathLayer.frame = self.bounds;
        [self.layer addSublayer:searchPathLayer];
        [searchPathLayer drawPath:pathsPoint];//划线
        //[searchPathLayer setBackgroundColor:[[UIColor redColor] colorWithAlphaComponent:0.9].CGColor];
    }else{
        [searchPathLayer drawPath:pathsPoint];
    }
}


#pragma ---tools---
-(CGPoint)getCenterPosition:(NSArray *)configArrays{
    if ([configArrays count]==5) {
        //得到矩形的中心点
        return [Tools_math getShapeCenter:CGPointMake([[[configArrays objectAtIndex:1] objectAtIndex:0] floatValue], [[[configArrays objectAtIndex:1] objectAtIndex:1] floatValue])
                                  :CGPointMake([[[configArrays objectAtIndex:2] objectAtIndex:0] floatValue], [[[configArrays objectAtIndex:2] objectAtIndex:1] floatValue])
                                  :CGPointMake([[[configArrays objectAtIndex:3] objectAtIndex:0] floatValue], [[[configArrays objectAtIndex:3] objectAtIndex:1] floatValue])
                                  :CGPointMake([[[configArrays objectAtIndex:4] objectAtIndex:0] floatValue], [[[configArrays objectAtIndex:4] objectAtIndex:1] floatValue])];
    }else{
        return CGPointMake(0, 0);
    }
}

//根据id获取 config
-(NSArray *)getBuildConfigById:(int)bid{
    for (int i=0; i<[self.hotAreas count]; i++) {
        int searchId = [[[[self.hotAreas objectAtIndex:i] objectAtIndex:0] objectForKey:@"id"] intValue];
        if (bid==searchId) {
            return [self.hotAreas objectAtIndex:i];
        }
    }
    //@[@{@"name":@"长安马自达",@"id":@"41"},@[@"127",@"178"],@[@"213",@"193"],@[@"199",@"270"],@[@"112",@"256"]]
    return nil;
}

- (void)dealloc
{
    track();

    self.hotAreas = nil;
    self.pathPoints = nil;
}

@end
