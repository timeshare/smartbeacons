//
//  LayerSearchPath.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-27.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "LayerSearchPath.h"

@implementation LayerSearchPath



-(void)drawInContext:(CGContextRef)ctx{
    
    
    //画面
    CGContextRef c = ctx;
    
    CGContextSetLineCap(c, kCGLineCapRound);
    CGContextSetLineWidth(c, 3);
    
    CGContextSetFillColorWithColor(c, [[UIColor redColor] colorWithAlphaComponent:0.8].CGColor);
    
    //画线 pathsPoint
    CGContextSetStrokeColorWithColor(c, [[UIColor purpleColor] colorWithAlphaComponent:0.8].CGColor);
    
    for (int i=0; i<[self.pathPoints count]; i++) {
        CGPoint pointBegin = [[self.pathPoints objectAtIndex:i] CGPointValue];
        CGContextMoveToPoint(c, pointBegin.x, pointBegin.y);
        if((i+1)<[self.pathPoints count]){
            CGPoint pointEnd = [[self.pathPoints objectAtIndex:(i+1)] CGPointValue];
            CGContextAddLineToPoint(c, pointEnd.x, pointEnd.y);
        }
    }
    CGContextStrokePath(c);
    
    
    UIGraphicsEndImageContext();
}

-(void)drawPath:(NSMutableArray *)pathsPoint{
    
    self.pathPoints = pathsPoint;
    [self setNeedsDisplay];
}


@end
