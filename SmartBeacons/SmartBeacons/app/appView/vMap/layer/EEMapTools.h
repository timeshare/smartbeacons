//
//  EEMapTools.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EEMap;
@interface EEMapTools : NSObject{
    EEButton *btnSearch;
    EEButton *btnShopper;
    EEButton *btnFood;
    EEButton *btnTraffic;
    EEButton *btnSheShi;
    EEButton *btnKeyWord;
}

@property(nonatomic,weak)EEMap *delegate;

-(void)initView;

@end
