//
//  EEMap.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-21.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMap.h"
#import "EEMapPathLayer.h"

#define  searchBtnX 75
#define  searchBtnY 435

@implementation EEMap

static bool isBusy;

static EEMap *eeMap;

+(EEMap *)shareInstance{
    if (eeMap==nil) {
        traceException(@"eemap 为空");
    }
    return eeMap;
}

+(void)cancelInstance{
    eeMap = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(id)mapWithConfigPlist:(NSString *)plistFileName frame:(CGRect)frame{
    EEMap *eemap = [[EEMap alloc] initWithFrame:frame];
    eemap.multipleTouchEnabled = YES;
    eemap.minScaleValue = 0.5;
    eemap.maxScaleValue = 2.0;
    [eemap initView];
    
    return eemap;
}

-(void)initView{
    //缩放容器
    eeMap = self;
    
    contentScroll = [[EEMapScroll alloc] initWithFrame:CGRectMake(0, 0, View_Size.width,View_Size.height-AppButtomHeight)];
    [self addSubview:contentScroll];
    contentScroll.delegate = self;
    contentScroll.multipleTouchEnabled = YES;
    contentScroll.minimumZoomScale = 0.5;
    contentScroll.maximumZoomScale = 1.4;
    contentScroll.showsVerticalScrollIndicator = NO;
    contentScroll.showsHorizontalScrollIndicator = NO;
    contentScroll.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    mapContainer = [[EEMapContainer alloc] initWithSource:@"config.plist"];
    mapContainer.delegate = self;
    [contentScroll addSubview:mapContainer];
//    [self addSubview:mapContainer];
    contentScroll.contentSize = mapContainer.frame.size;
    contentScroll.mapContainer = mapContainer;

//    //按钮层
    mapTools = [[EEMapTools alloc] init];
    mapTools.delegate = self;
    [mapTools initView];
    
    //加入手势
    UITapGestureRecognizer *stageGuesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapStageGesture:)];
    [self addGestureRecognizer:stageGuesture];
    stageGuesture.numberOfTapsRequired = 1;
    stageGuesture.numberOfTouchesRequired = 1;
    
    //UITapGesture
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noticeCenter:) name:MapNotice object:nil];
    
    //默认进入某点
    [self userLocateToPoint:CGPointMake(450, 700) isAutoLocateTo:YES];
    [self userLocateToBuildId:110 isAutoLocateTo:YES];
//    [self mapLocatToPoint:CGPointMake(450, 700)];
    [self mapLocatToUserPoint];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addMark1) name:@"toilet" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addMark2) name:@"qirui" object:nil];

}

-(void)addMark1{
    if(_imgView){
        [_imgView removeFromSuperview];
    }
    UIImage *image = [UIImage imageNamed:@"assets/map/mapmark1.png"];
    _imgView = [[UIImageView alloc]initWithImage:image];
    _imgView.center = CGPointMake(173.5, 557);
    _imgView.alpha = 0.8;
    [self mapLocatToPoint:_imgView.center];
    [mapContainer addSubview:_imgView];
    
}

-(void)addMark2{
    if(_imgView){
        [_imgView removeFromSuperview];
    }
    
    UIImage *image = [UIImage imageNamed:@"assets/map/mapmark1.png"];
    _imgView = [[UIImageView alloc]initWithImage:image];
    _imgView.center = CGPointMake(210, 470);
    _imgView.alpha = 0.8;
    [self mapLocatToPoint:_imgView.center];
    [mapContainer addSubview:_imgView];
    
}

-(void)clickCheckPathPoint:(id)sender{
    [mapContainer.mapPathLayer  checkPathPoint:sender];
}

#pragma mark --- 注册通知 ---
-(void)noticeCenter:(NSNotification *)notice{
    id postTag = notice.object;
    if ([postTag isEqualToString:@"showOneTipInformation"]) {//打开详情界面
        if ([self.delegate respondsToSelector:@selector(mapSelectOneHotAreaInformation:)]) {
            [self.delegate mapSelectOneHotAreaInformation:notice.userInfo];
        }
    }else if([postTag isEqualToString:@"mapCenterToChanged"]){
        EEMapPathPoint *pathPoint = [notice.userInfo objectForKey:@"post"];
        CGPoint centPoint = CGPointMake(pathPoint.x, pathPoint.y);
        [self mapLocatToPoint:centPoint];
    }else if([postTag isEqualToString:@"appRewake"]){
        [mapContainer resetUserLightAnimation];
    }
}


#pragma mark --- 缩放控制 ---
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    for(id subView in scrollView.subviews)
    {
        if([subView isKindOfClass:[EEMapContainer class]])
        {
            return subView;
        }
    }
    
    return nil;
}

#pragma mark --- 移动场景 ---
-(void)tapStageGesture:(UITapGestureRecognizer *)gesture{
    [mapContainer tapStageGesture:gesture];
}

-(void)mapLocatToPoint:(CGPoint)point{
    [contentScroll mapLocatToPoint:point];
}

-(void)mapLocatToUserPoint{
    [contentScroll mapLocatToPoint:mapContainer.userCurrentLocation];
}

#pragma mark --- 人物位置 移动 ---
-(void)userLocateToPoint:(CGPoint)point isAutoLocateTo:(BOOL)isAuto{
    [mapContainer userLocateToPoint:point];
    if (isAuto && ![EEMap isBusy]) {
        [contentScroll mapLocatToPoint:point];
    }
}

-(void)userLocateToBuildId:(int)bid isAutoLocateTo:(BOOL)isAuto{
    CGPoint targetPoint = [mapContainer userLocateToBuidId:bid];
    if (isAuto && ![EEMap isBusy]) {
        [contentScroll mapLocatToPoint:targetPoint];
    }
}

#pragma mark --- 寻路 ---

-(void)findPathFromUserLocateTo:(int)toBid{
    [mapContainer findPathFromUserLocateTo:toBid];
}

-(void)clickFindPath:(id)sender{
    int s1 = arc4random()%53+1;
    int s2 = arc4random()%53+1;
    if (s2==s1) {
        s2=s1-1;
    }
    trace(@"s1,s2: %d %d",s1,s2);
    
    [mapContainer.mapPathLayer searchPath:41 toPoint:53];
}


//设置繁忙。 当用户在操作map的时候，我们可以设置isBusy为yes。 该变量用在一些点位是否移动的判断上
+(void)isBusy:(BOOL)b{
    isBusy = b;
}
+(BOOL)isBusy{
    return isBusy;
}

#pragma mark --- 地图放大缩小 ---
-(void)scaleMapBig{
    [contentScroll setZoomScale:(contentScroll.zoomScale + 0.2) animated:YES];
}

-(void)scaleMapSmall{
    [contentScroll setZoomScale:(contentScroll.zoomScale - 0.2) animated:YES];
}

#pragma mark --- 回收 ---
- (void)dealloc
{
    
    track();
    eeMap = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    mapTools.delegate = nil;
    contentScroll.delegate = nil;
    self.delegate = nil;
    [contentScroll removeFromSuperview];
}


@end
