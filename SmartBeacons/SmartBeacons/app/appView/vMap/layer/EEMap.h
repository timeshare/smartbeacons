//
//  EEMap.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-21.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import "EEMapContainer.h"
#import "EEMapTools.h"
#import "EEMapScroll.h"

@protocol EEMapDelegate <NSObject>

-(void)mapSelectOneHotAreaInformation:(NSDictionary *)configData;

@end


@interface EEMap : UIView<UIScrollViewDelegate>{
    EEMapContainer *mapContainer;
    
    EEMapTools *mapTools;
    
    EEMapScroll *contentScroll;
    
}
@property(nonatomic,strong) UIImageView *imgView;

@property(nonatomic)CGFloat minScaleValue;
@property(nonatomic)CGFloat maxScaleValue;

@property(nonatomic,weak)id delegate;


+(void)cancelInstance;
+(EEMap *)shareInstance;

-(void)mapLocatToPoint:(CGPoint)point;

-(void)mapLocatToUserPoint;
+(id)mapWithConfigPlist:(NSString *)plistFileName frame:(CGRect)frame;

-(void)userLocateToPoint:(CGPoint)point isAutoLocateTo:(BOOL)isAuto;
-(void)userLocateToBuildId:(int)bid isAutoLocateTo:(BOOL)isAuto;

-(void)findPathFromUserLocateTo:(int)toBid;

+(void)isBusy:(BOOL)b;
+(BOOL)isBusy;

-(void)scaleMapBig;

-(void)scaleMapSmall;
    
@end
