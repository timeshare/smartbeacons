//
//  EEMapTools.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapTools.h"
#import "EEMap.h"
#import "EENoticeMessage.h"


@implementation EEMapTools

int cha3 = 25;
-(void)initView{
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(270, 300-20, 32, 34) normal:@"assets/map/point.png" target:self action:@selector(clickMySelfLocation:)]];
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(270, 300+25, 32, 33.5) normal:@"assets/map/plus.png" target:self action:@selector(clickPlusMap:)]];
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(270, 300+60, 32, 33.5) normal:@"assets/map/minus.png" target:self action:@selector(clickCutMap:)]];
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(15, 20, 28, 28) normal:@"assets/map/compass.png" target:self action:nil]];
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(15, 50, 28, 28) normal:@"assets/map/f1_on.png" target:self action:nil]];
    
    [self.delegate addSubview:[Global createBtn:CGRectMake(15, 80, 28, 28) normal:@"assets/map/f2_off.png" target:self action:nil]];
    
//    int w1 = 65;
//    btnSearch = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_search.png" target:self action:@selector(clickShowSearchBtn:)];btnSearch.alpha = 0.9;
//    btnShopper = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_shop.png" target:self action:@selector(clickOthers:)];btnShopper.tag = 1;
//    btnFood = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_food.png" target:self action:@selector(clickOthers:)];btnFood.tag = 2;
//    btnTraffic = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_jiaotong.png" target:self action:@selector(clickOthers:)];btnTraffic.tag = 3;
//    btnSheShi = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_sheshi.png" target:self action:@selector(clickOthers:)];btnSheShi.tag =4;
//    btnKeyWord = [Global createBtn:CGRectMake(10, 0, w1, w1) normal:@"assets/image/mapico_keyword.png" target:self action:@selector(clickOthers:)];btnKeyWord.tag = 5;
//    
//    btnSearch.center        = ccp(280, 66-cha3);
//    btnShopper.center        = ccp(280, 66-cha3);
//    btnFood.center          = ccp(280, 66-cha3);
//    btnTraffic.center       = ccp(280, 66-cha3);
//    btnSheShi.center        = ccp(280, 66-cha3);
//    btnKeyWord.center       = ccp(280, 66-cha3);
//    
//    btnShopper.alpha = btnFood.alpha = btnTraffic.alpha = btnSheShi.alpha =  btnKeyWord.alpha = 0;
//    
//    [self.delegate addSubview:btnSearch];
//    [self.delegate addSubview:btnShopper];
//    [self.delegate addSubview:btnFood];
//    [self.delegate addSubview:btnTraffic];
//    [self.delegate addSubview:btnSheShi];
//    [self.delegate addSubview:btnKeyWord];
    
    [self.delegate bringSubviewToFront:btnSearch];
}





//-(void)clickShowSearchBtn:(EEButton *)btn{
//    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
//    [UIView animateWithDuration:0.45 animations:^{
//        if (btnShopper.alpha==1) {
//            
//            btnShopper.center = ccp(280, 66-cha3);
//            btnFood.center = ccp(280, 66-cha3);
//            btnTraffic.center = ccp(280, 66-cha3);
//            btnSheShi.center = ccp(280, 66-cha3);
//            btnKeyWord.center = ccp(280, 66-cha3);
//            
//            btnShopper.alpha = 0;
//            btnFood.alpha = 0;
//            btnTraffic.alpha = 0;
//            btnSheShi.alpha = 0;
//            btnKeyWord.alpha = 0;
//        }else if(btnShopper.alpha==0){
//            
//            
//            btnShopper.center = ccp(280, 137-cha3);
//            btnFood.center = ccp(280, 198-cha3);
//            btnTraffic.center = ccp(280, 256-cha3);
//            btnSheShi.center = ccp(280, 317-cha3);
//            btnKeyWord.center = ccp(280, 375-cha3);
//            
//            btnShopper.alpha = 1;
//            btnFood.alpha = 1;
//            btnTraffic.alpha = 1;
//            btnSheShi.alpha = 1;
//            btnKeyWord.alpha = 1;
//        }
//    }];
//}
//
//-(void)clickOthers:(EEButton *)btn{
//    switch (btn.tag) {
//        case 1:
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"shop" object:nil];
//            break;
//        case 2:
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"food" object:nil];
//            break;
//        case 3:
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"traffic" object:nil];
//            break;
//        case 4:
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"sheshi" object:nil];
//            break;
//        case 5:
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"keyword" object:nil];
//            break;
//        default:
//            break;
//    }
//}


-(void)clickMySelfLocation:(id)sender{
    
    [[EEMap shareInstance] mapLocatToUserPoint];
}

-(void)clickPlusMap:(id)sender{
    track();
    
    [self.delegate scaleMapBig];
}
-(void)clickCutMap:(id)sender{
    track();
    [self.delegate scaleMapSmall];
}

- (void)dealloc
{
    track();
}



@end

