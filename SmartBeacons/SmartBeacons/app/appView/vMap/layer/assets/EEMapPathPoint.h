//
//  EEMapPathPoint.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-26.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EEMapPathPoint : NSObject

@property(nonatomic)BOOL isInSearch;
@property(nonatomic)float x;
@property(nonatomic)float y;
@property(nonatomic)int sid;

@property(nonatomic)float distance;

@property(nonatomic,strong)NSMutableArray *friends;
@property(nonatomic,weak)id parent;
@property(nonatomic)BOOL isBoxPoint;
@end
