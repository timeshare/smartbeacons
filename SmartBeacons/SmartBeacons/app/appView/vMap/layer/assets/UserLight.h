//
//  UserLight.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-2.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserLight : UIView{
    EEImageView *aroundRage;
}

+(id)instance;

-(void)scaleRange;

@end
