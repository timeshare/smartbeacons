//
//  EEMapTipCanvas.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-28.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapTipCanvas.h"

@implementation EEMapTipCanvas

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(id)initWithData:(NSDictionary *)dataConfig{
    self = [super init];
    
    self.dataconfig = dataConfig;
    
    [self addSubview:tmpLabel = [Global createLabel:CGRectMake(0, 0, 110, 30) label:[dataConfig objectForKey:@"name"] lines:0 fontSize:14 fontName:nil textcolor:RGB(100, 100, 100) align:NSTextAlignmentCenter]];
    
    //增加按钮
//    [self addSubview:[Global createBtn:R(0, 35, 50, 25) normal:@"assets/image/btnbg2.png" down:@"assets/image/btnbg3.png" title:@"设为起点" col:RGB(255, 255, 255) size:12 target:self action:@selector(clickStartPoint:)]];
    
    [self addSubview:[Global createBtn:R(0, 35, 50, 25) normal:@"assets/image/btnbg2.png"  down:@"assets/image/btnbg3.png" title:@"设为终点" col:RGB(255, 255, 255) size:12 target:self action:@selector(clickEndPoint:)]];
    [self addSubview:[Global createBtn:R(60, 35, 50, 25) normal:@"assets/image/btnbg2.png" down:@"assets/image/btnbg3.png"  title:@"详细信息" col:RGB(255, 255, 255) size:12 target:self action:@selector(clickDetailInfor:)]];

    
    [self setFrame:CGRectMake(5, 5, 110, 60)];
    return self;
}

//-(void)clickStartPoint:(id)sender{
//    
//    [self sendAction:@selector(clickStartPoint:) to:self.delegate forEvent:nil];
//}

-(void)clickEndPoint:(id)sender{
    [self sendAction:@selector(clickEndPoint:) to:self.delegate forEvent:nil];
}

-(void)clickDetailInfor:(id)sender{
    [self sendAction:@selector(clickDetailInfor:) to:self.delegate forEvent:nil];
}

- (void)dealloc
{
    track();
    self.dataconfig = nil;
}

@end
