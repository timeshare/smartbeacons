//
//  EEMapPathPoint.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-26.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapPathPoint.h"

@implementation EEMapPathPoint

- (void)dealloc
{
    self.friends = nil;
}

-(NSString *)description{
    return [NSString stringWithFormat:@"sid:%d  isInSearch:%d isBoxPoint:%d",self.sid,self.isInSearch,self.isBoxPoint];
}

@end
