//
//  EEMapTipCanvas.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-28.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EEMapTipCanvas : UIControl

@property(nonatomic,strong)NSDictionary *dataconfig;

@property(nonatomic,weak)id delegate;


-(id)initWithData:(NSDictionary *)dataConfig;

@end
