//
//  UserLight.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-2.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "UserLight.h"

#define userlightdefaulescale 0.5

@implementation UserLight

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(id)instance{
    UserLight *userLight = [[UserLight alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    userLight.clipsToBounds = NO;
    [userLight initView];
    
    return userLight;
}


-(void)initView{
    [self addSubview:[Global createImage:@"assets/image/hotpoint.png" fitSize:CGSizeMake(30,30) center:CGPointMake(5, 5)]];
    aroundRage = [Global createImage:@"assets/image/hotround.png" center:CGPointMake(5, 5)];
    [aroundRage setTransform:CGAffineTransformMakeScale(userlightdefaulescale, userlightdefaulescale)];
    [self addSubview:aroundRage];
    [self sendSubviewToBack:aroundRage];
    
    [self scaleRange];
}


-(void)scaleRange{
    //    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //    rotationAnimation.toValue = [NSNumber numberWithFloat:(2 * M_PI) * 2];
    //    rotationAnimation.duration = 1.0f;
    //    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    
    CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleAnimation.toValue = [NSNumber numberWithFloat:0.0];
    scaleAnimation.fromValue = [NSNumber numberWithFloat:userlightdefaulescale];
    scaleAnimation.duration = 1.0f;
    scaleAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    CAAnimationGroup *animationGroup = [CAAnimationGroup animation];
    animationGroup.duration = 1.0f;
    animationGroup.autoreverses = YES;
    animationGroup.repeatCount = 0x1e100f;
    animationGroup.animations =[NSArray arrayWithObjects:scaleAnimation, nil];
    [aroundRage.layer addAnimation:animationGroup forKey:@"animationGroup"];
}



- (void)dealloc
{
    track();
}


@end
