//
//  EEMapContainer.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-21.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EEMapPathLayer.h"
#import "UserLight.h"

@interface EEMapContainer : UIView{
    UIImageView *mapView;

    UserLight *userLight;
}

@property(nonatomic,weak)id delegate;

@property(nonatomic,strong)EEMapPathLayer *mapPathLayer;

@property(nonatomic)CGFloat scaleNum;

@property(nonatomic)CGPoint userCurrentLocation;


-(void)findPathFromUserLocateTo:(int)toBid;

-(void)userLocateToPoint:(CGPoint)point;
-(CGPoint)userLocateToBuidId:(int)bid;

-(id)initWithSource:(NSString *)fileName;

-(void)tapStageGesture:(UITapGestureRecognizer *)gesture;

-(void)resetUserLightAnimation;

@end
