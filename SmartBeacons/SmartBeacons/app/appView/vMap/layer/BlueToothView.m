//
//  BlueToothView.m
//  SmartBeacons
//
//  Created by Alex on 14-9-26.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BlueToothView.h"
#import "AppView.h"

@implementation BlueToothView{
    UIPageControl *_pagecontrol;
}

-(void)initView{
//    self.frame = CGRectMake(0, 0, View_Size.width, View_Size.height);
//     [self setBackgroundColor:CWhite];
//    
//    UIImage *image = [UIImage imageNamed:@"assets/image/bluetooth.png"];
//    UIImageView *imgview = [[UIImageView alloc]initWithImage:image];
//    imgview.frame = CGRectMake(0, 0, View_Size.width, 416.5);
//    [self addSubview:imgview];
//    
//    UIButton *blueTooth = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 155, 38.5)];
//    blueTooth.center = CGPointMake(View_Size.width/2, 470);
//    [blueTooth setBackgroundImage:[UIImage imageNamed:@"assets/image/gogo.png"] forState:UIControlStateNormal];
////    [blueTooth setBackgroundImage:[UIImage imageNamed:@"assets/image/gogo2.png"] forState:UIControlStateHighlighted];
//    [blueTooth addTarget:self action:@selector(mainpage) forControlEvents:UIControlEventTouchUpInside];
//    [self addSubview:blueTooth];

    [self setupScrollview];
    [self setupPageControl];
}

-(void)setupScrollview{
    UIScrollView *scrollview = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    scrollview.delegate = self;
    [self addSubview:scrollview];
    scrollview.pagingEnabled = YES;
    scrollview.showsHorizontalScrollIndicator = NO;
    scrollview.contentSize = CGSizeMake(View_Size.width*2, View_Size.height);
    for (int i=0; i<2; i++) {
        UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(320*i, 0, View_Size.width, View_Size.height)];
        imageview.image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/image/guide%d.png",i+1]];
        [scrollview addSubview:imageview];
    }
    [scrollview addSubview:tmpButtom = [Global createBtn:CGRectMake(0, 0, 155, 38.5) normal:@"assets/image/gogo.png" down:@"assets/image/gogo.png" target:self action:@selector(mainpage)]];
    tmpButtom.center = CGPointMake(320+View_Size.width/2, 440);
}

-(void)setupPageControl{
    _pagecontrol = [[UIPageControl alloc]initWithFrame:CGRectMake(0, View_Size.height-140, 0, 0)];
    _pagecontrol.numberOfPages = 2;
    _pagecontrol.currentPage = 0;
//    _pagecontrol.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"assets/image/dot.png"]];
//    _pagecontrol.backgroundColor = [UIColor yellowColor];
    _pagecontrol.enabled = NO;
    [self addSubview:_pagecontrol];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    int page = scrollView.contentOffset.x/scrollView.frame.size.width;
    _pagecontrol.currentPage = page;
}

-(void)mainpage{
    GO(@"AppView");
}
@end
