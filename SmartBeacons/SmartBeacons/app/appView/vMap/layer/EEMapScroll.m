//
//  EEMapScroll.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEMapScroll.h"
#import "EEMapContainer.h"

@implementation EEMapScroll


-(void)mapLocatToPoint:(CGPoint)point{
    
    float scaleNum = self.mapContainer.transform.a;
    CGPoint targetPoint = CGPointMake(point.x*scaleNum, point.y*scaleNum);
    
    float tx = self.width*0.5-targetPoint.x;
    float ty = self.height*0.5-targetPoint.y;
    
    float resultX = tx;
    float resultY = ty;
    
    if (resultY>0) {
        ty = 0;
    }
    if (resultY<self.height-self.mapContainer.height) {
        ty = (self.height-self.mapContainer.height);
    }
    if (resultX>0) {
        tx = 0;
    }
    if (resultX<self.width-self.mapContainer.width) {
        tx = (self.width-self.mapContainer.width);
    }
    
    [self setContentOffset:CGPointMake(-tx, -ty) animated:YES];
    
//    trace(@"%@",[NSValue valueWithCGAffineTransform:self.mapContainer.transform]);
}

- (void)dealloc
{
    track();
}

@end
