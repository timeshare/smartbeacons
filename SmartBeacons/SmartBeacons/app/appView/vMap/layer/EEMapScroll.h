//
//  EEMapScroll.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-22.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
@class EEMapContainer;


@interface EEMapScroll : UIScrollView{
    
}

@property(nonatomic,weak)EEMapContainer *mapContainer;

-(void)mapLocatToPoint:(CGPoint)point;

@end
