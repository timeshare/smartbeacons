//
//  TrafficView.m
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "TrafficView.h"

@implementation TrafficView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"交  通";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    UIImage *trafficImage = [UIImage imageNamed:@"assets/map/交通.png"];
    UIImageView *view = [[UIImageView alloc]initWithImage:trafficImage];
    view.frame = CGRectMake(0, 44, View_Size.width, View_Size.height-AppButtomHeight-44);
    [self addSubview:view];
    
    
}

@end
