//
//  SheshiView.m
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "SheshiView.h"

@implementation SheshiView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"设  施";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //添加按钮
    for(int i=0; i<3; i++){
        for(int j=0; j<3; j++){
            if(j!=1){
                if(j==0){
                    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 44+i*114, 106.5, 114)];
                    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/map/设施-icon%d.png",i*3+j+1]];
                    btn.tag = i*3+j+1;
                    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
                    [btn setBackgroundImage:image forState:UIControlStateNormal];
                    [self addSubview:btn];
                    
                }else{
                    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(106.5+107, 44+i*114, 106.5, 114)];
                    UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/map/设施-icon%d.png",i*3+j+1]];
                    btn.tag = i*3+j+1;
                    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
                    [btn setBackgroundImage:image forState:UIControlStateNormal];
                    [self addSubview:btn];
                }
            }else{
                UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(106.5, 44+i*114, 107, 114)];
                UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"assets/map/设施-icon%d.png",i*3+j+1]];
                btn.tag = i*3+j+1;
                [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
                [btn setBackgroundImage:image forState:UIControlStateNormal];
                [self addSubview:btn];
            }
        }
    }
    
}

-(void)btnAction:(UIButton *)btn{
    switch (btn.tag) {
        case 1:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"toilet" object:nil];
            [self eeRemoveRight];
            break;
            
        default:
            break;
    }
}

- (void)dealloc
{
    track();
}


@end
