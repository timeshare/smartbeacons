//
//  VMap.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "VMap.h"
#import "EEBeacon.h"
#import "EENoticeMessage.h"
#import "ShopInforMation.h"
#import "ShopView.h"
#import "FoodView.h"
#import "TrafficView.h"
#import "SheshiView.h"


@implementation VMap

+(id)instance{
     VMap *view  = [[VMap alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [view initView];
    return view;
}


-(void)initView{
    [self setBackgroundColor:CWhite];
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    
//    _webview = [[UIWebView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight)];
//    _webview.scalesPageToFit = YES;
//    [self addSubview:_webview];
//    
//    NSURL *url = [NSURL URLWithString:@"http://www.baidu.com"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    [_webview loadRequest:request];
    
    [MapNoticeManager shareInstance].vmap = self;
    
    //beacon广播
    [EEBeacon startListener:self];
    
    //地图显示
    eeMap = [EEMap mapWithConfigPlist:@"map.plist" frame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight)];
    eeMap.delegate = self;
    [self addSubview:eeMap];
    
    //监听顶部通知框点击事件
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(eeNoticeMessage:) name:@"EENoticeMessage" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goShopView) name:@"shop" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goFoodView) name:@"food" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goTrafficView) name:@"traffic" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goSheshiView) name:@"sheshi" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goKeywordView) name:@"keyword" object:nil];
    
    //添加手势响应
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGr];
}

-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}

-(void)viewTapped:(UITapGestureRecognizer *)tapGr{
    [_searchBar resignFirstResponder];
}


-(void)goShopView{
    ShopView *shopView = [[ShopView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [self addTransition:1];
    [self addSubview:shopView];
    
}

-(void)goFoodView{
    FoodView *foodView = [[FoodView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [self addTransition:1];
    [self addSubview:foodView];
    
}

-(void)goTrafficView{
    TrafficView *trafficView = [[TrafficView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [self addTransition:1];
    [self addSubview:trafficView];
    
    
}

-(void)goSheshiView{
    SheshiView *sheshiView = [[SheshiView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [self addTransition:1];
    [self addSubview:sheshiView];
    
}

-(void)goKeywordView{
    if(_isSearch == NO){
        //添加searchBar
        self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
        _searchBar.placeholder = @"请输入关键字...";
        _searchBar.delegate = self;
        _searchBar.showsCancelButton = YES;
        _searchBar.searchBarStyle = UISearchBarStyleDefault;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        [self addSubview:self.searchBar];
        [self.searchBar becomeFirstResponder];
        _isSearch = YES;
    }else if(_isSearch==YES){
    
        [self.searchBar removeFromSuperview];
        _isSearch = NO;
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"搜索");
    [_searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    NSLog(@"取消");
    
    [self.searchBar removeFromSuperview];
    _isSearch = NO;
 
    [_searchBar resignFirstResponder];
}

//#pragma mark --- beacon delegate ---
//-(void)eeBeaconNoticeMainbeacon:(CLBeacon *)nearestBeacon{
//    track();
//    //MapNoticeManager 这个是专门用来管理通知信息的
//    [[MapNoticeManager shareInstance] catchIbeaconNotice:nearestBeacon];
//    
//}

-(void)setuserPosition:(CGPoint)toPoiont{
    [eeMap userLocateToPoint:toPoiont isAutoLocateTo:NO];
}

-(void)setuserPositionBuilderId:(int)bid isAuto:(BOOL)isauto{
    [eeMap userLocateToBuildId:bid isAutoLocateTo:isauto];
}



#pragma mark --- notice message ---
-(void)eeNoticeMessage:(NSNotification *)notice{
    if ([notice.object isEqualToString:@"selectedOneMessage"]) {

        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/shop/shoplist.plist" ofType:nil];
        NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
        
        ShopInforMation *shopInformation = [ShopInforMation instanceByData:[arrayList objectAtIndex:0]];
        [shopInformation addTransition:1];
        [[RootController sharedRootViewController].view addSubview:shopInformation];
    }
}

#pragma mark --- 弹出详细信息页面 ---
-(void)mapSelectOneHotAreaInformation:(NSDictionary *)configData{
    
    NSString *filePath = [[NSBundle mainBundle]pathForResource:@"assets/shop/shoplist.plist" ofType:nil];
    NSMutableArray *arrayList = [NSMutableArray arrayWithContentsOfFile:filePath];
    
    ShopInforMation *shopInformation = [ShopInforMation instanceByData:[arrayList objectAtIndex:0]];
    [shopInformation addTransition:1];
    [[RootController sharedRootViewController].view addSubview:shopInformation];
}


-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
//    
//    [eeMap removeFromSuperview];
//    [EEMap cancelInstance];
//    eeMap = nil;
    track();
    
    //beacon广播
    [EEBeacon startListener:[Global sharedGlobal].appView];
}


@end
