//
//  User.m
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "UserInfo.h"
#import "ITTObjectSingleton.h"


@implementation UserInfo
ITTOBJECT_SINGLETON_BOILERPLATE(UserInfo, shareUserInfo)

- (id)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appLicationEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [self loadLastUserInfo];
    }
    return self;
}

- (void)loadLastUserInfo
{
    NSString *key = [[NSUserDefaults standardUserDefaults]objectForKey:@"LAST_USER"];
    if(key != nil){
        NSDictionary *dicInfo = [[NSUserDefaults standardUserDefaults] objectForKey:key];
        if (dicInfo) {
            _uid = dicInfo[@"uid"];
            _name = dicInfo[@"name"];
            _nickName = dicInfo[@"nickName"];
            _headImg = dicInfo[@"headImg"];
            _phoneNo = dicInfo[@"phoneNo"];
            _isLogin = [dicInfo[@"isLogin"] boolValue];
            _shareType = [dicInfo[@"shareType"] integerValue];
        }else{
            [self loadDefaultUserInfo];
        }
    }else{
        [self loadDefaultUserInfo];
    }
}

-(void)updateLoginInfo
{
    NSDictionary *dicInfo = @{@"uid":_uid,
                              @"name":_name,
                              @"nickName":_nickName,
                              @"headImg":_headImg,
                              @"phoneNo":_phoneNo,
                              @"shareType":[NSNumber numberWithInteger:_shareType],
                              @"isLogin":[NSNumber numberWithBool:_isLogin]
                              };
    [[NSUserDefaults standardUserDefaults]setObject:dicInfo forKey:_uid];
    [[NSUserDefaults standardUserDefaults]setObject:_uid forKey:@"LAST_USER"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

-(void)loadDefaultUserInfo
{
    _uid = @"1";
    _name = @"";
    _nickName = @"";
    _headImg = @"";
    _phoneNo = @"";
    _isLogin = NO;
    _shareType = 0;
}

-(void)logout
{
    _isLogin = NO;
    [self updateLoginInfo];
}

- (void)appLicationEnterBackGround:(NSNotification *)nsti
{
    [self updateLoginInfo];
}

@end
