//
//  DataBaseView.m
//  SmartBeacons
//
//  Created by mac on 14-11-20.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "DataBaseView.h"
#import "DataBaseCell.h"
#import "PdfReadView.h"
@implementation DataBaseView
{
    UITableView *_tableView;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:  [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"我的资料库";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];

    //加入tableview
    _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight) style:UITableViewStylePlain];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = RGB(234, 234, 234);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self addSubview:_tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"databasecell";
    DataBaseCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell){
        cell = [[NSBundle mainBundle]loadNibNamed:@"DataBaseCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == 0) {
        
    }else if (indexPath.row == 1){
        cell.carLogoImg.image = [UIImage imageNamed:@"database_logo2.png"];
        cell.carMaker.text = @"保时捷";
        cell.carMakerEg.text = @"PORSCHE";
        
    }else if (indexPath.row == 2){
        cell.carLogoImg.image = [UIImage imageNamed:@"database_logo1@2x.png"];
        cell.carMaker.text = @"保时捷";
        cell.carMakerEg.text = @"PORSCHE";
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PdfReadView *pdf = [[PdfReadView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [pdf addTransition:1];
    [self addSubview:pdf];
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}

@end
