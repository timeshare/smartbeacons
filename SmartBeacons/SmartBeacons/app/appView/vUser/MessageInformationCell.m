//
//  MessageInformationCell.m
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "MessageInformationCell.h"

@implementation MessageInformationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initCellByData:(NSMutableDictionary *)dataDictionary{
    self = [super init];
    if(self){
        self.messageData = dataDictionary;
        
        NSString *imageFile = [self.messageData objectForKey:@"infologopic"];
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(115, 0, 98, 87)];
        [image setImage:[UIImage imageNamed:imageFile]];
        [self addSubview:image];
        
        NSString *title = [self.messageData objectForKey:@"title"];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 10, 200, 20)];
        titleLabel.text = title;
        titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [titleLabel setTextColor:[UIColor grayColor]];
        [self addSubview:titleLabel];
        
        NSString *time = [self.messageData objectForKey:@"time"];
        UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(220, 10, 250, 20)];
        timeLabel.text = time;
        timeLabel.font = [UIFont systemFontOfSize:14];
        [timeLabel setTextColor:[UIColor grayColor]];
        [self addSubview:timeLabel];
        
        NSString *description = [self.messageData objectForKey:@"description"];
        UILabel *descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 23, 300, 60)];
        descriptionLabel.text = description;
        descriptionLabel.numberOfLines = 2;
        descriptionLabel.font = [UIFont systemFontOfSize:14];
        [descriptionLabel setTextColor:[UIColor grayColor]];
        [self addSubview:descriptionLabel];
    }
    return  self;
}

@end
