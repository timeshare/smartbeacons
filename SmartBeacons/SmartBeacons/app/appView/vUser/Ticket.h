//
//  Ticket.h
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "BaseClass.h"

@interface Ticket : EEView<UITableViewDataSource,UITableViewDelegate>


@property(nonatomic,strong)NSMutableArray *configList;

@property(nonatomic, strong)UITableView *contentTable;

@end
