//
//  Setting.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Setting.h"
#import "TableViewCell.h"
#import "ChangePassword.h"
@implementation Setting
{
    UISwitch *_switch;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"设    置";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    _tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, 320, View_Size.height)style:UITableViewStylePlain];
    _tableview.delegate = self;
    _tableview.dataSource = self;
    _tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:_tableview];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Scell = @"settingcell";
    TableViewCell *cell = [tableView dequeueReusableHeaderFooterViewWithIdentifier:Scell];
    if(cell == nil){
        cell = [[NSBundle mainBundle]loadNibNamed:@"TableViewCell" owner:nil options:nil][0];
    }
    
    if(indexPath.row == 0){
//        [cell.iconBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
//        cell.itemTitle.text = @"";
//        cell.rightIcon.hidden =YES;
    }else if(indexPath.row == 1){
        [cell.iconBtn setImage:[UIImage imageNamed:@"set_delete.png"] forState:UIControlStateNormal];
        cell.itemTitle.text = @"清除缓存";
        cell.rightIcon.hidden =YES;
       
//    }else if(indexPath.row == 2){
//        [cell.iconBtn setImage:[UIImage imageNamed:@"set_broadcast_enabled.png"] forState:UIControlStateNormal];
//        cell.itemTitle.text = @"推送开关";
//        cell.rightIcon.hidden =YES;
//        CGRect f = cell.rightIcon.frame;
//        if (_switch == nil) {
//            _switch = [[UISwitch alloc]initWithFrame:CGRectMake(View_Size.width - 70, f.origin.y - 5, 0, 0)];
//            _switch.onTintColor = RGB(132, 190, 93);
//            _switch.on = YES;
//            [cell.contentView addSubview:_switch];
//        }

    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.row == 0){
        ChangePassword *cPwd = [[ChangePassword alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height - StatusHeight)];
        [cPwd addTransition:1];
        [self addSubview:cPwd];
    }else if(indexPath.row == 1){
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"确定要清空缓存吗" delegate:self cancelButtonTitle:@"否" otherButtonTitles:@"是", nil];
        [alert show];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}
@end
