//
//  ChangePassword.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "ChangePassword.h"

@implementation ChangePassword

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
        NSLog(@"%@",NSStringFromCGRect(self.frame));
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:  [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"修  改  密  码";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //UITextField--电话
    UITextField *phone = [[UITextField alloc]initWithFrame:CGRectMake(45, 60, 260, 40)];
    phone.backgroundColor = [UIColor whiteColor];
    phone.placeholder = @"输入您绑定的手机号";
    [self addSubview:phone];
    
    UIImageView *phoneView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 60, 30, 40)];
    [phoneView setBackgroundColor:[UIColor whiteColor]];
    UIImage *phoneImg = [UIImage imageNamed:@"assets/image/phone.png"];
    [phoneView setImage:phoneImg];
    phoneView.contentMode =  UIViewContentModeCenter;
    [self addSubview:phoneView];
    
    //验证按钮
    [self addSubview:tmpButtom=[Global createBtn:CGRectMake(15, 115, 290, 45) normal:@"assets/image/blueBtnBack.png" down:nil title:@"验 证" col:[UIColor whiteColor] size:20 target:self action:nil]];tmpButtom.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    
    
    //UITextField--密码
    UITextField *password = [[UITextField alloc]initWithFrame:CGRectMake(45, 180, 260, 40)];
    password.backgroundColor = [UIColor whiteColor];
    password.placeholder = @"输入您的新密码";
    password.secureTextEntry = YES;
    [self addSubview:password];
    
    UIImageView *passwordView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 180, 30, 40)];
    [passwordView setBackgroundColor:[UIColor whiteColor]];
    UIImage *passwordImg = [UIImage imageNamed:@"assets/image/password.png"];
    [passwordView setImage:passwordImg];
    passwordView.contentMode =  UIViewContentModeCenter;
    [self addSubview:passwordView];
    
    //UITextField--确认密码
    UITextField *password2 = [[UITextField alloc]initWithFrame:CGRectMake(45, 230, 260, 40)];
    password2.backgroundColor = [UIColor whiteColor];
    password2.placeholder = @"重新输入您的新密码";
    password2.secureTextEntry = YES;
    [self addSubview:password2];
    
    UIImageView *passwordView2 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 230, 30, 40)];
    [passwordView2 setBackgroundColor:[UIColor whiteColor]];
    UIImage *passwordImg2 = [UIImage imageNamed:@"assets/image/password.png"];
    [passwordView2 setImage:passwordImg2];
    passwordView2.contentMode =  UIViewContentModeCenter;
    [self addSubview:passwordView2];
    
    //修改密码按钮
    [self addSubview:tmpButtom=[Global createBtn:CGRectMake(15, 290, 290, 45) normal:@"assets/image/blueBtnBack.png" down:nil title:@"修改密码" col:[UIColor whiteColor] size:20 target:self action:nil]];tmpButtom.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    
    [self addSubview:tmpButtom=[Global createBtn:CGRectMake(15, 350, 140, 40) normal:@"assets/user/login.png" down:nil title:@"登 陆" col:[UIColor whiteColor] size:20 target:self action:nil]];tmpButtom.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    
    [self addSubview:tmpButtom=[Global createBtn:CGRectMake(165, 350, 140, 40) normal:@"assets/user/register.png" down:nil title:@"注 册" col:[UIColor whiteColor] size:20 target:self action:nil]];tmpButtom.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    
    
}

-(void)confirm{
    NSLog(@"已发送");
}

-(void)change{
    NSLog(@"已修改");
}







@end
