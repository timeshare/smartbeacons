//
//  UserItemBtn.m
//  SmartBeacons
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "UserItemBtn.h"
#define kTitleRatio 0.45
@implementation UserItemBtn

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //1.文字居中,top, 字体大小
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:14];
        [self setTitleColor:RGB(135, 164, 135) forState:UIControlStateNormal];
        
        //2.图片的内容模式
        self.imageView.contentMode = UIViewContentModeBottom;
        
        //3.设置选中时的背景
        
    }
    return self;
}

- (void)setHighlighted:(BOOL)highlighted
{

}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    CGFloat imageX = 0;
    CGFloat imageY = 0;
    CGFloat imageWidth = contentRect.size.width;
    CGFloat imageHeight = contentRect.size.height * (1 - kTitleRatio);
    return CGRectMake(imageX, imageY, imageWidth, imageHeight);
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    CGFloat titleX = 0;
    CGFloat titleHeight = contentRect.size.height*kTitleRatio;
    CGFloat titleY = contentRect.size.height-titleHeight -10; //3：微调
    CGFloat titleWidth = contentRect.size.width;
    return CGRectMake(titleX, titleY, titleWidth, titleHeight);
}
@end
