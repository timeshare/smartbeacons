//
//  MyNameCardController.m
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "MyNameCardController.h"
#import "MyCardEditView.h"
#import "MyCardEditController.h"
@interface MyNameCardController ()

@end

@implementation MyNameCardController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.editBtn addTarget:self action:@selector(btnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    NameCardModel *nameCard = [NameCardModel shareNameCardModel];
    nameCard.name = @"李杰";
    nameCard.phoneNo = @"18901777546";
    self.lblName.text = nameCard.name;
    self.lblPhoneNo.text = nameCard.phoneNo;
    self.lblPos.text = nameCard.pos;
    self.lblCmpy.text = nameCard.cmpy;
    self.lblTel.text = nameCard.tel;
    self.lblMail.text = nameCard.mail;
    self.lblQQ.text = nameCard.qq;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 *  跳转到编辑页面
 */
- (void)btnClicked
{
    MyCardEditView *editV = [[MyCardEditView alloc]initWithFrame:CGRectMake(0, StatusHeight, View_Size.width, View_Size.height - StatusHeight)];
    [editV addTransition:1];
    editV.editC.delegate = self;
    [self.view.superview.superview.superview addSubview:editV];
}

/**
 *  代理方法
 */
-(void)editFinished:(UIViewController *)edit nameCard:(NameCardModel *)nameCard
{
    self.lblName.text = nameCard.name;
    self.lblPhoneNo.text = nameCard.phoneNo;
    self.lblPos.text = nameCard.pos;
    self.lblCmpy.text = nameCard.cmpy;
    self.lblTel.text = nameCard.tel;
    self.lblMail.text = nameCard.mail;
    self.lblQQ.text = nameCard.qq;
    
}
@end
