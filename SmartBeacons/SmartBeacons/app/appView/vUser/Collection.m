//
//  Collection.m
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Collection.h"
#import "VShop.h"
#import "GuideView.h"
#import "RecentActionView.h"
#define KHeight 75

@implementation Collection
{
    UIButton *_selectedBtn;
    VShop *_shopView;
    GuideView *_guideView;
    RecentActionView *_activityView;
    
    UIView *_selectedView;
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //获取数据
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/user/ticket.plist" ofType:nil];
    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
    self.configList = arrayList;
    
    //添加分类按钮
    [self addBtn];
    
    
}

- (void)addBtn
{
    //添加3个按钮
    //加入车款按钮
    UIButton *carTypeBtn = [[UIButton alloc]initWithFrame:CGRectMake(55, 9, 60, 25)];
    carTypeBtn.layer.cornerRadius = 12;
    [carTypeBtn setTitle:@"车款" forState:UIControlStateNormal];
    carTypeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [carTypeBtn addTarget:self action:@selector(btnClicked :) forControlEvents:UIControlEventTouchUpInside];
    // actBtn.backgroundColor = [UIColor whiteColor];
    carTypeBtn.tag = 0;
    //  [actBtn setTitleColor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
    [self addSubview:carTypeBtn];
    
    //加入展商按钮
    UIButton * businessBtn = [[UIButton alloc]initWithFrame:CGRectMake(125, 9, 60, 25)];
    businessBtn.layer.cornerRadius = 12;
    [businessBtn setTitle:@"展商" forState:UIControlStateNormal];
    businessBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [businessBtn addTarget:self action:@selector(btnClicked :) forControlEvents:UIControlEventTouchUpInside];
    businessBtn.tag = 1;
    [self addSubview:businessBtn];
    //加入活动按钮
    UIButton * activityBtn = [[UIButton alloc]initWithFrame:CGRectMake(195, 9, 60, 25)];
    activityBtn.layer.cornerRadius = 12;
    [activityBtn setTitle:@"活动" forState:UIControlStateNormal];
    activityBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [activityBtn addTarget:self action:@selector(btnClicked :) forControlEvents:UIControlEventTouchUpInside];
    activityBtn.tag = 2;
    [self addSubview:activityBtn];

    [self btnClicked:carTypeBtn];
}


- (void)btnClicked:(UIButton *)btn
{
    //设置选中的按钮 及未选中按钮的状态
    _selectedBtn.backgroundColor = [UIColor clearColor];
    [_selectedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitleColor:RGB(132, 190, 93) forState:UIControlStateNormal];
    _selectedBtn = btn;
    
    //加载点击按钮的view
    if (btn.tag == 0) { //点击的是 车款
        if (_guideView == nil) {
            _guideView = [[GuideView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
            _guideView.scrollView.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
            CGRect f = _guideView.scrollView.frame;
            f.size.height += AppButtomHeight;
            _guideView.scrollView.frame = f;
        }
        [self addSubview:_guideView.scrollView];
        
        [_selectedView removeFromSuperview];
        _selectedView = _guideView;
    }else if (btn.tag == 1){ //点击的是 展商
        if (_shopView == nil) {
            _shopView = [VShop instance];
            CGRect f = _shopView.contentTable.frame;
            f.size.height += AppButtomHeight;
            _shopView.contentTable.frame = f;
        }
        [self addSubview:_shopView.contentTable];
        
        [_selectedView removeFromSuperview];
        _selectedView = _shopView.contentTable;
    }else { //点击的是活动
        if (_activityView == nil) {
            _activityView = [[RecentActionView alloc]init];
            for (UIView *v in _activityView.subviews) {
                if ([v isKindOfClass:[UIScrollView class]]) {
                    CGRect f = v.frame;
                    f.size.height += AppButtomHeight;
                    v.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];
                    v.frame = f;
                }
            }
        }
        [self addSubview:_activityView];
        
        [_selectedView removeFromSuperview];
        _selectedView = _activityView;
    }
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}
@end
