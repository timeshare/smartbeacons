//
//  TicketCell.h
//  SmartBeacons
//
//  Created by mac on 14-11-13.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TicketCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *TicketLogo;
@property (weak, nonatomic) IBOutlet UIButton *TicketType;
@property (weak, nonatomic) IBOutlet UIImageView *TicketUsed;
@property (weak, nonatomic) IBOutlet UILabel *TicketTitle;

@end
