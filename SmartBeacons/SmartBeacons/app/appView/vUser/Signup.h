//
//  Signup.h
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "BaseClass.h"

@interface Signup : EEView

@property(nonatomic, strong) UITextField *name;
@property(nonatomic, strong) UITextField *password;
@property(nonatomic, strong) UITextField *phone;

@end
