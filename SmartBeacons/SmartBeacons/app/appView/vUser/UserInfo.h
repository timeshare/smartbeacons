//
//  User.h
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
@interface UserInfo : NSObject
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *nickName;
@property (nonatomic,copy) NSString *headImg;
@property (nonatomic,copy) NSString *phoneNo;
@property (nonatomic,assign) ShareType shareType;
@property (nonatomic,assign) BOOL isLogin;

+ (UserInfo *)shareUserInfo;


- (void)loadDefaultUserInfo;
- (void)loadLastUserInfo;
- (void)updateLoginInfo;
- (void)logout;
//- (BOOL)needsLogin;
@end
