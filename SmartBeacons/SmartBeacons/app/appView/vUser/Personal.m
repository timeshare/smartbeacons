//
//  Personal.m
//  SmartBeacons
//
//  Created by mac on 14-11-11.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>

#import "Personal.h"
#import "UserInfo.h"
#import "Login.h"
#import "VPImageCropperViewController.h"
#import <ShareSDK/ShareSDK.h>

#define ORIGINAL_MAX_WIDTH 640.0f

@interface Personal()
@property (nonatomic,strong)UIView *v1;
@property (nonatomic,strong)UIView *v2;

@property (nonatomic,strong)UIButton *headPortrait;

@end

@implementation Personal

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //0.设置背景颜色
        [self setBackgroundColor:RGB(245, 245, 245)];
        
        //判断是否已经登陆
        if ([UserInfo shareUserInfo].isLogin == YES) {
            [self addLoginedView];
        }else{
            [self addLoginView];
        }
        
    }
    return self;
}

/**
 *  登陆前的view
 */
- (void)addLoginView
{
    CGRect tmpF = self.frame;
    tmpF.origin.y = 0;
    self.v1 = [[UIView alloc]initWithFrame:tmpF];
    //1.头像按钮
    UIButton *headPic = [UIButton buttonWithType:UIButtonTypeCustom];
    [headPic setImage:[UIImage imageNamed:@"assets/userCenter/headpic.png"] forState:UIControlStateNormal];
    headPic.frame = CGRectMake(View_Size.width * 0.5-45, self.frame.size.height * 0.4 - 45, 90, 90);
    [headPic addTarget:self action:@selector(goLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.v1 addSubview:headPic];
    
    //2.登陆按钮
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setImage:[UIImage imageNamed:@"assets/userCenter/loginbtn@2x.png"] forState:UIControlStateNormal];
    loginBtn.frame = CGRectMake(0, 0, 76, 37);
    loginBtn.center = CGPointMake(View_Size.width * 0.5, CGRectGetMaxY(headPic.frame) + 37/2 + 5);
    [loginBtn addTarget:self action:@selector(goLogin) forControlEvents:UIControlEventTouchUpInside];
    [self.v1 addSubview:loginBtn];
    
    [self addSubview: self.v1];
    
    //3.注册成功的通知
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(thirdLoginSuccess) name:@"thirdLoginSuccess" object:nil];
}

/**
 *  第三方登陆成功后执行
 */
- (void)thirdLoginSuccess
{
    [self login:nil success:YES];
    
}

/**
 *  跳转到登陆页面
 */
- (void)goLogin
{
    Login *loginView = [[Login alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height - StatusHeight)];
    loginView.delegate = self;
    [loginView addTransition:1];
    [[RootController sharedRootViewController].view addSubview:loginView];
}

/**
 *  登陆后的view
 */
- (void)addLoginedView
{
    CGRect tmpF = self.frame;
    tmpF.origin.y = 0;
    self.v2 = [[UIView alloc]initWithFrame:tmpF];
    //1.头像按钮
    UIButton *headPic = [UIButton buttonWithType:UIButtonTypeCustom];
    if ([UserInfo shareUserInfo].headImg) {
        [headPic setImage:[UIImage imageWithContentsOfFile:[UserInfo shareUserInfo].headImg] forState:UIControlStateNormal];
        
    }else{
        [headPic setImage:[UIImage imageNamed:@"assets/userCenter/headpic.png"] forState:UIControlStateNormal];
    }
    
    
    headPic.layer.cornerRadius = 45;
    [headPic.layer setMasksToBounds:YES];
    headPic.frame = CGRectMake(20, self.frame.size.height * 0.5 - 45, 90, 90);
    [headPic addTarget:self action:@selector(selectHeadpic) forControlEvents:UIControlEventTouchUpInside];
    [self.v2 addSubview:headPic];
    self.headPortrait = headPic;
    
    //2.拍照按钮
    UIButton *photoBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [photoBtn setImage:[UIImage imageNamed:@"assets/userCenter/photo.png"] forState:UIControlStateNormal];
    CGFloat xPhotoBtn = CGRectGetMaxX(headPic.frame) - 24;
    CGFloat yPhotoBtn = CGRectGetMaxY(headPic.frame) - 24;
    photoBtn.frame = CGRectMake(xPhotoBtn, yPhotoBtn, 24, 24);
    [photoBtn addTarget:self action:@selector(selectHeadpic) forControlEvents:UIControlEventTouchUpInside];
    [self.v2 addSubview:photoBtn];
    
    //3.名字标签
    CGFloat xNameLbl = CGRectGetMaxX(headPic.frame) + 20;
    CGFloat yNameLbl = headPic.frame.origin.y ;
    UILabel *nameLbl = [[UILabel alloc]initWithFrame:CGRectMake(xNameLbl, yNameLbl, 120, 20 )];
    nameLbl.text = [UserInfo shareUserInfo].nickName;
    [self.v2 addSubview:nameLbl];
    
    //4.编辑我的名片按钮
    UIButton *editCardBtn = [[UIButton alloc]init];
    [editCardBtn setImage:[UIImage imageNamed:@"assets/userCenter/edit.png"] forState:UIControlStateNormal];
    [editCardBtn setTitle:@"  编辑我的名片" forState:UIControlStateNormal];
    [editCardBtn setFont:[UIFont systemFontOfSize:14.0]];
    [editCardBtn setTitleColor:RGB(70, 159, 243) forState:UIControlStateNormal];
    [editCardBtn setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    CGFloat xEditBtn = xNameLbl;
    CGFloat yEditBtn = CGRectGetMaxY(nameLbl.frame) + 10 ;
    editCardBtn.frame = CGRectMake(xEditBtn, yEditBtn, 150, 30);
    [self.v2 addSubview:editCardBtn];
    
    //5.账号绑定Label
    //5.1 icon
    UIImageView *accountCnc = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/icon.png"]];
    CGFloat xCnc = xNameLbl;
    CGFloat yCnc = CGRectGetMaxY(headPic.frame) - 14.5;
    accountCnc.frame = CGRectMake(xCnc, yCnc, 17, 14.5);
    [self.v2 addSubview:accountCnc];
    //5.2 label
    UILabel *cncLbl = [[UILabel alloc]init];
    cncLbl.text = @"账号绑定: ";
    cncLbl.textColor = RGB(82, 81, 81);
    cncLbl.font = [UIFont systemFontOfSize:14.0];
    CGFloat xCncLbl = CGRectGetMaxX(accountCnc.frame) + 10;
    CGFloat yCncLbl = CGRectGetMaxY(accountCnc.frame) -14.5;
    CGFloat wCncLbl = [cncLbl.text sizeWithFont:[UIFont systemFontOfSize:14.0]].width;
    cncLbl.frame = CGRectMake(xCncLbl, yCncLbl,wCncLbl , 14.5);
    [self.v2 addSubview:cncLbl];
    //5.3第三方的登陆button
    UIButton *weiboBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [weiboBtn setImage:[UIImage imageNamed:@"assets/userCenter/sina.png"] forState:UIControlStateNormal];
    CGFloat xWb = CGRectGetMaxX(cncLbl.frame);
    CGFloat yWb = yCncLbl;
    [weiboBtn setFrame:CGRectMake(xWb, yWb, 18,18)];
    [self.v2 addSubview:weiboBtn];
    
    UIButton *qqBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [qqBtn setImage:[UIImage imageNamed:@"assets/userCenter/QQ.png"] forState:UIControlStateNormal];
    CGFloat xQQ = CGRectGetMaxX(weiboBtn.frame) +5;
    CGFloat yQQ = yCncLbl;
    [qqBtn setFrame:CGRectMake(xQQ, yQQ, 18.5,18)];
    [self.v2 addSubview:qqBtn];
    
    UIButton *weixinBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [weixinBtn setImage:[UIImage imageNamed:@"assets/userCenter/weixin.png"] forState:UIControlStateNormal];
    CGFloat xWx = CGRectGetMaxX(qqBtn.frame) + 5;
    CGFloat yWx = yCncLbl;
    [weixinBtn setFrame:CGRectMake(xWx, yWx, 18,18)];
    [self.v2 addSubview:weixinBtn];
    
    //6.退出按钮
    UIButton *exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [exitBtn setTitle:@"退出" forState:UIControlStateNormal];
    [exitBtn setTitleColor:RGB(82, 81, 81) forState:UIControlStateNormal];
    exitBtn.frame = CGRectMake(View_Size.width - 10 - 50, 10, 50, 30);
    [exitBtn setFont:[UIFont systemFontOfSize:14.0]];
    [exitBtn addTarget:self action:@selector(loginout) forControlEvents:UIControlEventTouchUpInside];
    [self.v2 addSubview:exitBtn];
    
    [self addSubview:self.v2];
}

/**
 *  退出登陆
 */
- (void)loginout
{
    //修改账号信息
    [UserInfo shareUserInfo].isLogin = NO;
    [[UserInfo shareUserInfo]updateLoginInfo];
    
    if (self.v1 == nil) {
        [self addLoginView];
    }
    
    [ShareSDK cancelAuthWithType:[UserInfo shareUserInfo].shareType];
    
    CGRect tmpF1 = self.v1.frame;
    tmpF1.origin.x = View_Size.width;
    self.v1.frame = tmpF1;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect tmpF2 = self.v2.frame;
        tmpF2.origin.x = -View_Size.width;
        self.v2.frame = tmpF2;
        
        CGRect tmpF1 = self.v1.frame;
        tmpF1.origin.x = 0;
        self.v1.frame = tmpF1;
        [self addSubview:self.v1];
    } completion:^(BOOL finished) {
        [self.v2 removeFromSuperview];
    }];
    
}

/**
 *  选择头像更改方式
 */
- (void)selectHeadpic
{
    UIActionSheet *choiceSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"取消"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"拍照", @"从相册中选取", nil];
    [choiceSheet showInView:self.superview];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^() {
        UIImage *portraitImg = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        portraitImg = [self imageByScalingToMaxSize:portraitImg];
        // 裁剪
        VPImageCropperViewController *imgEditorVC = [[VPImageCropperViewController alloc] initWithImage:portraitImg cropFrame:CGRectMake(0, 100.0f, self.frame.size.width, self.frame.size.width) limitScaleRatio:3.0];
        imgEditorVC.delegate = self;
        [[RootController sharedRootViewController] presentViewController:imgEditorVC animated:YES completion:^{
            // TO DO
            
        }];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^(){
    }];
}

#pragma mark actionsheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        // 拍照
        if ([self isCameraAvailable] && [self doesCameraSupportTakingPhotos]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([self isFrontCameraAvailable]) {
                controller.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [[RootController sharedRootViewController] presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }else if(buttonIndex == 1){
        // 从相册中选取
        if ([self isPhotoLibraryAvailable]) {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            NSMutableArray *mediaTypes = [[NSMutableArray alloc] init];
            [mediaTypes addObject:(__bridge NSString *)kUTTypeImage];
            controller.mediaTypes = mediaTypes;
            controller.delegate = self;
            [[RootController sharedRootViewController] presentViewController:controller
                               animated:YES
                             completion:^(void){
                                 NSLog(@"Picker View Controller is presented");
                             }];
        }
    }
}


/**
 *  login代理方法
 */
- (void)login:(EEView *)loginView success:(BOOL)is
{
    if (is == YES) {
        [self.v1 removeFromSuperview];
        [self addLoginedView];
    }
}

/**
 *  重写setframe
 */
- (void)setFrame:(CGRect)frame
{
    CGRect tmpF = frame;
    tmpF.size.height = 200;
    frame = tmpF;
    
    [super setFrame:frame];
}

#pragma mark camera utility
- (BOOL) isCameraAvailable{
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isRearCameraAvailable{
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear];
}

- (BOOL) isFrontCameraAvailable {
    return [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront];
}

- (BOOL) doesCameraSupportTakingPhotos {
    return [self cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL) isPhotoLibraryAvailable{
    return [UIImagePickerController isSourceTypeAvailable:
            UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickVideosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeMovie sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}
- (BOOL) canUserPickPhotosFromPhotoLibrary{
    return [self
            cameraSupportsMedia:(__bridge NSString *)kUTTypeImage sourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}

- (BOOL) cameraSupportsMedia:(NSString *)paramMediaType sourceType:(UIImagePickerControllerSourceType)paramSourceType{
    __block BOOL result = NO;
    if ([paramMediaType length] == 0) {
        return NO;
    }
    NSArray *availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:paramSourceType];
    [availableMediaTypes enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *mediaType = (NSString *)obj;
        if ([mediaType isEqualToString:paramMediaType]){
            result = YES;
            *stop= YES;
        }
    }];
    return result;
}

#pragma mark image scale utility
- (UIImage *)imageByScalingToMaxSize:(UIImage *)sourceImage {
    if (sourceImage.size.width < ORIGINAL_MAX_WIDTH) return sourceImage;
    CGFloat btWidth = 0.0f;
    CGFloat btHeight = 0.0f;
    if (sourceImage.size.width > sourceImage.size.height) {
        btHeight = ORIGINAL_MAX_WIDTH;
        btWidth = sourceImage.size.width * (ORIGINAL_MAX_WIDTH / sourceImage.size.height);
    } else {
        btWidth = ORIGINAL_MAX_WIDTH;
        btHeight = sourceImage.size.height * (ORIGINAL_MAX_WIDTH / sourceImage.size.width);
    }
    CGSize targetSize = CGSizeMake(btWidth, btHeight);
    return [self imageByScalingAndCroppingForSourceImage:sourceImage targetSize:targetSize];
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark VPImageCropperDelegate
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    [self.headPortrait setImage:editedImage forState:UIControlStateNormal];
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        // save the pic to sandbox
        [self saveImage:editedImage];
        
    }];
}

- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
    }];
}

- (void)saveImage:(UIImage *)image {
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.png"];
    
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
    //    UIImage *smallImage = [self thumbnailWithImageWithoutScale:image size:CGSizeMake(93, 93)];
    [UIImagePNGRepresentation(image) writeToFile:imageFilePath atomically:YES];//写入文件
    [UserInfo shareUserInfo].headImg = imageFilePath;
    [[UserInfo shareUserInfo]updateLoginInfo];
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"thirdLoginSuccess" object:nil];
}

@end
