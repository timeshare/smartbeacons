//
//  Login.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Login.h"
#import "PersonalCenter.h"
#import "CooperationLoginView.h"
#import "UserInfo.h"
@implementation Login

+(id)instance{
    Login *view  = [[Login alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:  [UIColor whiteColor]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"登    陆";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //UITextField--用户名
    _name = [[UITextField alloc]initWithFrame:CGRectMake(45, 80, 260, 40)];
    _name.inputAccessoryView.frame = CGRectMake(300, 0, 265, 40);
    _name.backgroundColor = RGB(235, 235, 235);
    _name.placeholder = @"用户名";
    _name.returnKeyType = UIReturnKeyDone;
    [self addSubview:_name];
    
    //左侧图片加载
    UIImageView *nameView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 80, 30, 40)];
    [nameView setBackgroundColor:RGB(235, 235, 235)];
    UIImage *nameImg = [UIImage imageNamed:@"assets/image/user.png"];
    [nameView setImage:nameImg];
    nameView.contentMode =  UIViewContentModeCenter;
    [self addSubview:nameView];
    
    
    //UITextField--密码
    _password = [[UITextField alloc]initWithFrame:CGRectMake(45, 130, 260, 40)];
    _password.backgroundColor = RGB(235, 235, 235);
    _password.placeholder = @"输入您的密码";
    _password.secureTextEntry = YES;
    [self addSubview:_password];
    
    //左侧图片加载
    UIImageView *passwordView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 130, 30, 40)];
    [passwordView setBackgroundColor:RGB(235, 235, 235)];
    UIImage *passwordImg = [UIImage imageNamed:@"assets/image/password.png"];
    [passwordView setImage:passwordImg];
    passwordView.contentMode =  UIViewContentModeCenter;
    [self addSubview:passwordView];
    
    //登陆按钮
    [self addSubview:[Global createBtn:CGRectMake(15, 195, 290, 45) normal:@"assets/userCenter/login_btn.png" down:nil title:@"登 陆" col:[UIColor whiteColor] size:20 target:self action:@selector(login)]];
    
    //忘记密码
    UIButton *forgetbtn = [[UIButton alloc]initWithFrame:CGRectMake(15, 245, 80, 40)];
    //forgetbtn.center = CGPointMake(15, 265);
    [forgetbtn setTitle:@"忘记密码？" forState:UIControlStateNormal];
    [forgetbtn setTitleColor:RGB(255, 153, 153) forState:UIControlStateNormal];
    forgetbtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:forgetbtn];
    
    //立即注册
    UIButton *loginBtn = [[UIButton alloc]initWithFrame:CGRectMake(320-80-15, 245, 80, 40)];
    [loginBtn setTitle:@"立即注册!" forState:UIControlStateNormal];
    [loginBtn setTitleColor:RGB(255, 153, 153) forState:UIControlStateNormal];
    loginBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:loginBtn];
    
    //合作登陆
    CooperationLoginView *cLoginView = [[CooperationLoginView alloc]init];
    CGRect tmpFrame = cLoginView.frame;
    tmpFrame.origin = CGPointMake(0, self.frame.size.height -tmpFrame.size.height -AppButtomHeight);
    cLoginView.frame = tmpFrame;
    [self addSubview:cLoginView];
    
    //添加手势响应
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGr];
    
}

#pragma mark 点击屏幕隐藏键盘
-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_name resignFirstResponder];
    [_password resignFirstResponder];
}

-(void)dealloc{
    self.info = nil;
}

//登陆
- (void)login
{
    if ([_name.text  isEqual: @"admin"] && [_password.text  isEqual: @"123"]) {
        [UserInfo shareUserInfo].name = _name.text;
        [UserInfo shareUserInfo].isLogin = YES;
        [UserInfo shareUserInfo].nickName = @"Shane";
        [[UserInfo shareUserInfo]updateLoginInfo];
    //    [self saveImage:[UIImage imageNamed:@"assets/userCenter/headpic.png"]];
        
        //代理
        if ([self.delegate respondsToSelector:@selector(login:success:)]) {
            [self.delegate login:self success:YES];
        }
        
        [self eeRemoveRight];
    }else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"账号信息有误" delegate:self cancelButtonTitle:@"确认" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)saveImage:(UIImage *)image {
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.jpg"];
    
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
//    UIImage *smallImage = [self thumbnailWithImageWithoutScale:image size:CGSizeMake(93, 93)];
    [UIImagePNGRepresentation(image) writeToFile:imageFilePath atomically:YES];//写入文件
    [UserInfo shareUserInfo].headImg = imageFilePath;
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];
   
}

//二维码扫描
- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}
@end
