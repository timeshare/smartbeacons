//
//  RecordCell.h
//  SmartBeacons
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordModel.h"
@interface RecordCell : UITableViewCell
@property (nonatomic,strong) RecordModel *record;
@property (nonatomic,strong) UIImageView *leftIcon;
@property (nonatomic,strong) UILabel *typeLabel;
@property (nonatomic,strong) UILabel *recordTimeLbl;
@property (nonatomic,strong) UIImageView *rightCarImage;
@end
