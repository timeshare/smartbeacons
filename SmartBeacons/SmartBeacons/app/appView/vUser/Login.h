//
//  Login.h
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "BaseClass.h"

@protocol loginDelegate <NSObject>
@optional
- (void)login:(EEView *)loginView success:(BOOL)is;
@end

@interface Login : EEView<UITextFieldDelegate>

@property(nonatomic, strong)NSDictionary *info;

@property(nonatomic, strong) UITextField *name;

@property(nonatomic, strong) UITextField *password;

@property (nonatomic,weak) id<loginDelegate> delegate;
@end
