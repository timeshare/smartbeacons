//
//  RecordCell.m
//  SmartBeacons
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "RecordCell.h"

@implementation RecordCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        //设置白条背景图片
        UIImageView *bg = [[UIImageView alloc]initWithFrame:CGRectMake(5, 10, View_Size.width - 10, 50)];
        bg.image = [UIImage imageNamed: @"assets/userCenter/record/bg_record_item.png"];
        [self addSubview:bg];
        
        self.leftIcon = [[UIImageView alloc]init];
        [self addSubview:self.leftIcon];
        
        self.typeLabel = [[UILabel alloc]init];
        [self addSubview:self.typeLabel];
        
        self.recordTimeLbl = [[UILabel alloc]init];
        [self addSubview:self.recordTimeLbl];
        
        self.rightCarImage = [[UIImageView alloc]init];
        [self addSubview:self.rightCarImage];
        
    }
    return self;
}

-(void)setRecord:(RecordModel *)record
{
    //1.左边logo设置
    UIImage *leftImg = [UIImage imageNamed:record.carIconImg];
    self.leftIcon.image = leftImg;
    self.leftIcon.frame =CGRectMake(15, 13, leftImg.size.width, leftImg.size.height);
    
    //2.型号label设置
    self.typeLabel.text = record.carType;
    CGFloat xTypelbl = CGRectGetMaxX(self.leftIcon.frame) + 10;
    CGFloat yTypelbl = 20;
    CGSize sTypelbl = [self.typeLabel.text sizeWithFont:[UIFont systemFontOfSize:14]];
    self.typeLabel.frame = CGRectMake(xTypelbl, yTypelbl, 150, sTypelbl.height);
    
    //3.时间
    self.recordTimeLbl.text = record.recordTime;
    CGFloat xRecordTime = xTypelbl;
    CGFloat yRecordTime = CGRectGetMaxY(self.typeLabel.frame) + 3;
    [self.recordTimeLbl setTextColor:RGB(170, 170, 170)];
    CGSize sTimelbl = [self.recordTimeLbl.text sizeWithFont:[UIFont systemFontOfSize:14]];
    [self.recordTimeLbl setFrame:CGRectMake(xRecordTime, yRecordTime, 150, sTimelbl.height)];
    
    //4.车的图片
    UIImage *right = [UIImage imageNamed:record.carImg];
    self.rightCarImage.image = right;
    CGFloat xRight = 320 - right.size.width -10 ;
    
    self.rightCarImage.frame = CGRectMake(xRight, 13, right.size.width, right.size.height);
    
}

@end
