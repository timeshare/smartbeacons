//
//  PdfReadView.m
//  SmartBeacons
//
//  Created by mac on 14-11-20.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "PdfReadView.h"

@implementation PdfReadView
{
    UIWebView *_pdfWeb;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
        closeBtn.frame = CGRectMake(260,20, 60, 30);
        [closeBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
        [closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
        
        _pdfWeb = [[UIWebView alloc]initWithFrame:CGRectMake(0, 0 , View_Size.width, View_Size.height)];
        _pdfWeb.backgroundColor = [UIColor whiteColor];
        [_pdfWeb addSubview:closeBtn];
        [self addSubview:_pdfWeb];
        [self loadPDF];
        
    }
    
    return self;
}

#pragma mark 加载pdf文件
- (void)loadPDF
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"CFNetwork 编程指南.pdf" ofType:nil];
    

 //   NSURL *url = [NSURL fileURLWithPath:path];
    
    NSData *data = [NSData dataWithContentsOfFile:path];
    
    [_pdfWeb loadData:data MIMEType:@"application/pdf" textEncodingName:@"UTF-8" baseURL:nil];
}

- (void)close
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.frame;
        f.origin.y += View_Size.height;
        self.frame = f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
}
@end
