//
//  NameCard.h
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "BaseClass.h"
#import "ZBarSDK.h"

@interface NameCard : EEView<ZBarReaderDelegate,UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)NSMutableArray *configList;

@property(nonatomic, strong) ZBarReaderViewController *reader;

@end
