//
//  MessageInformationCell.h
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageInformationCell : UITableViewCell

@property(nonatomic, strong)NSDictionary *messageData;

-(id)initCellByData:(NSMutableDictionary *)dataDictionary;

@end
