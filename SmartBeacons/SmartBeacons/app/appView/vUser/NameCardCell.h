//
//  NameCardCell.h
//  SmartBeacons
//
//  Created by mac on 14-11-13.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NameCardCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *namecardHead;
@property (weak, nonatomic) IBOutlet UIImageView *line;

@property (weak, nonatomic) IBOutlet UILabel *namecardName;
@end
