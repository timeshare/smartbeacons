//
//  CooperationLoginView.m
//  SmartBeacons
//
//  Created by mac on 14-11-10.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "CooperationLoginView.h"
#import <ShareSDK/ShareSDK.h>
#import <Parse/Parse.h>
#import "UserInfo.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"
@implementation CooperationLoginView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //合作登陆
        UIButton *cooperationbtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 0, 150, 15)];
        cooperationbtn.center = CGPointMake(320 * 0.5,cooperationbtn.frame.size.height);
        [cooperationbtn setTitle:@"使用其他方式登陆" forState:UIControlStateNormal];
        [cooperationbtn setTitleColor:RGB(161, 161, 161) forState:UIControlStateNormal];
        cooperationbtn.titleLabel.font = [UIFont systemFontOfSize:14];
        cooperationbtn.enabled = NO;
        [self addSubview:cooperationbtn];
        
        //合作登陆的左右两边的横线
        UIView *leftLineH = [[UIView alloc]initWithFrame:CGRectMake(25, 15, 70, 1)];
        leftLineH.backgroundColor = RGB(203, 203, 203);
        [self addSubview:leftLineH];
        
        UIView *rightLineH = [[UIView alloc]initWithFrame:CGRectMake(320-25-70, 15, 70, 1)];
        rightLineH.backgroundColor = RGB(203, 203, 203);
        [self addSubview:rightLineH];
        
        UIView *leftLineV = [[UIView alloc]initWithFrame:CGRectMake(107, 30, 1, 80)];
        leftLineV.backgroundColor = RGB(203, 203, 203);
        [self addSubview:leftLineV];
        
        UIView *rightLineV = [[UIView alloc]initWithFrame:CGRectMake(217, 30, 1, 80)];
        rightLineV.backgroundColor = RGB(203, 203, 203);
        [self addSubview:rightLineV];
        
        UIButton *weibo = [[UIButton alloc]initWithFrame:CGRectMake(20, 40, 54.5, 69.5)];
        UIButton *qq = [[UIButton alloc]initWithFrame:CGRectMake(130, 40, 54.5, 69.5)];
        UIButton *weixin = [[UIButton alloc]initWithFrame:CGRectMake(240, 40, 54.5, 69.5)];
        
        weibo.tag = ShareTypeSinaWeibo;
        qq.tag = ShareTypeQQSpace;
        weixin.tag = ShareTypeWeixiSession;
        
        [weibo setBackgroundImage:[UIImage imageNamed:@"assets/userCenter/weibo_bigicon.png"] forState:UIControlStateNormal];
        [qq setBackgroundImage:[UIImage imageNamed:@"assets/userCenter/qq_bigicon.png"] forState:UIControlStateNormal];
        [weixin setBackgroundImage:[UIImage imageNamed:@"assets/userCenter/weixin_bigicon.png"] forState:UIControlStateNormal];
        
        [weibo addTarget:self action:@selector(loginWithSharetype:) forControlEvents:UIControlEventTouchUpInside];
        [qq addTarget:self action:@selector(loginWithSharetype:) forControlEvents:UIControlEventTouchUpInside];
        [weixin addTarget:self action:@selector(loginWithSharetype:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:weibo];
        [self addSubview:qq];
        [self addSubview:weixin];
        
        self.bounds = CGRectMake(0, 0, 320, CGRectGetMaxY(weibo.frame)+20);
        return self;
    }
    return self;
}

- (void)loginWithSharetype:(UIButton *)btn
{
    if (btn.tag == ShareTypeQQSpace) {
        if ([QQApi isQQInstalled] && [QQApi isQQSupportApi]){
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"请安装最新版本qq" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    if (btn.tag == ShareTypeWeixiSession) {
        if ([WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi]){
            
        }else{
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"未安装微信" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
    }
    
    [ShareSDK getUserInfoWithType:btn.tag
                      authOptions:nil
                           result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {
                               
                               if (result)
                               {
                                   [UserInfo shareUserInfo].shareType = btn.tag;
                                   [UserInfo shareUserInfo].uid = [userInfo uid];
                                   [UserInfo shareUserInfo].name = @"";
                                   [UserInfo shareUserInfo].nickName = [userInfo nickname];
                                   [UserInfo shareUserInfo].isLogin = YES;
                                   UIImage *headImg = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[userInfo profileImage]]]];
                                   if(headImg.size.width <90||headImg.size.height<90){
                                       headImg = [self imageByScalingAndCroppingForSourceImage:headImg targetSize:CGSizeMake(90, 90)];
                                   }
                                   [self saveImage:headImg];
                                   
                                   [[NSNotificationCenter defaultCenter]postNotificationName:@"thirdLoginSuccess" object:nil];
                                   [(EEView *)self.superview eeRemoveRight];
                               }
                              
                           }];
}


- (void)saveImage:(UIImage *)image {
    //    NSLog(@"保存头像！");
    //    [userPhotoButton setImage:image forState:UIControlStateNormal];
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"selfPhoto.png"];
    
    success = [fileManager fileExistsAtPath:imageFilePath];
    if(success) {
        success = [fileManager removeItemAtPath:imageFilePath error:&error];
    }
    //    UIImage *smallImage=[self scaleFromImage:image toSize:CGSizeMake(80.0f, 80.0f)];//将图片尺寸改为80*80
    //    UIImage *smallImage = [self thumbnailWithImageWithoutScale:image size:CGSizeMake(93, 93)];
    [UIImagePNGRepresentation(image) writeToFile:imageFilePath atomically:YES];//写入文件
    [UserInfo shareUserInfo].headImg = imageFilePath;
    [[UserInfo shareUserInfo]updateLoginInfo];
    //    [userPhotoButton setImage:selfPhoto forState:UIControlStateNormal];
    
}

- (UIImage *)imageByScalingAndCroppingForSourceImage:(UIImage *)sourceImage targetSize:(CGSize)targetSize {
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth  = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else
            if (widthFactor < heightFactor)
            {
                thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
            }
    }
    UIGraphicsBeginImageContext(targetSize); // this will crop
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width  = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil) NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}
@end
