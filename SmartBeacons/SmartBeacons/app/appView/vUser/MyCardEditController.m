//
//  MyNameCardController.m
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "MyCardEditController.h"

@interface MyCardEditController ()

@end

@implementation MyCardEditController
{
    CGRect _staticFrame;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NameCardModel *nameCard = [NameCardModel shareNameCardModel];
    self.lblName.text = nameCard.name;
    self.lblPhoneNo.text = nameCard.phoneNo;
    self.lblPos.text = nameCard.pos;
    self.lblCmpy.text = nameCard.cmpy;
    self.lblTel.text = nameCard.tel;
    self.lblMail.text = nameCard.mail;
    self.lblQQ.text = nameCard.qq;
    
    //
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
    
    _staticFrame = self.view.frame;
    //添加手势响应
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGr];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - notification handler

//- (void)keyboardWillChangeFrame:(NSNotification *)notification
//{
//   
//}

//- (void)dealloc
//{
//    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIKeyboardWillChangeFrameNotification object:self];
//}

/**
 *  手势点击响应方法
 */
- (void)viewTapped:(UITapGestureRecognizer *)ges
{
    for (UIView *v in self.view.subviews) {
        if ([v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
        }
    }
}

#pragma mark textfield代理方法
//开始编辑输入框的时候，软键盘出现，执行此事件
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
//    NSLog(@"%@",NSStringFromCGRect(textField.frame));
    if (textField.tag < 4) {
        return;
    }
    CGFloat yOffset = 181;
    CGRect inputFieldRect = self.view.frame;
    
    inputFieldRect.origin.y -= yOffset;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = inputFieldRect;
    }];
}

//当用户按下return键或者按回车键，keyboard消失
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//输入框编辑完成以后，将视图恢复到原始状态
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3 animations:^{
        self.view.frame = _staticFrame;
    }];
}

/**
 *  保存数据
 */
- (IBAction)saveData:(UIButton *)sender {
    //数据
    NameCardModel *nameCard = [NameCardModel shareNameCardModel];
    nameCard.name = self.lblName.text;
    nameCard.phoneNo = self.lblPhoneNo.text;
    nameCard.pos = self.lblPos.text;
    nameCard.cmpy = self.lblCmpy.text;
    nameCard.tel = self.lblTel.text;
    nameCard.mail = self.lblMail.text;
    nameCard.qq = self.lblQQ.text;
  
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(editFinished:nameCard:)]) {
            [self.delegate editFinished:self nameCard:nameCard];
        }
    }
    [(EEView *)self.view.superview.superview eeRemoveRight];
}
@end
