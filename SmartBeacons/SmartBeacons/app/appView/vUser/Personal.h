//
//  Personal.h
//  SmartBeacons
//
//  Created by mac on 14-11-11.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Login.h"
#import "VPImageCropperViewController.h"
@interface Personal : UIView <loginDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,VPImageCropperDelegate>

@end
