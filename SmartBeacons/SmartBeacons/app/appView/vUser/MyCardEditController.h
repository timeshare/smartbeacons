//
//  MyNameCardController.h
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NameCardModel.h"

@protocol EditFinishedDelegate <NSObject>
@optional
- (void)editFinished:(UIViewController *)edit nameCard:(NameCardModel*)nameCard;

@end

@interface MyCardEditController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgViewHeadpic;
@property (weak, nonatomic) IBOutlet UITextField *lblName;
@property (weak, nonatomic) IBOutlet UITextField *lblPhoneNo;
@property (weak, nonatomic) IBOutlet UITextField *lblPos;
@property (weak, nonatomic) IBOutlet UITextField *lblCmpy;
@property (weak, nonatomic) IBOutlet UITextField *lblTel;
@property (weak, nonatomic) IBOutlet UITextField *lblMail;
@property (weak, nonatomic) IBOutlet UITextField *lblQQ;
- (IBAction)saveData:(UIButton *)sender;

@property (nonatomic,weak)id<EditFinishedDelegate> delegate;
@end
