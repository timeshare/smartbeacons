//
//  LoginedView.m
//  SmartBeacons
//
//  Created by mac on 14-11-11.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "PersonalCenter.h"
#import "Login.h"
#import "Signup.h"
#import "ChangePassword.h"
#import "Setting.h"
#import "Message.h"
#import "Ticket.h"
#import "NameCard.h"
#import "Collection.h"
#import "Personal.h"
#import "UserItemBtn.h"
#import "RecordView.h"
#import "DataBaseView.h"

#define deltaH 50
@interface PersonalCenter()

@end
@implementation PersonalCenter

+(id)instance{
    PersonalCenter *view  = [[PersonalCenter alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}

-(void)initView{
    [self setBackgroundColor:[UIColor whiteColor]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"个人中心";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //个人信息栏
    Personal * pView = [[Personal alloc]initWithFrame:CGRectMake(0, AppTopHeight, 320, 100)];
    
    [self addSubview:pView];
    
    //下方的六个button
    CGFloat yRecordItem = CGRectGetMaxY(pView.frame);
    CGFloat itemWidth = View_Size.width/3;
    CGFloat itemHeight = (self.frame.size.height - yRecordItem - AppButtomHeight) * 0.5;
    
    //1.记录
    UserItemBtn *recordItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(0, yRecordItem, itemWidth, itemHeight)];
    [recordItem setImage:[UIImage imageNamed:@"assets/userCenter/record.png"] forState:UIControlStateNormal];
    [recordItem setTitle:@"纪录" forState:UIControlStateNormal];
    [recordItem addTarget:self action:@selector(record) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:recordItem];
    //2.电子券
    UserItemBtn *ticketItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(itemWidth, yRecordItem, itemWidth, itemHeight)];
    [ticketItem setImage:[UIImage imageNamed:@"assets/userCenter/ElectricTicket.png"] forState:UIControlStateNormal];
    [ticketItem setTitle:@"电子券" forState:UIControlStateNormal];
    [ticketItem addTarget:self action:@selector(ticket) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:ticketItem];
    //3.名片夹
    UserItemBtn *namecardItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(itemWidth * 2, yRecordItem, itemWidth, itemHeight)];
    [namecardItem setImage:[UIImage imageNamed:@"assets/userCenter/namecard.png"] forState:UIControlStateNormal];
    [namecardItem setTitle:@"名片夹" forState:UIControlStateNormal];
    [namecardItem addTarget:self action:@selector(namecard) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:namecardItem];
    //4.设置
    UserItemBtn *settingItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(0, yRecordItem + itemHeight, itemWidth, itemHeight)];
    [settingItem setImage:[UIImage imageNamed:@"assets/userCenter/setting.png"] forState:UIControlStateNormal];
    [settingItem setTitle:@"设置" forState:UIControlStateNormal];
    [settingItem addTarget:self action:@selector(setting) forControlEvents:UIControlEventTouchUpInside];
    NSLog(@"y-location:%i",settingItem.frame.origin.y);
    [self addSubview:settingItem];
    //5.我的收藏
    UserItemBtn *collectItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(itemWidth, yRecordItem + itemHeight, itemWidth, itemHeight)];
    [collectItem setImage:[UIImage imageNamed:@"assets/userCenter/collect.png"] forState:UIControlStateNormal];
    [collectItem setTitle:@"我的收藏" forState:UIControlStateNormal];
    [collectItem addTarget:self action:@selector(collection) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:collectItem];
    //6.我的资料库
    UserItemBtn *changePwdItem = [[UserItemBtn alloc]initWithFrame:CGRectMake(itemWidth * 2, yRecordItem + itemHeight, itemWidth, itemHeight)];
    [changePwdItem setImage:[UIImage imageNamed:@"assets/userCenter/database.png"] forState:UIControlStateNormal];
    [changePwdItem setTitle:@"我的资料库" forState:UIControlStateNormal];
    [changePwdItem addTarget:self action:@selector(goDatabase) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:changePwdItem];
    
    //7.加入分割线
    //7.1四根垂直线
    UIImageView *line1 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/sepatate_line_v.png"]];
    line1.frame = CGRectMake(itemWidth, yRecordItem, 1, itemHeight);
    [self addSubview:line1];
    
    UIImageView *line2 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/sepatate_line_v.png"]];
    line2.frame = CGRectMake(itemWidth *2 , yRecordItem, 1, itemHeight);
    [self addSubview:line2];
    
    UIImageView *line3 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/sepatate_line_v.png"]];
    line3.frame = CGRectMake(itemWidth, yRecordItem + itemHeight, 1, itemHeight -5);
    [self addSubview:line3];
    
    UIImageView *line4 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/sepatate_line_v.png"]];
    line4.frame = CGRectMake(itemWidth *2, yRecordItem +itemHeight, 1, itemHeight -5);
    [self addSubview:line4];
    
    //7.2 三根水平线
    UIImageView *line5 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/separate_line_h.png"]];
    line5.frame = CGRectMake(itemWidth - 93, yRecordItem + itemHeight, 93, 1);
    [self addSubview:line5];
    
    UIImageView *line6 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/separate_line_h.png"]];
    line6.frame = CGRectMake(itemWidth, yRecordItem + itemHeight, itemWidth, 1);
    [self addSubview:line6];
    
    UIImageView *line7 = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/separate_line_h.png"]];
    line7.frame = CGRectMake(itemWidth * 2, yRecordItem + itemHeight, 93, 1);
    [self addSubview:line7];
    
}



#pragma mark 转登陆页面
-(void)login{
    track();
    Login * loginView = [[Login alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [loginView addTransition:1];
    //    [[RootController sharedRootViewController].view addSubview:login];
    [self addSubview:loginView];
}

#pragma mark 转注册页面
-(void)signup{
    track();
    Signup *signup = [[Signup alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [signup addTransition:1];
    [self addSubview:signup];
}

#pragma mark 转记录页面
- (void)record
{
    RecordView *r = [[RecordView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [r addTransition:1 ];
    [[RootController sharedRootViewController].view addSubview:r];
    
}

#pragma mark 转消息页面
-(void)message{
    Message *message = [[Message alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [message addTransition:1];
    [self addSubview:message];
}

#pragma mark 转电子券页面
-(void)ticket{
    Ticket *ticket = [[Ticket alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [ticket addTransition:1];
    [[RootController sharedRootViewController].view addSubview:ticket];
}

#pragma mark 转名片夹页面
-(void)namecard{
    NameCard *namecard = [[NameCard alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [namecard addTransition:1];
    [[RootController sharedRootViewController].view addSubview:namecard];
}

#pragma mark 转设置页面
-(void)setting{
    Setting *setting = [[Setting alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [setting addTransition:1];
    [[RootController sharedRootViewController].view addSubview:setting];
}

#pragma mark 转收藏页面
-(void)collection{
    Collection *collection = [[Collection alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [collection addTransition:1];
    [[RootController sharedRootViewController].view addSubview:collection];
}

- (void)goDatabase
{
    DataBaseView *database = [[DataBaseView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [database addTransition:1];
     [[RootController sharedRootViewController].view addSubview:database];
}

@end
