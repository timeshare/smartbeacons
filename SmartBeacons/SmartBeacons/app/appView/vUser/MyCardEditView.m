//
//  MyCardEditView.m
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "MyCardEditView.h"
#import "MyCardEditController.h"
@implementation MyCardEditView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self){
        [self initView];
    }
    return self;
}

- (void)initView
{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"编辑我的名片";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //content
    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight)];
    scroll.bounces =NO;
    scroll.showsVerticalScrollIndicator =NO;
    scroll.contentSize = CGSizeMake(View_Size.width, 548 - AppTopHeight);
    self.editC = [[MyCardEditController alloc]init];
    self.editC.view.frame = CGRectMake(0, 0, View_Size.width, 548 - AppTopHeight);
    [scroll addSubview:self.editC.view];
    [self addSubview:scroll];
}

@end
