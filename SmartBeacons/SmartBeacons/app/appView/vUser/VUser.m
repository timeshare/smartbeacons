//
//  VUser.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

//test1111

#import "VUser.h"
#import "Login.h"
#import "Signup.h"
#import "ChangePassword.h"
#import "Setting.h"
#import "Message.h"
#import "Ticket.h"
#import "NameCard.h"
#import "Collection.h"

@implementation VUser

+(id)instance{
    VUser *view  = [[VUser alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}


-(void)initView{
    [self setBackgroundColor:[UIColor whiteColor]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"个人中心";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(60, 55, 200,40) normal:@"assets/image/login.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(login)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(60, 105, 200,40) normal:@"assets/image/register.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(signup)]];
    
    
    [self addSubview:[Global createViewByFrame:CGRectMake(0, 160, View_Size.width, 0.6) color:[UIColor grayColor]]];
    
    
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(28, 200, 65.5,65.5) normal:@"assets/image/message.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(message)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(128, 200, 65.5,65.5) normal:@"assets/image/ticket.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(ticket)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(228, 200, 65.5,65.5) normal:@"assets/image/namecard.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(namecard)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(28, 310, 65.5,65.5) normal:@"assets/image/setting.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(setting)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(128, 310, 65.5,65.5) normal:@"assets/image/collection.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(collection)]];
    [self addSubview:tmpButtom = [Global createBtn:CGRectMake(228, 310, 65.5,65.5) normal:@"assets/image/changepsd.png" down:nil title:nil col:CBlack size:14 target:self action:@selector(changepassword)]];

    UIButton *messageLabel = [[UIButton alloc]initWithFrame:CGRectMake(48, 275, 22.5, 11)];
    [messageLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/messageLabel.png"] forState:UIControlStateNormal];
    messageLabel.enabled = NO;
    [self addSubview:messageLabel];
    
    UIButton *ticketLabel = [[UIButton alloc]initWithFrame:CGRectMake(143, 275, 34.5, 11.5)];
    [ticketLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/ticketLabel.png"] forState:UIControlStateNormal];
    ticketLabel.enabled = NO;
    [self addSubview:ticketLabel];
    
    UIButton *namecardLabel = [[UIButton alloc]initWithFrame:CGRectMake(243, 275, 35, 11.5)];
    [namecardLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/namecardLabel.png"] forState:UIControlStateNormal];
    namecardLabel.enabled = NO;
    [self addSubview:namecardLabel];
    
    UIButton *settingLabel = [[UIButton alloc]initWithFrame:CGRectMake(48, 385, 23, 10.5)];
    [settingLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/settingLabel.png"] forState:UIControlStateNormal];
    settingLabel.enabled = NO;
    [self addSubview:settingLabel];
    
    UIButton *collectionLabel = [[UIButton alloc]initWithFrame:CGRectMake(138, 385, 47.5, 11.5)];
    [collectionLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/collectionLabel.png"] forState:UIControlStateNormal];
    collectionLabel.enabled = NO;
    [self addSubview:collectionLabel];
    
    UIButton *changepsdLabel = [[UIButton alloc]initWithFrame:CGRectMake(238, 385, 47.5, 11.5)];
    [changepsdLabel setBackgroundImage:[UIImage imageNamed:@"assets/image/changepsdLabel.png"] forState:UIControlStateNormal];
    changepsdLabel.enabled = NO;
    [self addSubview:changepsdLabel];

}

#pragma mark 转登陆页面
-(void)login{
    track();
    Login *login = [[Login alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [login addTransition:1];
//    [[RootController sharedRootViewController].view addSubview:login];
    [self addSubview:login];
}

#pragma mark 转注册页面
-(void)signup{
    track();
    Signup *signup = [[Signup alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [signup addTransition:1];
    [self addSubview:signup];
}

#pragma mark 转消息页面
-(void)message{
    Message *message = [[Message alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [message addTransition:1];
    [self addSubview:message];
}

#pragma mark 转电子券页面
-(void)ticket{
    Ticket *ticket = [[Ticket alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [ticket addTransition:1];
    [self addSubview:ticket];
}

#pragma mark 转名片夹页面
-(void)namecard{
    NameCard *namecard = [[NameCard alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [namecard addTransition:1];
    [self addSubview:namecard];
}

#pragma mark 转设置页面
-(void)setting{
    Setting *setting = [[Setting alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [setting addTransition:1];
    [self addSubview:setting];
}

#pragma mark 转收藏页面
-(void)collection{
    Collection *collection = [[Collection alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [collection addTransition:1];
    [self addSubview:collection];
}

#pragma mark 转修改密码页面
-(void)changepassword{
    ChangePassword *pass = [[ChangePassword alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [pass addTransition:1];
    [self addSubview:pass];
}

-(void)dealloc{
    track();
}

@end
