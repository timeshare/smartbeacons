//
//  RecordView.m
//  SmartBeacons
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "RecordView.h"
#import "RecordCell.h"
#import "RecordModel.h"
#import "EEUINoticeMessage.h"
@interface RecordView()
{
    UITableView *_tableView;
    NSMutableArray *_dataList;
    EEUINoticeMessage *_detail;
}
@end

@implementation RecordView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:RGB(228, 228, 228)];
        
        //顶部背景
        UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
        UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
        [self addSubview:imgView];
        
        //加文字
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        title.textAlignment = NSTextAlignmentCenter;
        title.text = @"记 录";
        title.font = [UIFont boldSystemFontOfSize:20];
        title.textColor = [UIColor whiteColor];
        [self addSubview:title];
        
        //返回按钮
        [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
        
        //二维码按钮
        [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
        
        //展示列表
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.backgroundColor = RGB(228, 228, 228);
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:_tableView];
        
        _dataList = [NSMutableArray array];
        RecordModel *r1 = [[RecordModel alloc]init];
        r1.carIconImg = @"assets/userCenter/record/logo1.png";
        r1.carType = @"MODEL S P85D";
        r1.recordTime = @"08/06 14:23";
        r1.carImg = @"assets/userCenter/record/car1.png";
        [_dataList addObject:r1];
        
        RecordModel *r2 = [[RecordModel alloc]init];
        r2.carIconImg = @"assets/userCenter/record/logo2.png";
        r2.carType = @"甲壳虫";
        r2.recordTime = @"08/06 14:23";
        r2.carImg = @"assets/userCenter/record/car2.png";
        [_dataList addObject:r2];
        
        RecordModel *r3 = [[RecordModel alloc]init];
        r3.carIconImg = @"assets/userCenter/record/logo3.png";
        r3.carType = @"GALLARDO";
        r3.recordTime = @"08/06 14:23";
        r3.carImg = @"assets/userCenter/record/car3.png";
        [_dataList addObject:r3];
        
    }
    return self;
}

//tableview的数据源方法
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"cell";
    RecordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(!cell){
        cell = [[RecordCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = RGB(228, 228, 228);
    }
    cell.record = _dataList[indexPath.row];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataList.count;
}

//tableview的代理方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //取消选中状态
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    //展示详情
    _detail = [[EEUINoticeMessage alloc]initWithImage:@"assets/mainpage/info.png" andText:nil];
    [self addSubview:_detail];
    
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}

@end
