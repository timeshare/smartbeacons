//
//  NameCard.m
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "NameCard.h"
#import "CardLabel.h"
#import "NameCardController.h"
#import "NameCardCell.h"
#import "MyNameCardController.h"

@implementation NameCard 
{
    UIButton * _selectedBtn;
    UITableView *_nameList;
    NameCardController *_nameCard;
    UIButton *_detailBtn;
    MyNameCardController *_myNameCard;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //添加两个按钮
    //加入名片夹按钮
    UIButton *actBtn = [[UIButton alloc]initWithFrame:CGRectMake(70, 9, 80, 25)];
    actBtn.layer.cornerRadius = 12;
    [actBtn setTitle:@"名片夹" forState:UIControlStateNormal];
    actBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [actBtn addTarget:self action:@selector(btnClicked :) forControlEvents:UIControlEventTouchUpInside];
   // actBtn.backgroundColor = [UIColor whiteColor];
    actBtn.tag = 0;
  //  [actBtn setTitleColor:[UIColor colorWithRed:131/255.0 green:189/255.0 blue:90/255.0 alpha:1] forState:UIControlStateNormal];
    
    //加入我的名片按钮
    UIButton * listBtn = [[UIButton alloc]initWithFrame:CGRectMake(160, 9, 80, 25)];
    listBtn.layer.cornerRadius = 12;
    [listBtn setTitle:@"我的名片" forState:UIControlStateNormal];
    listBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [listBtn addTarget:self action:@selector(btnClicked :) forControlEvents:UIControlEventTouchUpInside];
    listBtn.tag = 1;
 // listBtn.backgroundColor = [UIColor clearColor];
    
    [self addSubview:actBtn];
    [self addSubview:listBtn];
    [self btnClicked:actBtn];

    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //获取数据
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/user/namecard.plist" ofType:nil];
//    NSMutableArray *arrayList = [NSMutableArray arrayWithContentsOfFile:filePath];
//    self.configList = arrayList;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    NameCardCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(nil==cell){
        cell = [[NSBundle mainBundle]loadNibNamed:@"NameCardCell" owner:nil options:nil][0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if (indexPath.row == 1) {
        [cell.namecardHead setImage:[UIImage imageNamed:@"namecard_logo1.png"] forState:UIControlStateNormal] ;
        cell.namecardName.text = @"王文军";
    }else if (indexPath.row ==2 ){
        [cell.namecardHead setImage:[UIImage imageNamed:@"namecard_logo2.png"] forState:UIControlStateNormal] ;
        cell.namecardName.text = @"Shane_Shen";
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_nameCard == nil) {
        _nameCard = [[NameCardController alloc]init];
    }
    if (_detailBtn == nil) {
        _detailBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _detailBtn.frame = CGRectMake(13, 11, 22, 22);
        [_detailBtn addTarget:self action:@selector(dismissNameCardDetail) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_detailBtn];
    }
    _nameCard.view.frame = CGRectMake(0 + View_Size.width, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight);
    [self addSubview:_nameCard.view];
    [UIView animateWithDuration:0.3 animations:^{
        _nameCard.view.frame =  CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight);
    }];
    
}

- (void)dismissNameCardDetail
{
    _nameCard.view.frame =  CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight);
    [UIView animateWithDuration:0.3 animations:^{
        _nameCard.view.frame =  CGRectMake(0 + View_Size.width, AppTopHeight, View_Size.width, View_Size.height - AppTopHeight);
    } completion:^(BOOL finished) {
        [_nameCard.view removeFromSuperview];
        [_detailBtn removeFromSuperview];
        _nameCard = nil;
        _detailBtn = nil;
    }];
    
}

- (void)btnClicked:(UIButton *)btn
{
    //如果已经选中，则返回
    if (_selectedBtn == btn) return;
    
    _selectedBtn.backgroundColor = [UIColor clearColor];
    [_selectedBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _selectedBtn = btn;
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitleColor:RGB(131, 189, 90) forState:UIControlStateNormal];
    
    if (_selectedBtn.tag == 0) { //点击的是名片夹
        if (_nameList == nil) {
            _nameList = [[UITableView alloc]initWithFrame:CGRectMake(0, AppTopHeight, 320, View_Size.height -AppTopHeight) style:UITableViewStylePlain];
            _nameList.delegate = self;
            _nameList.dataSource = self;
            _nameList.backgroundColor = RGB(228, 228, 228);
            _nameList.separatorStyle = UITableViewCellSelectionStyleNone;
        }
        
        [self addSubview:_nameList];
    }else{ //点击的是我的名片
        if (_detailBtn) {
            [_detailBtn removeFromSuperview];
            [_nameCard.view removeFromSuperview];
            _detailBtn = nil;
            _nameCard = nil;
        }
        _myNameCard = [[MyNameCardController alloc]init];
        _myNameCard.view.frame = CGRectMake(0, 0, View_Size.width, 548 -AppTopHeight);
        UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height -AppTopHeight)];
        scroll.contentSize = CGSizeMake(View_Size.width, 548-AppTopHeight);
        scroll.backgroundColor = RGB(238, 238, 238);
      //  scroll.bounces = NO;
        [self addSubview:scroll];
        [scroll addSubview:_myNameCard.view];
        
    }
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}

@end
