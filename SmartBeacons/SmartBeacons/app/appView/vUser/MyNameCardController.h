//
//  MyNameCardController.h
//  SmartBeacons
//
//  Created by mac on 14-11-14.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyCardEditController.h"
@interface MyNameCardController : UIViewController <EditFinishedDelegate>
@property (weak, nonatomic) IBOutlet UIButton *editBtn;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNo;
@property (weak, nonatomic) IBOutlet UIImageView *lblHeadpic;
@property (weak, nonatomic) IBOutlet UILabel *lblPos;
@property (weak, nonatomic) IBOutlet UILabel *lblCmpy;
@property (weak, nonatomic) IBOutlet UILabel *lblTel;
@property (weak, nonatomic) IBOutlet UILabel *lblMail;
@property (weak, nonatomic) IBOutlet UILabel *lblQQ;

@end
