//
//  CardLabel.h
//  SmartBeacons
//
//  Created by Alex on 14-10-11.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardLabel : UILabel

@property(nonatomic,strong)NSDictionary *configData;

-(id)initWithFrame:(CGRect)frame andImage:(NSString *)image andData:(NSMutableDictionary *)dataDictionary;

//-(id)initWithFrame:(CGRect)frame andColor:() andData:(NSMutableDictionary *)dataDictionary;


@end
