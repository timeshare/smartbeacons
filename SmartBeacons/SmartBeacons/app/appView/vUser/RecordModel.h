//
//  RecordModel.h
//  SmartBeacons
//
//  Created by mac on 14-11-12.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecordModel : NSObject
@property (nonatomic,copy) NSString *carIconImg;
@property (nonatomic,copy) NSString *carType;
@property (nonatomic,copy) NSString *recordTime;
@property (nonatomic,copy) NSString *carImg;
@end
