//
//  Message.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Message.h"
#import "MessageCell.h"
#import "MessageInformation.h"

@implementation Message

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"消    息";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //取得数据
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/user/message.plist" ofType:nil];
    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
    self.configList = arrayList;
    
    //加入tableView
    UITableView *tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, View_Size.width, View_Size.height) style:UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    //取消cell之间的间隔线
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableview];
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.configList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Mcell = @"Mcell";
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:Mcell];
    if(cell == nil){
        cell = [[MessageCell alloc]initCellByData:[self.configList objectAtIndex:indexPath.row]];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    if(indexPath.row%2 != 0 ){
        cell.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1];
    }
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        MessageInformation *messageInformation = [[MessageInformation alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
        [messageInformation addTransition:1];
        [self addSubview:messageInformation];
    }
}



@end
