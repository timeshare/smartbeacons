//
//  Signup.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Signup.h"

@implementation Signup

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}


-(void)initView{
    
    [self setBackgroundColor:  [UIColor colorWithRed:0.97 green:0.97 blue:0.97 alpha:1.0]];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"注    册";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //UITextField--用户名
    _name = [[UITextField alloc]initWithFrame:CGRectMake(45, 60, 260, 40)];
    _name.inputAccessoryView.frame = CGRectMake(300, 0, 265, 40);
    _name.backgroundColor = [UIColor whiteColor];
    _name.placeholder = @"用户名";
    _name.returnKeyType = UIReturnKeyDone;
    [self addSubview:_name];
    
    //左侧图片加载
    UIImageView *nameView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 60, 30, 40)];
    [nameView setBackgroundColor:[UIColor whiteColor]];
    UIImage *nameImg = [UIImage imageNamed:@"assets/image/user.png"];
    [nameView setImage:nameImg];
    nameView.contentMode =  UIViewContentModeCenter;
    [self addSubview:nameView];
    
    
    //UITextField--密码
    _password = [[UITextField alloc]initWithFrame:CGRectMake(45, 110, 260, 40)];
    _password.backgroundColor = [UIColor whiteColor];
    _password.placeholder = @"输入您的密码";
    _password.secureTextEntry = YES;
    [self addSubview:_password];
    
    //左侧图片加载
    UIImageView *passwordView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 110, 30, 40)];
    [passwordView setBackgroundColor:[UIColor whiteColor]];
    UIImage *passwordImg = [UIImage imageNamed:@"assets/image/password.png"];
    [passwordView setImage:passwordImg];
    passwordView.contentMode =  UIViewContentModeCenter;
    [self addSubview:passwordView];
    
    //UITextField--电话
    _phone = [[UITextField alloc]initWithFrame:CGRectMake(45, 160, 260, 40)];
    _phone.backgroundColor = [UIColor whiteColor];
    _phone.placeholder = @"输入您的电话";
    _phone.keyboardType = UIKeyboardTypePhonePad;
    [self addSubview:_phone];
    
    UIImageView *phoneView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 160, 30, 40)];
    [phoneView setBackgroundColor:[UIColor whiteColor]];
    UIImage *phoneImg = [UIImage imageNamed:@"assets/image/phone.png"];
    [phoneView setImage:phoneImg];
    phoneView.contentMode =  UIViewContentModeCenter;
    [self addSubview:phoneView];

    
    
    [self addSubview:[Global createBtn:CGRectMake(15, 215, 290, 45) normal:@"assets/image/signupbtn.png" down:nil title:nil col:CBlack size:16 target:self action:nil]];
    
    //合作登陆
    UIButton *cooperationbtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 290, 95, 15)];
    [cooperationbtn setBackgroundImage:[UIImage imageNamed:@"assets/image/cooperation.png"] forState:UIControlStateNormal];
    cooperationbtn.enabled = NO;
    [self addSubview:cooperationbtn];
    
    
    UIButton *weibo = [[UIButton alloc]initWithFrame:CGRectMake(20, 330, 65, 65)];
    UIButton *qq = [[UIButton alloc]initWithFrame:CGRectMake(130, 330, 65, 65)];
    UIButton *weixin = [[UIButton alloc]initWithFrame:CGRectMake(240, 330, 65, 65)];
    
    
    [weibo setBackgroundImage:[UIImage imageNamed:@"assets/image/weibo.png"] forState:UIControlStateNormal];
    [qq setBackgroundImage:[UIImage imageNamed:@"assets/image/qq.png"] forState:UIControlStateNormal];
    [weixin setBackgroundImage:[UIImage imageNamed:@"assets/image/weixin.png"] forState:UIControlStateNormal];
    
    
    [self addSubview:weibo];
    [self addSubview:qq];
    [self addSubview:weixin];

    //添加手势响应
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    tapGr.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tapGr];
    
}

#pragma mark 点击屏幕隐藏键盘
-(void)viewTapped:(UITapGestureRecognizer*)tapGr
{
    [_name resignFirstResponder];
    [_password resignFirstResponder];
    [_phone resignFirstResponder];
}

@end
