//
//  Ticket.m
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "Ticket.h"
#import "TicketCell.h"
#import "TicketDetailView.h"
@implementation Ticket

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"电子券";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
//    //获取数据
//    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/user/ticket.plist" ofType:nil];
//    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
//    self.configList = arrayList;
    
    self.contentTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, View_Size.width, View_Size.height-44)];
    self.contentTable.backgroundColor = RGB(228, 228, 228);
    self.contentTable.delegate = self;
    self.contentTable.dataSource = self;
    self.contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:self.contentTable];
    
}

#pragma mark tableviewDataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifierCell = @"cell";
    
    TicketCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle]loadNibNamed:@"TicketCell" owner:nil options:nil][0];
    }
    if (indexPath.row == 1) {
        cell.TicketLogo.image = [UIImage imageNamed:@"ticket_logo2.png"];
         cell.TicketUsed.image = [UIImage imageNamed:@"ticket_used.png"];
        [cell.TicketType setBackgroundImage:[UIImage imageNamed:@"ticket_eat.png"] forState:UIControlStateNormal];
        [cell.TicketType setTitle:@"餐饮券" forState:UIControlStateNormal];
        cell.TicketTitle.text = @"麦当劳";
//        CGFloat dimY = cell.frame.size.height * indexPath.row + AppTopHeight;
//        CGRect f = (CGRect){CGPointMake(0, dimY),cell.frame.size};
//        UIView *dimView = [[UIView alloc]initWithFrame:f];
//        dimView.backgroundColor = [UIColor colorWithRed:228/255 green:228/255 blue:228/255 alpha:0.1];
//        [self addSubview:dimView];
    }
    if (indexPath.row == 2) {
        cell.TicketLogo.image = [UIImage imageNamed:@"ticket_logo1.png"];
       // cell.TicketUsed.image = [UIImage imageNamed:@"ticket_used.png"];
       // [cell.TicketType setBackgroundImage:[UIImage imageNamed:@"ticket_eat.png"] forState:UIControlStateNormal];
        
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 93;
}

#pragma mark tableview代理方法

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TicketDetailView *detail = [[TicketDetailView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [detail addTransition:1];
    [[RootController sharedRootViewController].view addSubview:detail];
}

- (void)scan
{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"scan" object:nil];
}

@end
