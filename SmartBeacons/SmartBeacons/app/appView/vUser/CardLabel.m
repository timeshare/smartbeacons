//
//  CardLabel.m
//  SmartBeacons
//
//  Created by Alex on 14-10-11.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "CardLabel.h"

@implementation CardLabel

-(id)initWithFrame:(CGRect)frame andImage:(NSString *)image andData:(NSMutableDictionary *)dataDictionary{
    self = [super init];
    if (self) {
        self.frame = frame;
        
        //设置背景图片
        NSString *string = [[NSBundle mainBundle] pathForResource:image ofType:nil];
        UIImage *imgName = [[UIImage alloc]initWithContentsOfFile:string];
        UIImageView *img = [[UIImageView alloc]initWithImage:imgName];
        [self addSubview:img];
        
        self.configData = dataDictionary;
        int startx = 10;
        
        NSString *name = [NSString stringWithFormat:@"%@",[self.configData objectForKey:@"name"]];
        id sexImage = [self.configData objectForKey:@"sexImage"];
        NSString *company  = [NSString stringWithFormat:@"公司：%@",[self.configData objectForKey:@"company"]];
        NSString *position  = [NSString stringWithFormat:@"职位：%@",[self.configData objectForKey:@"position"]];
        NSString *telephone  = [NSString stringWithFormat:@"手机：%@",[self.configData objectForKey:@"telephone"]];
        NSString *qq  = [NSString stringWithFormat:@"QQ：%@",[self.configData objectForKey:@"qq"]];
        NSString *email  = [NSString stringWithFormat:@"邮箱：%@",[self.configData objectForKey:@"email"]];
        
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx, 3, 50, 25) label:name lines:0 fontSize:18 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
        [self addSubview:[Global createImage:sexImage fitSize:CGSizeMake(10, 10) center:CGPointMake(60, 15)]];
        
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx, 30, 300, 25) label:company lines:0 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx, 55, 150, 25) label:position lines:0 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx+115, 55, 180, 25) label:telephone lines:0 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx, 80, 150, 25) label:qq lines:0 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        [self addSubview:tmpLabel = [Global createLabel:CGRectMake(startx+115, 80, 180, 25) label:email lines:0 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        
        
    }
    return self;
}

@end
