//
//  MessageInformation.h
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface MessageInformation : EEView<UITableViewDataSource,UITableViewDelegate>


@property(nonatomic,strong)NSDictionary *configData;

@property (nonatomic, strong)NSArray *configList;


@end
