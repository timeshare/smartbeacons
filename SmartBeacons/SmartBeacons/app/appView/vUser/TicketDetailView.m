//
//  TicketDetailView.m
//  SmartBeacons
//
//  Created by mac on 14-11-13.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "TicketDetailView.h"

@implementation TicketDetailView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView
{
    [self setBackgroundColor:RGB(228, 228, 228)];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"电子劵";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //二维码按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //整体背景
    UIImageView *bg = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"assets/userCenter/ticket/ticket_detail.png"]];
    bg.frame = CGRectMake(0, AppTopHeight, View_Size.width, self.frame.size.height - AppTopHeight);
    [self addSubview:bg];
    
    //导航按钮
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"assets/userCenter/ticket/ticket_planebtn.png"] forState:UIControlStateNormal];
    btn.frame = CGRectMake(260, 120, 15.5, 14.5);
    [btn addTarget:self action:@selector(loc) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:btn];
}

- (void) loc
{
    
}

- (void)scan
{

}

@end
