//
//  MessageInformation.m
//  SmartBeacons
//
//  Created by Alex on 14-10-13.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "MessageInformation.h"
#import "MessageInformationCell.h"

@implementation MessageInformation
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

-(void)initView{
    [self setBackgroundColor:CWhite];
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加文字
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = @"消    息";
    title.font = [UIFont boldSystemFontOfSize:20];
    title.textColor = [UIColor whiteColor];
    [self addSubview:title];
    
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(13, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    //取得数据
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/user/MessageInformation.plist" ofType:nil];
    NSMutableArray *arrayList= [NSMutableArray arrayWithContentsOfFile:filePath];
    self.configList = arrayList;
    
    //加入tableView
    UITableView *tableview = [[UITableView alloc]initWithFrame:CGRectMake(0, 44, View_Size.width, View_Size.height-AppButtomHeight-44) style:UITableViewStylePlain];
    tableview.delegate = self;
    tableview.dataSource = self;
    //取消cell之间的间隔线
    tableview.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableview];
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.configList count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
        return 90;
    }else{
        return 80;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Mcell = @"Mcell";
    MessageInformationCell *cell = [tableView dequeueReusableCellWithIdentifier:Mcell];
    if(cell == nil){
        cell = [[MessageInformationCell alloc]initCellByData:[self.configList objectAtIndex:indexPath.row]];
        
        if (indexPath.row == 0) {
            UIView *aView = [[UIView alloc]initWithFrame:cell.frame];
            aView.backgroundColor = [UIColor whiteColor];
            cell.selectedBackgroundView = aView;

        }
    }
    if(indexPath.row%2 != 0 ){
        cell.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1];
    }
    return  cell;
}


@end
