//
//  NameCardModel.h
//  SmartBeacons
//
//  Created by mac on 14-11-18.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ITTObjectSingleton.h"
@interface NameCardModel : NSObject
@property (nonatomic,copy) NSString *name;

@property (copy, nonatomic)  NSString *phoneNo;
@property (copy, nonatomic)  NSString *headpic;
@property (copy, nonatomic)  NSString *pos;
@property (copy, nonatomic)  NSString *cmpy;
@property (copy, nonatomic)  NSString *tel;
@property (copy, nonatomic)  NSString *mail;
@property (copy, nonatomic)  NSString *qq;

+ (NameCardModel *)shareNameCardModel;
@end
