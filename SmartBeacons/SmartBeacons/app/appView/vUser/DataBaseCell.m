//
//  DataBaseCell.m
//  SmartBeacons
//
//  Created by mac on 14-11-20.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "DataBaseCell.h"

@implementation DataBaseCell

- (void)awakeFromNib {
    // Initialization code
    self.bgView.layer.cornerRadius = 5;
    self.bgView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
