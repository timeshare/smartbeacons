//
//  DataBaseCell.h
//  SmartBeacons
//
//  Created by mac on 14-11-20.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataBaseCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UILabel *carMaker;
@property (weak, nonatomic) IBOutlet UILabel *carMakerEg;
@property (weak, nonatomic) IBOutlet UIImageView *carLogoImg;

@end
