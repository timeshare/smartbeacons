//
//  Collection.h
//  SmartBeacons
//
//  Created by mac on 14-9-4.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "BaseClass.h"

@interface Collection : EEView<UITableViewDataSource,UITableViewDelegate>{
    BOOL _isZHOpen,_isZSOpen,_isHDOpen;
    UIButton *_ZHBtn,*_ZSBtn,*_HDBtn;
}

@property(nonatomic,strong)NSMutableArray *configList;

@property(nonatomic, strong)UITableView *contentTable;

@end
