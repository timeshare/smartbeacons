//
//  BMapViewController.h
//  SmartBeacons
//
//  Created by Alex on 14/11/20.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BMapKit.h"
@interface BMapViewController : UIViewController<BMKMapViewDelegate,BMKLocationServiceDelegate,BMKRouteSearchDelegate>

@property (nonatomic, strong)BMKMapView *bmkMapView;
@property (nonatomic, strong) BMKLocationService *locService;
@property (nonatomic, strong) BMKRouteSearch *routesearch;

@property (nonatomic)CLLocationCoordinate2D endCoor;
@property (nonatomic)CLLocationCoordinate2D startCoor;

@end
