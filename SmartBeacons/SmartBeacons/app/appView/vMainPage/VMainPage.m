//
//  VMainPage.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "VMainPage.h"
#import "SmartScroll.h"
#import "ShopInforMation.h"
#import "GuideView.h"


@implementation VMainPage
+(id)instance{
    VMainPage *view  = [[VMainPage alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    [view initView];
    return view;
}

-(void)initView{
    
    //顶部背景
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    
    //添加scrollview
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight)];
    _scrollView.contentSize = CGSizeMake(View_Size.width, 268+190);
    _scrollView.showsVerticalScrollIndicator = NO;
//    _scrollView.bounces = NO;
    [self addSubview:_scrollView];
    
    //图片滑动
    self.picArray = [[NSMutableArray alloc]init];
    NSString *filepath1 = @"assets/mainpage/car@2x.png";
    NSString *filepath2 = @"assets/mainpage/car2@2x.png";
    NSString *filepath3 = @"assets/mainpage/car3@2x.png";
    NSDictionary *dic1 = [NSDictionary dictionaryWithObjectsAndKeys:filepath1,@"posterURL", nil];
    NSDictionary *dic2 = [NSDictionary dictionaryWithObjectsAndKeys:filepath2,@"posterURL", nil];
    NSDictionary *dic3 = [NSDictionary dictionaryWithObjectsAndKeys:filepath3,@"posterURL", nil];
    
    [self.picArray addObject:dic1];
    [self.picArray addObject:dic2];
    [self.picArray addObject:dic3];
    
    SmartScroll *scroll = [[SmartScroll alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, 190) withData:self.picArray alwaysToFill:YES];
    [_scrollView addSubview:scroll];
    
    //加导览按钮
    UIButton *guideBtn = [[UIButton alloc]init];
    [_scrollView addSubview: guideBtn = [Global createBtn:CGRectMake(0, 0, 201, 40) normal:@"assets/mainpage/guide.png" down:@"assets/mainpage/guide_no@2x.png" target:self action:@selector(guide)]];
    guideBtn.center = CGPointMake(View_Size.width/2, 51+190);
    
    //画灰线
    [_scrollView addSubview: tmpView = [Global createViewByFrame:CGRectMake(0, 0, 280, 1) color:[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1]]];
    tmpView.center = CGPointMake(View_Size.width/2, 100+190);
    
    //加入内容
    [_scrollView addSubview:[Global createImage:@"assets/mainpage/greenbar.png" center:CGPointMake(30, 135+190)]];
    [_scrollView addSubview:[Global createLabel:CGRectMake(45, 110+190, 300, 30) label:@"2014 AUTO PUDONG" lines:1 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    [_scrollView addSubview:tmpLabel = [Global createLabel:CGRectMake(45, 130+190, 300, 30) label:@"浦东国际汽车展览会" lines:1 fontSize:16 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    tmpLabel.font = [UIFont boldSystemFontOfSize:18];
    
    [_scrollView addSubview:[Global createImage:@"assets/mainpage/locationicon.png" center:CGPointMake(80, 185+190)]];
    [_scrollView addSubview:[Global createLabel:CGRectMake(95, 170+190, 300, 30) label:@"地点：上海浦东国际展览中心" lines:1 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    [_scrollView addSubview:[Global createBtn:CGRectMake(295-15.5/2, 185-14.5/2+190, 15.5, 14.5) normal:@"assets/mainpage/planeicon.png" down:nil target:self action:@selector(goMap)]];
    
    [_scrollView addSubview:[Global createImage:@"assets/mainpage/dateicon.png" center:CGPointMake(80, 210+190)]];
    [_scrollView addSubview:[Global createLabel:CGRectMake(95, 195+190, 300, 30) label:@"日期：2014-10-21 ~ 2014-10-31" lines:1 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    
    [_scrollView addSubview:[Global createImage:@"assets/mainpage/timeicon.png" center:CGPointMake(80, 235+190)]];
    [_scrollView addSubview:[Global createLabel:CGRectMake(95, 220+190, 300, 30) label:@"时段：9：00 ~ 19：00" lines:1 fontSize:14 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    tap.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tap];
}
-(void)viewTapped:(UITapGestureRecognizer *)tap{
    [_searchBar resignFirstResponder];
}

#pragma mark 扫描二维码
-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}

#pragma mark 调用地图
-(void)goMap{
    _mapVC = [[BMapViewController alloc]init];
    _mapVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [[RootController sharedRootViewController]presentViewController:_mapVC animated:YES completion:nil];
}

#pragma mark 导览
-(void)guide{
    GuideView *guideView = [[GuideView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height-AppButtomHeight)];
    [self addTransition:1];
    [self addSubview:guideView];
}

-(void)dealloc{
    track();
}

@end
