//
//  VMainPage.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "BMapViewController.h"

@interface VMainPage : EEView<UISearchBarDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong)UIScrollView *scrollView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong)NSMutableArray *picArray;

//地图
@property (nonatomic, strong)CLLocationManager *locationManager;
@property (nonatomic)CLLocationCoordinate2D coor;

@property (nonatomic, strong) BMapViewController *mapVC;
@end

