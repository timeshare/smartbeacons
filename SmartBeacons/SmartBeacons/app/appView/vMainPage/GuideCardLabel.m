//
//  GuideCardLabel.m
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "GuideCardLabel.h"
#import "EEUINoticeMessage.h"

@implementation GuideCardLabel

-(id)initWithFrame:(CGRect)frame andData:(NSMutableDictionary *)dataDictionary{
    self = [super init];
    if (self) {
        self.frame = frame;
        self.userInteractionEnabled = YES;
        //设置背景图片
        UIImage *imgName = [UIImage imageNamed:@"assets/mainpage/guideCard.png"];
        UIImageView *img = [[UIImageView alloc]initWithImage:imgName];
        img.frame = CGRectMake(0, 0, 300, 105);
        [self addSubview:img];
        
        NSString *title = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"title"]];
        NSString *content = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"content"]];
        id image = [dataDictionary objectForKey:@"picture"];
        id icon = [dataDictionary objectForKey:@"icon"];
        
        [self addSubview:[Global createLabel:CGRectMake(50, 5, 250, 30) label:title lines:1 fontSize:15 fontName:nil textcolor:[UIColor blackColor] align:NSTextAlignmentLeft]];
        
        if(image == nil || image == [NSNull null]){
            [self addSubview:tmpLabel = [Global createLabel:CGRectMake(15, 28, 270, 80) label:content lines:2 fontSize:13 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        }else{
            [self addSubview:tmpLabel = [Global createLabel:CGRectMake(15, 28, 205, 80) label:content lines:2 fontSize:13 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
        }

        [self addSubview:[Global createImage:image fitSize:CGSizeMake(60, 50) center:CGPointMake(260, 65)]];
        [self addSubview:[Global createImage:icon center:CGPointMake(16, 16)]];
        
        UIButton *btn = [[UIButton alloc]initWithFrame:img.frame];
        [btn addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:btn];
        
    }
    return self;
}

-(void)click{
    //展示详情
    EEUINoticeMessage *notice = [[EEUINoticeMessage alloc]initWithImage:@"assets/mainpage/info.png" andText:nil];
    [[RootController sharedRootViewController].view addSubview:notice];
}

@end
