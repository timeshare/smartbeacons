//
//  GuideCardLabel.h
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideCardLabel : UILabel

-(id)initWithFrame:(CGRect)frame andData:(NSMutableDictionary *)dataDictionary;


@end
