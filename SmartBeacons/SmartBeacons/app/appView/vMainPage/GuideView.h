//
//  GuideView.h
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "BaseClass.h"

@interface GuideView : EEView<UISearchBarDelegate>

@property (nonatomic, strong)NSMutableArray *dataArray;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIScrollView *scrollView;

@end
