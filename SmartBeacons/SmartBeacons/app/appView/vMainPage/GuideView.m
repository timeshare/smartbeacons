//
//  GuideView.m
//  SmartBeacons
//
//  Created by Alex on 14/10/31.
//  Copyright (c) 2014年 Alex. All rights reserved.
//

#import "GuideView.h"
#import "GuideCardLabel.h"
@implementation GuideView

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        [self initView];
    }
    return self;
}

-(void)initView{
    self.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
    
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, AppTopHeight, View_Size.width, View_Size.height-AppTopHeight-AppButtomHeight)];
    _scrollView.contentSize = CGSizeMake(View_Size.width, 455);
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    //顶部条
    UIImage *image = [UIImage imageNamed:@"assets/image/topBack.png"];
    UIImageView *imgView = [[UIImageView alloc]initWithImage:image];
    [self addSubview:imgView];
    
    //加搜索框
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, 230, 30)];
    for (UIView *subview in _searchBar.subviews) {
        
        if ([subview isKindOfClass:NSClassFromString(@"UIView")] && subview.subviews.count > 0) {
            [[subview.subviews objectAtIndex:0] removeFromSuperview];
            break;
        }
    }
    _searchBar.delegate = self;
    _searchBar.center = CGPointMake(View_Size.width/2, 22.5);
    _searchBar.backgroundColor = [UIColor clearColor];
    _searchBar.backgroundImage = nil;
    _searchBar.placeholder = @"输入车款/品牌/活动关键字";
    [self addSubview:_searchBar];
    
    //加入二维码扫描按钮
    [self addSubview:[Global createBtn:CGRectMake(280, 10, 25, 25) normal:@"assets/mainpage/erweimaicon.png" down:nil target:self action:@selector(scan)]];
    //返回按钮
    [self addSubview:[Global createBtn:CGRectMake(11, 11, 22, 22) normal:@"assets/image/return.png" down:@"assets/image/return.png" target:self action:@selector(eeRemoveRight)]];
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"assets/mainpage/guideCard.plist" ofType:nil];
    self.dataArray = [[NSMutableArray alloc]initWithContentsOfFile:filePath];
    
    [self addCard];
}

-(void)addCard{
    GuideCardLabel *card1 = [[GuideCardLabel alloc]initWithFrame:CGRectMake(10, 10, 300, 105) andData:[self.dataArray objectAtIndex:0]];
    [_scrollView addSubview:card1];
    
    GuideCardLabel *card2 = [[GuideCardLabel alloc]initWithFrame:CGRectMake(10, 125, 300, 105) andData:[self.dataArray objectAtIndex:1]];
    [_scrollView addSubview:card2];
    
    GuideCardLabel *card3 = [[GuideCardLabel alloc]initWithFrame:CGRectMake(10, 240, 300, 105) andData:[self.dataArray objectAtIndex:2]];
    [_scrollView addSubview:card3];
    
    GuideCardLabel *card4 = [[GuideCardLabel alloc]initWithFrame:CGRectMake(10, 355, 300, 105) andData:[self.dataArray objectAtIndex:3]];
    [_scrollView addSubview:card4];
}

-(void)scan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"scan" object:nil];
}

@end
