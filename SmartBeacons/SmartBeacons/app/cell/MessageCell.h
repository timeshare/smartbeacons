//
//  MessageCell.h
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageCell : UITableViewCell

@property(nonatomic, strong)NSDictionary *messageData;

-(id)initCellByData:(NSMutableDictionary *)dataDictionary;

@end
