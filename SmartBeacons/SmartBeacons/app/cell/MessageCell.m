//
//  MessageCell.m
//  SmartBeacons
//
//  Created by mac on 14-9-2.
//  Copyright (c) 2014年 mac. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(id)initCellByData:(NSMutableDictionary *)dataDictionary{
    self = [super init];
    if(self){
        self.messageData = dataDictionary;
        
        NSString *imageFile = [self.messageData objectForKey:@"picture"];
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(20, 10, 40.5, 40)];
        [image setImage:[UIImage imageNamed:imageFile]];
        [self addSubview:image];
        
        NSString *imageTip = [self.messageData objectForKey:@"tip"];
        UIImageView *image2 = [[UIImageView alloc]initWithFrame:CGRectMake(50, 8, 13, 13)];
        [image2 setImage:[UIImage imageNamed:imageTip]];
        [self addSubview:image2];
        
        NSString *title = [self.messageData objectForKey:@"title"];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 0, 250, 40)];
        titleLabel.text = title;
        titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [titleLabel setTextColor:[UIColor grayColor]];
        [self addSubview:titleLabel];
        
        NSString *time = [self.messageData objectForKey:@"time"];
        UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(80, 23, 250, 40)];
        timeLabel.text = time;
        timeLabel.font = [UIFont systemFontOfSize:14];
        [timeLabel setTextColor:[UIColor grayColor]];
        [self addSubview:timeLabel];
    }
    return  self;
}

-(void)dealloc{
    track();
    self.messageData = nil;
}

@end
