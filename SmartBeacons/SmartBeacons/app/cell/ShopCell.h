//
//  ShopCell.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopCell : UITableViewCell

-(id)initCellByData:(NSDictionary *)dataDictionary;

@property(nonatomic,strong)NSDictionary *configData;

@end
