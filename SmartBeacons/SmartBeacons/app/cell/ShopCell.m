//
//  ShopCell.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-20.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "ShopCell.h"

@implementation ShopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

-(id)initCellByData:(NSDictionary *)dataDictionary{
    self = [super init];
    if (self) {
        self.configData = dataDictionary;
        
        NSString *name     = [self.configData objectForKey:@"name"];
        NSString *imageFile = [self.configData objectForKey:@"pic"];
        
        [self addSubview:[Global createImage:imageFile center:CGPointMake(35, 22)]];
        [self addSubview:[Global createLabel:CGRectMake(90, 10, 220, 25) label:name lines:0 fontSize:15 fontName:nil textcolor:[UIColor grayColor] align:NSTextAlignmentLeft]];
    }
    return self;
}


-(void)dealloc{
    track();
    self.configData = nil;
    
}


@end
