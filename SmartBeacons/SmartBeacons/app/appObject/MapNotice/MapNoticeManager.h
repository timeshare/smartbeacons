//
//  MapNoticeManager.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-23.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"
#import <CoreLocation/CoreLocation.h>
#import "VMap.h"
#import "EENoticeMessage.h"

@interface MapNoticeManager : NSObject{
    int lastMajor;
    int lastMinor;
}

@property(nonatomic,weak)id vmap;

+(MapNoticeManager *)shareInstance;


-(void)catchIbeaconNotice:(CLBeacon *)nearestBeacon;

@end
