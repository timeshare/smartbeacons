//
//  MapNoticeManager.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-23.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "MapNoticeManager.h"

@implementation MapNoticeManager

static MapNoticeManager* instance;

+(MapNoticeManager *)shareInstance{
    if (instance==nil) {
        instance = [[MapNoticeManager alloc] init];
    }
    return instance;
}

-(void)catchIbeaconNotice:(CLBeacon *)nearestBeacon{
    int curMajor = [nearestBeacon.major intValue];
    int curMinor = [nearestBeacon.minor intValue];
    
//    trace(@"major:%d   minor:%d",curMajor,curMinor);
    
    if (lastMajor==curMajor && lastMinor==curMinor) {//发现相同的iBeacon
        return;
    }
    if (curMajor==1) {
        if (curMinor==2) {
//            [[EENoticeMessage getInstance] noticeText:@"发现沃尔沃体验馆"];
            [[EENoticeMessage getInstance] noticeImage:@"assets/mainpage/info.png" text:@"2"];
//            [self.vmap setuserPositionBuilderId:112 isAuto:YES];
        }else if (curMinor==3){
//            [[EENoticeMessage getInstance] noticeText:@"发现法拉利体验馆"];
            [[EENoticeMessage getInstance] noticeImage:@"assets/mainpage/info2.png" text:@"3"];
//            [self.vmap setuserPositionBuilderId:106 isAuto:YES];
        }else if (curMinor==4){
//            [[EENoticeMessage getInstance] noticeText:@"发现凯迪拉克体验馆"];
            [[EENoticeMessage getInstance] noticeImage:@"assets/mainpage/info2.png" text:@"4"];
//            [self.vmap setuserPositionBuilderId:119 isAuto:YES];
        }else if (curMinor==5){
//            [[EENoticeMessage getInstance] noticeText:@"发现保时捷体验馆"];
            [[EENoticeMessage getInstance] noticeImage:@"assets/mainpage/info2.png" text:@"5"];
//            [self.vmap setuserPositionBuilderId:103 isAuto:YES];
        }
    }
    lastMajor = [nearestBeacon.major intValue];
    lastMinor = [nearestBeacon.minor intValue];
}

@end
