//
//  AppSlider.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-19.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "AppSlider.h"

@implementation AppSlider

+(id)instance{
    AppSlider *appSlider = [[AppSlider alloc] init];
    [appSlider initView];
    return appSlider;
}


-(void)initView{
    [self setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    
    [self setBackgroundColor:[UIColor whiteColor]];
}


@end
