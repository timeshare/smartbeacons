//
//  AppDelegate.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

/*
                _ooOoo_
               o8888888o
               88" . "88
               (| -_- |)
               O\  =  /O
            ____/`---'\____
          .'  \\|     |//  `.
         /  \\|||  :  |||//  \
        /  _||||| -:- |||||-  \
        |   | \\\  -  /// |   |
        | \_|  ''\---/''  |   |
        \  .-\__  `-`  ___/-. /
      ___`. .'  /--.--\  `. . __
   ."" '<  `.___\_<|>_/___.'  >'"".
 | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 \  \ `-.   \_ __\ /__ _/   .-` /  /
 ======`-.____`-.___\_____/___.-`____.-'======
 `=---='
 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
 */

#import "AppDelegate.h"
#import "RootController.h"
//#import "VActionController.h"
#import <ShareSDK/ShareSDK.h>
#import <Parse/Parse.h>
#import "WeiboSDK.h"
#import "WXApi.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <QZoneConnection/ISSQZoneApp.h>

#import "BMapViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    //注册百度地图
    self.mapManager = [[BMKMapManager alloc] init];
    BOOL ret = [self.mapManager start:@"aoKaHnq3GRAWCIfkIeaMmfHj" generalDelegate:self];
    if (!ret) {
        NSLog(@"失败");
    }
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    [Global sharedGlobal];
    
    RootController *rootcontroller = [RootController sharedRootViewController];
    self.window.rootViewController = rootcontroller;
    
    //shareSDK 登陆设置
    [self shareSDKSetting];
    
    return YES;
}

- (void)onGetNetworkState:(int)iError
{
    if (0 == iError) {
        NSLog(@"联网成功");
    }
    else{
        NSLog(@"onGetNetworkState %d",iError);
    }
    
}

- (void)onGetPermissionState:(int)iError
{
    if (0 == iError) {
        NSLog(@"授权成功");
    }
    else {
        NSLog(@"onGetPermissionState %d",iError);
    }
}

- (void)shareSDKSetting
{
    [ShareSDK registerApp:@"44cf6a45ff53"];
    
    //1.sina Weibo
    [ShareSDK connectSinaWeiboWithAppKey:@"2452646394"
                               appSecret:@"03f32c2ef446fd0171447ea24bd697fb"
                             redirectUri:@"http://www.baidu.com"
                             weiboSDKCls:[WeiboSDK class]];
    
   // 2.wechat
    [ShareSDK connectWeChatWithAppId:@"wx6f70a9f308f74728"
                           appSecret:@"1ad1d6e9809b8f486e94e12473db49fe"
                           wechatCls:[WXApi class]];
    
    //3.Q zone
    [ShareSDK connectQZoneWithAppKey:@"1103527963"
                           appSecret:@"mxvVqnnPU0ogue8T"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    //4.QQ
    [ShareSDK connectQQWithQZoneAppKey:@"1103527963"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    [Parse setApplicationId:@"REpO3cXZ8RZoKyisH5gv85n34gISmO5Du4bcClp4"
                  clientKey:@"wgGzLeNEzRU8PLRv7Zp6PFceGjMSYyKCMFMsSPQG"];
    
    //开启QQ空间网页授权开关
    id<ISSQZoneApp> app =(id<ISSQZoneApp>)[ShareSDK getClientWithType:ShareTypeQQSpace];
    [app setIsAllowWebAuthorize:YES];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url wxDelegate:nil];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    //UITapGesture
    [[NSNotificationCenter defaultCenter] postNotificationName:MapNotice object:@"appRewake"];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
