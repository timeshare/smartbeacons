//
//  RootController.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RootController : UIViewController{
    id currentShowView;
}

+ (RootController*) sharedRootViewController;
-(void)replaceView:(NSString *)className;

@end
