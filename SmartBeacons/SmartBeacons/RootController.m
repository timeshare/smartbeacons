//
//  RootController.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "RootController.h"
#import "EENoticeMessage.h"

static RootController *_rootViewController = NULL;

@interface RootController ()

@end

@implementation RootController

+ (RootController*) sharedRootViewController
{
    if (!_rootViewController) {
        _rootViewController = [[RootController alloc] init];
    }
    return _rootViewController;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    RootView *rootView = [[RootView alloc] initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    self.view = rootView;
    [rootView setBackgroundColor:[UIColor whiteColor]];
    
    //存储用户的偏好设置,存储在本地,比如:程序是否是第一次加载
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (![userDefaults boolForKey:@"aa"]) {
        GO(@"BlueToothView");
        [userDefaults setBool:YES forKey:@"aa"];
        //立即同步
        [userDefaults synchronize];
    }else{
        GO(@"AppView");
    }
    
    //初始化
    //GO(@"AppSlider");
//    GO(@"AppView");
//    GO(@"BlueToothView");

    //注册 信息顶部弹出框delegate
    [[EENoticeMessage getInstance] regist:self.view];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)replaceView:(NSString *)className{

    [currentShowView removeFromSuperview];
    
    Class classTag = NSClassFromString(className);
    
    id instance = [classTag instance];
    
    [self.view addSubview:instance];
    
    currentShowView = instance;
}






@end
