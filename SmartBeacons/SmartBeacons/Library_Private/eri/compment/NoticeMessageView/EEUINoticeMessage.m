//
//  EEUINoticeMessage.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-4.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEUINoticeMessage.h"
#import <ShareSDK/ShareSDK.h>
#import "ShopInforMation.h"
#import "ShopInformationDS.h"
@implementation EEUINoticeMessage
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(id)initWithImage:(NSString *)imagePath andText:(NSString *)text{
    self = [super init];
    if(self){
        _delegate = [UIApplication sharedApplication].delegate;
//        NSLog(@"%hhd",_delegate.isAdd);
        [self setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
        
        //添加朦板
        _backview = [[UIView alloc]initWithFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
        _backview.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"assets/mainpage/grayBack.png"]];
        [self addSubview:_backview];
        
        //添加图片
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 45, 300, 448.5)];
        _imageView.image = [UIImage imageNamed:imagePath];
        _imageView.userInteractionEnabled = YES;
        [self addSubview:_imageView];
        
        //图片添加点击事件
        UITapGestureRecognizer *tap;
        if([text isEqualToString:@"2"]){
            tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
        }else{
            tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(view2Tapped:)];
        }
        tap.cancelsTouchesInView = NO;
        tap.delegate = self;
        [_imageView addGestureRecognizer:tap];
        
        //图片上添加按钮
        [_imageView addSubview:[Global createBtn:CGRectMake(221, 20.5, 40.5, 34) normal:@"assets/mainpage/collect.png" target:self action:nil]];
        [_imageView addSubview:[Global createBtn:CGRectMake(261, 21.5, 40.5, 34) normal:@"assets/mainpage/share.png" target:self action:@selector(shareContent)]];
        
        //添加取消按钮
        [self addSubview:_btn = [Global createBtn:CGRectMake(280, 20, 20, 20) normal:@"assets/mainpage/cross.png" down:nil target:self action:@selector(eeRemoveTop)]];
    }
    return self;
}

-(void)viewTapped:(UITapGestureRecognizer *)tap{
    ShopInforMation *shop = [ShopInforMation instanceByData:nil];
    [shop addTransition:1];
    [[RootController sharedRootViewController].view addSubview:shop];
    [self eeRemoveTop];
}

-(void)view2Tapped:(UITapGestureRecognizer *)tap{
    ShopInformationDS *shop = [ShopInformationDS instanceByData:nil];
    [shop addTransition:1];
    [[RootController sharedRootViewController].view addSubview:shop];
    [self eeRemoveTop];
}

//解决页面的点击和按钮冲突
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIButton class]]){
        return NO;
    }
    return YES;
}

//-(void)changeWithArray:(NSArray *)array{
//    
//    NSLog(@"11111--%@,22222222--%@",[array objectAtIndex:0],[array objectAtIndex:1]);
//    [self changeWithImage:[array objectAtIndex:0] andText:[array objectAtIndex:1]];
//}

-(void)changeWithImage:(NSString *)imagePath andText:(NSString *)text{
    [self setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
    _backview.hidden = YES;
    _btn.hidden = YES;
    
    _nextImageView = [[UIImageView alloc]initWithFrame:CGRectMake(330, 45, 300, 448.5)];
    _nextImageView.image = [UIImage imageNamed:imagePath];
    [self addSubview:_nextImageView];
    _nextImageView.userInteractionEnabled = YES;
    
    //图片添加点击事件
    UITapGestureRecognizer *tap;
    if([text isEqualToString:@"2"]){
         tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(viewTapped:)];
    }else{
         tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(view2Tapped:)];
    }
    tap.cancelsTouchesInView = NO;
    tap.delegate = self;
    [_nextImageView addGestureRecognizer:tap];
    
    //图片上添加按钮
    [_nextImageView addSubview:[Global createBtn:CGRectMake(221, 20.5, 40.5, 34) normal:@"assets/mainpage/collect.png" target:self action:nil]];
    [_nextImageView addSubview:[Global createBtn:CGRectMake(261, 21.5, 40.5, 34) normal:@"assets/mainpage/share.png" target:self action:@selector(shareContent)]];
    
    [self addSubview:_backview2 = [Global createImage:@"assets/mainpage/grayBack.png" frame:CGRectMake(0, 0, View_Size.width, View_Size.height)]];
    [self addSubview:_btn2 = [Global createBtn:CGRectMake(280, 20, 20, 20) normal:@"assets/mainpage/cross.png" down:nil target:self action:@selector(eeRemoveTop)]];
    [self addSubview:_closeView = [Global createImage:@"assets/mainpage/change.png" frame:CGRectMake(60, 230, 201, 91)]];
    
    //动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    [UIView setAnimationDelay:2];
    _imageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
    _nextImageView.transform = CGAffineTransformMakeScale(0.8, 0.8);
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    [UIView setAnimationDelay:2.35];
    CGPoint tmpCenter1 = _imageView.center;
    CGPoint tmpCenter2 = _nextImageView.center;
    tmpCenter1.x -= View_Size.width;
    tmpCenter2.x -= View_Size.width;
    _imageView.center = tmpCenter1;
    _nextImageView.center = tmpCenter2;
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.35];
    [UIView setAnimationDelay:2.7];
    _nextImageView.transform = CGAffineTransformMakeScale(1, 1);
    [UIView commitAnimations];
    
    [self performSelector:@selector(dismiss) withObject:nil afterDelay:3.1];
    
    _imageView = _nextImageView;
}

-(void)dismiss{
    _closeView.hidden = YES;
    _backview.hidden = NO;
    _btn.hidden = NO;
    _backview2.hidden = YES;
    _btn2.hidden = YES;

}


-(void)eeRemoveTop{
//    [NSObject cancelPreviousPerformRequestsWithTarget:self];
//    NSLog(@"%hhd",self.appview.isAdd);
    self.alpha = 0;
    isRemoveAfterAction = YES;
    moveOver = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
    moveOver.fromValue = [NSNumber numberWithFloat:0];
    moveOver.toValue = [NSNumber numberWithFloat:-100];
    moveOver.duration = 0.3f;
    moveOver.delegate = self;
    moveOver.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    CABasicAnimation *alphaAnimate = [CABasicAnimation animationWithKeyPath:@"opacity"];
    alphaAnimate.fromValue = [NSNumber numberWithFloat:1];
    alphaAnimate.toValue = [NSNumber numberWithFloat:0];
    alphaAnimate.duration = 0.3f;
    alphaAnimate.delegate = self;
    alphaAnimate.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];

    
    [self.layer addAnimation:moveOver forKey:@"moveOver"];
    [self.layer addAnimation:alphaAnimate forKey:@"alpha"];
    
    _delegate.isAdd = NO;
//    NSLog(@"%hhd",_isAdd);
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    [self removeFromSuperview];
}


- (void)shareContent
{
    NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK"  ofType:@"jpg"];
    NSArray *shareList = [ShareSDK getShareListWithType:
                //          ShareTypeQQ,
                          ShareTypeWeixiSession,
                          ShareTypeWeixiTimeline,
                          ShareTypeSinaWeibo,
                //         ShareTypeQQSpace,
                          nil];
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:@"浦东国际汽车展,Welcome~~~~"
                                       defaultContent:@"分享内容"
                                                image:[ShareSDK imageWithPath:imagePath]
                                                title:@"ShareSDK"
                                                  url:@"http://www.sharesdk.cn"
                                          description:@"这是一条测试信息"
                                            mediaType:SSPublishContentMediaTypeNews];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:shareList
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                   
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    
                                }
                            }];
}


- (void)dealloc
{
//    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}
//[[EEUINoticeMessage alloc] initWithContent:content image:imagePath];

@end
