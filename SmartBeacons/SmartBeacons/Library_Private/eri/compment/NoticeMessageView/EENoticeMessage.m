//
//  EENoticeMessage.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-4.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EENoticeMessage.h"
#import "AppDelegate.h"
@implementation EENoticeMessage{
    BOOL _isChanging;
}

static EENoticeMessage *instance;

+(EENoticeMessage *)getInstance{
    if (instance==nil) {
        instance = [[EENoticeMessage alloc] init];
    }
    return instance;
}

-(void)regist:(id)pDelegate{
    self.parentDelegate = pDelegate;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        popDirection = NoticeMessageDirection_Top;//暂时没用上
    }
    return self;
}

//文字
-(void)noticeText:(NSString *)content{
    [self noticeText:content userInfor:nil];
}

-(void)noticeText:(NSString *)content userInfor:(NSDictionary *)inforDictioary{
    noticeMessage = [self createMessage:content image:nil text:nil];
    noticeMessage.configData = inforDictioary;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickMessage:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [noticeMessage addGestureRecognizer:tapGesture];

    [self.parentDelegate performSelector:@selector(add:) withObject:noticeMessage];
}

//图片
-(void)noticeImage:(NSString *)imagePath text:(NSString *)content{
    noticeMessage = [self createMessage:content image:imagePath text:content];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickMessage:)];
    tapGesture.numberOfTapsRequired = 1;
    tapGesture.numberOfTouchesRequired = 1;
    [noticeMessage addGestureRecognizer:tapGesture];
    
    [self.parentDelegate performSelector:@selector(add:) withObject:noticeMessage];
}

//选择了一条弹出信息
-(void)clickMessage:(UITapGestureRecognizer *)notice{
    track();
    [[NSNotificationCenter defaultCenter] postNotificationName:@"EENoticeMessage" object:@"selectedOneMessage" userInfo:noticeMessage.configData];
}

//创建
-(EEUINoticeMessage *)createMessage:(NSString *)content image:(NSString *)imagePath text:(NSString *)text{
    AppDelegate *delegate = [UIApplication sharedApplication].delegate;

    if(delegate.isAdd == NO){
        delegate.isAdd = YES;
        noticeMessage = [[EEUINoticeMessage alloc] initWithImage:imagePath andText:text];
    }else{
//        if(_isChanging){
//            NSArray *array = [NSArray arrayWithObjects:imagePath,text, nil];
//            [noticeMessage performSelector:@selector(changeWithArray:) withObject:array afterDelay:3.5];
//        }else{
            [noticeMessage changeWithImage:imagePath andText:text];
//        }
    }
    return noticeMessage;
}

@end
