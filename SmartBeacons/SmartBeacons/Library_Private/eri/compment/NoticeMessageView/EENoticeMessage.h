//
//  EENoticeMessage.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-4.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EEUINoticeMessage.h"

typedef enum _NoticeMessageDirection{
    NoticeMessageDirection_Top,
    NoticeMessageDirection_Down
}NoticeMessageDirection;

@interface EENoticeMessage : NSObject{
    NoticeMessageDirection popDirection;
    
    EEUINoticeMessage *noticeMessage;
}

@property(nonatomic,strong)id parentDelegate;
@property(nonatomic,strong)id eventDelegate;

@property(nonatomic) BOOL isAdd;

-(void)regist:(id)pDelegate;

+(EENoticeMessage *)getInstance;

-(void)noticeText:(NSString *)content;
-(void)noticeText:(NSString *)content userInfor:(NSDictionary *)inforDictioary;

-(void)noticeImage:(NSString *)imagePath text:(NSString *)content;


@end
