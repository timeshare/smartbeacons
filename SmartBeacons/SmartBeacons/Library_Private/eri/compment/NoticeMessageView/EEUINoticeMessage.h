//
//  EEUINoticeMessage.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-9-4.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface EEUINoticeMessage : EEView<UIGestureRecognizerDelegate>{
    CABasicAnimation *moveOver;
    BOOL _isChangeing;
}

@property(nonatomic) BOOL isAdd;

@property (nonatomic ,strong)AppDelegate *delegate;
@property(nonatomic,strong)NSDictionary *configData;
@property (nonatomic, strong)UIImageView *imageView;
@property (nonatomic, strong)UIImageView *nextImageView;
@property (nonatomic, strong)UIImageView *closeView;


@property (nonatomic, strong)UIView *backview;
@property (nonatomic, strong)UIView *backview2;
@property (nonatomic, strong)UIButton *btn;
@property (nonatomic, strong)UIButton *btn2;


//-(id)initWithContent:(NSString *)content image:(NSString *)imagePath;
-(id)initWithImage:(NSString *)imagePath andText:(NSString *)text;

//-(void)changeWithArray:(NSArray *)array;
-(void)changeWithImage:(NSString *)imagePath andText:(NSString *)text;

@end
