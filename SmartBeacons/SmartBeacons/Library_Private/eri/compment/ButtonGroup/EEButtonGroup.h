//
//  EEButtonGroup.h
//  Sonialvision
//
//  Created by 洪湃 on 14-8-6.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EEButtonGroupDelegate <NSObject>

-(void)EEButtonGroup_SelectIndex:(int)index group:(id)btnGroup;

@end



@interface EEButtonGroup : UIView{
    UITapGestureRecognizer *tapGesture;
    
}

@property(nonatomic,assign)id pdelegate;
@property(nonatomic,retain)NSArray *sourceArray;
@property(nonatomic)BOOL isTransClick;

@property(nonatomic,assign)id selectedItem;

- (id)initWithSourceArray:(NSArray *)listArray positionArray:(NSArray *)posArray normalColor:(UIColor *)nColor selectColor:(UIColor *)sColor fontSize:(int)size isTransparentArea:(BOOL)isTrans;

-(void)setSelectIndex:(int)index;

@end


@interface ImageButton : UIImageView

@property(nonatomic,copy)NSString *normalImageName;
@property(nonatomic,copy)NSString *selectImageName;
@property(nonatomic,assign)UILabel *titleLabel;

@property(nonatomic,retain)UIColor *normalColor;
@property(nonatomic,retain)UIColor *selectColor;

@property(nonatomic)BOOL isSelected;


-(void)setButtonSelect:(BOOL)select;








@end