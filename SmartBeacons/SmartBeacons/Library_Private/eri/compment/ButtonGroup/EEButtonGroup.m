//
//  EEButtonGroup.m
//  Sonialvision
//
//  Created by 洪湃 on 14-8-6.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "EEButtonGroup.h"
#import "UIImage+Extras.h"

@implementation EEButtonGroup
/**
 *  创建一个智能的button group
 *
 *  @param listArray 图片路径
 *  @param posArray  每个item的x坐标，如：@[@"100",@"200",@"300",@"400"] 
 *                   nil的情况下会自动计算frame，
 *  @param isTrans   点击是否忽略透明像素
 *
 *  @return EEButtonGroup对象
 */
- (id)initWithSourceArray:(NSArray *)listArray positionArray:(NSArray *)posArray normalColor:(UIColor *)nColor selectColor:(UIColor *)sColor fontSize:(int)size isTransparentArea:(BOOL)isTrans
{
    self = [super init];
    if (self) {
        
        self.sourceArray = listArray;
        self.isTransClick = isTrans;
        
        int itemwidth=0;
        int itemheight=0;
        for (int i=0; i<[listArray count]; i++) {
            NSDictionary *oneItem = [listArray objectAtIndex:i];
            NSString *title = [oneItem objectForKey:@"title"];

            
            UIImage *imgNormal = [UIImage imageNamed:[oneItem objectForKey:@"normal"]];
            UIImage *imgSelect = [UIImage imageNamed:[oneItem objectForKey:@"select"]];
            ImageButton *imgView = [[ImageButton alloc] initWithImage:imgNormal];
            imgView.normalImageName = [oneItem objectForKey:@"normal"];
            imgView.selectImageName = [oneItem objectForKey:@"select"];
            imgView.normalColor = nColor;
            imgView.selectColor = sColor;
            imgView.tag = 100+i;
            
            [self addSubview:imgView];
            
            int px;
            if (posArray!=nil) {
                px = [[posArray objectAtIndex:i] intValue];
            }else{
                px = imgNormal.size.width*i;
            }
            
            itemwidth = imgNormal.size.width;
            itemheight = imgNormal.size.height;
            
            [imgView ee_viewToPosition:CGPointMake(px, 0)];
            
            //加入label
            if (title!=nil) {
                
                [imgView addSubview:tmpObject = [Global createLabel:CGRectMake(0, 0, itemwidth, itemheight) label:title lines:0 fontSize:size fontName:nil textcolor:nColor align:NSTextAlignmentCenter]];
                imgView.titleLabel = tmpObject;
            }
            
            //手势
            tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
            [self addGestureRecognizer:tapGesture];
        }
        if (itemheight==0 || itemwidth==0) {
            traceException(@"宽和高为有为0的 itemwidth:%d,itemheight:%d",itemwidth,itemheight);
        }
        //设置frame
        if (posArray==nil) {
            [self setFrame:CGRectMake(0, 0, itemwidth*[listArray count], itemheight)];
        }else{
            [self setFrame:CGRectMake(0, 0, itemwidth+[[posArray objectAtIndex:[posArray count]-1] integerValue], itemheight)];
        }
        
        
    }
    return self;
}

-(void)tap:(UIGestureRecognizer *)gesture{
    if (gesture.state == UIGestureRecognizerStateEnded) {
        for (ImageButton *item in [self subviews]) {
            if ([item isKindOfClass:[UIImageView class]]) {
                CGPoint relativePoint = [gesture locationInView:item];
                if ([self checkClickValid:item position:relativePoint] ) {
                    
                    if (self.selectedItem == item) {
                        trace(@"点击了相同了item 不执行操作");
                        return;
                    }
                    if (self.pdelegate!=nil && [self.pdelegate respondsToSelector:@selector(EEButtonGroup_SelectIndex: group:)]) {
                        [self.pdelegate EEButtonGroup_SelectIndex:(item.tag-100) group:self];
                    }
                    
                    [self refreshAllImagesStateForSelect:item];
                    break;
                }
            }
        }
    }else{
        trace(@"没有点击在有效区域");
    }
}

-(void)refreshAllImagesStateForSelect:(ImageButton *)defaultItem{
    for (ImageButton *item in [self subviews]) {
        if ([item isKindOfClass:[UIImageView class]]) {
            if (item==defaultItem) {
                [item setButtonSelect:YES];
                self.selectedItem = defaultItem;
            }else{
                [item setButtonSelect:NO];
            }
        }
    }
}

-(BOOL)checkClickValid:(ImageButton *)imgButton position:(CGPoint)pos{
    if (self.isTransClick) {
        UIColor *color = [imgButton.image ee_getOnePointPixelColor:pos];
        if (color==nil) {
            return NO;
        }
        const float *f = CGColorGetComponents(color.CGColor);
        
        float alpha = f[3];
        if (alpha==0) {
            return NO;
        }else{
            return YES;
        }
        
    }else{
        return CGRectContainsPoint(imgButton.bounds, pos);
    }
}

//主动选中哪个
-(void)setSelectIndex:(int)index{
    if (self.pdelegate!=nil && [self.pdelegate respondsToSelector:@selector(EEButtonGroup_SelectIndex: group:)]) {
        [self.pdelegate EEButtonGroup_SelectIndex:index group:self];
    }
    
    [self refreshAllImagesStateForSelect:(ImageButton *)[self viewWithTag:(index+100)]];
}



- (void)dealloc
{
    self.pdelegate = nil;
    self.sourceArray = nil;
}
@end









@implementation ImageButton

-(void)setButtonSelect:(BOOL)select{
    if (self.isSelected==select) {
        return;
    }
    
    if (select) {
        UIImage *imgSelect = [UIImage imageNamed:self.selectImageName];
        self.image = imgSelect;
        self.titleLabel.textColor = self.selectColor;
    }else{
        UIImage *imgNormal = [UIImage imageNamed:self.normalImageName];
        self.image = imgNormal;
        self.titleLabel.textColor = self.normalColor;
    }
    self.isSelected = select;
}

-(void)dealloc{
    self.normalImageName = nil;
    self.selectImageName = nil;
    self.titleLabel = nil;
    self.normalColor = nil;
    self.selectColor = nil;
    
}

@end
