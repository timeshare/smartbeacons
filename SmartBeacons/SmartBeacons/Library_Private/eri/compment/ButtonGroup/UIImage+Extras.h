//
//  UIImage+Extras.h
//  DaRenXiu
//
//  Created by pai hong on 12-4-25.
//  Copyright (c) 2012年 
//-------洪湃--------------
//---qq:  454077256-------
//---tel: 186 2159 2830---
//------------------------. . All rights reserved.
//

@interface UIImage (Extras)


- (id)initWithContentsOfResolutionIndependentFile:(NSString *)path;
+ (UIImage*)imageWithContentsOfResolutionIndependentFile:(NSString *)path;

- (UIImage*)imageScale_to_Width:(CGFloat)width height:(CGFloat)height ;
//固定比例缩放，
+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) viewsize;

//得到图片上的一个像素值
-(UIColor *)ee_getOnePointPixelColor:(CGPoint)point;

//遮罩图 传入的是黑白遮罩图片，黑色代表显示区域
-(UIImage *)ee_maskImage:(UIImage *)maskImage;

//删除透明像素
-(UIImage *)ee_clipTransparentPiexlAndRecutCGRect:(CGRect *)clipRect;


- (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 ;
- (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect ;
/*
* @brief rotate image 90 withClockWise
*/
- (UIImage*)rotate90Clockwise;

/*
 * @brief rotate image 90 counterClockwise
 */
- (UIImage*)rotate90CounterClockwise;

/*
 * @brief rotate image 180 degree
 */
- (UIImage*)rotate180;

/*
 * @brief rotate image to default orientation
 */
- (UIImage*)rotateImageToOrientationUp;

/*
 * @brief flip horizontal
 */
- (UIImage*)flipHorizontal;

/*
 * @brief flip vertical
 */
- (UIImage*)flipVertical;

/*
 * @brief flip horizontal and vertical
 */
- (UIImage*)flipAll;

@end

