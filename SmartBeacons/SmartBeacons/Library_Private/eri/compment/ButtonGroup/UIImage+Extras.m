//
//  UIImage+Extras.m
//  DaRenXiu
//
//  Created by pai hong on 12-4-25.
//  Copyright (c) 2012年 
//-------洪湃--------------
//---qq:  454077256-------
//---tel: 186 2159 2830---
//------------------------. . All rights reserved.
//

#import "UIImage+Extras.h"

@implementation UIImage (Extras)

// UIImage+Extras.m
-(id)initWithContentsOfResolutionIndependentFile:(NSString *)path {
    if ( [[[UIDevice currentDevice] systemVersion] intValue] >=4 && [[UIScreen mainScreen] scale] == 2.0 ) {
        NSString *path2x = [[path stringByDeletingLastPathComponent] 
                            stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@", 
                                                            [[path lastPathComponent] stringByDeletingPathExtension], 
                                                            [path pathExtension]]];
        
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path2x] ) {
            return [self initWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path2x]] CGImage] scale:2.0 orientation:UIImageOrientationUp];
        }
        
    }
    
    return [self initWithData:[NSData dataWithContentsOfFile:path]];
}


+ (UIImage*)imageWithContentsOfResolutionIndependentFile:(NSString *)path {
    return [[UIImage alloc] initWithContentsOfResolutionIndependentFile:path];
}


#pragma mark -
#pragma mark ----UIImage 缩放 （缩略图）----

//如果比例没有调节好，可能会变形
- (UIImage*)imageScale_to_Width:(CGFloat)width height:(CGFloat)height {
    
    CGFloat destW = width;
    CGFloat destH = height;
    CGFloat sourceW = width;
    CGFloat sourceH = height;
    
    CGImageRef imageRef = self.CGImage;
    CGContextRef bitmap = CGBitmapContextCreate(NULL, 
                                                destW, 
                                                destH,
                                                CGImageGetBitsPerComponent(imageRef), 
                                                4*destW, 
                                                CGImageGetColorSpace(imageRef),
                                                (kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst));
    
    CGContextDrawImage(bitmap, CGRectMake(0, 0, sourceW, sourceH), imageRef);
    
    CGImageRef ref = CGBitmapContextCreateImage(bitmap);
    UIImage *result = [UIImage imageWithCGImage:ref];
    CGContextRelease(bitmap);
    CGImageRelease(ref);
    
    return result;
}

// 自动计算正确的宽高，来适应 Proportionately resize, completely fit in view, no cropping
+ (UIImage *) image: (UIImage *) image fitInSize: (CGSize) viewsize
{
	// calculate the fitted size
	CGSize size = [UIImage fitSize:image.size inSize:viewsize];
	
	UIGraphicsBeginImageContext(size);
    
	CGRect rect = CGRectMake(0, 0, size.width, size.height);
	[image drawInRect:rect];
	
	UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();  
	
	return newimg;  
}

+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
	CGFloat scale;
	CGSize newsize;
	
	if(thisSize.width<aSize.width && thisSize.height < aSize.height)
	{
		newsize = thisSize;
	}
	else 
	{
		if(thisSize.width >= thisSize.height)
		{
			scale = aSize.width/thisSize.width;
			newsize.width = aSize.width;
			newsize.height = thisSize.height*scale;
		}
		else 
		{
			scale = aSize.height/thisSize.height;
			newsize.height = aSize.height;
			newsize.width = thisSize.width*scale;
		}
	}
	return newsize;
}


#pragma mark --- 得到图片某点的像素颜色值 ---
-(UIColor *)ee_getOnePointPixelColor:(CGPoint)point{
    if (!CGRectContainsPoint(CGRectMake(0.0f, 0.0f, self.size.width, self.size.height), point)) {
        return nil;
    }
    
    NSInteger pointX = trunc(point.x);
    NSInteger pointY = trunc(point.y);
    CGImageRef cgImage = self.CGImage;
    NSUInteger width = self.size.width;
    NSUInteger height = self.size.height;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();

    unsigned char pixelData[4] = { 0, 0, 0, 0 };
    CGContextRef context = CGBitmapContextCreate(pixelData,
                                                 1,
                                                 1,
                                                 8,
                                                 4,
                                                 colorSpace,
                                                 kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    CGColorSpaceRelease(colorSpace);
    CGContextSetBlendMode(context, kCGBlendModeCopy);
    
    CGContextTranslateCTM(context, -pointX, pointY-(CGFloat)height);
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, (CGFloat)width, (CGFloat)height), cgImage);
    CGContextRelease(context);
    
    CGFloat red   = (CGFloat)pixelData[0] / 255.0f;
    CGFloat green = (CGFloat)pixelData[1] / 255.0f;
    CGFloat blue  = (CGFloat)pixelData[2] / 255.0f;
    CGFloat alpha = (CGFloat)pixelData[3] / 255.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

#pragma mark --- 遮罩 ---
-(UIImage *)ee_maskImage:(UIImage *)maskImage{
    
    CGImageRef imageSource = [self CGImage];
    CGImageRef imageMask   = [maskImage CGImage];
    
    CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(imageMask),
                                        CGImageGetHeight(imageMask),
                                        CGImageGetBitsPerComponent(imageMask),
                                        CGImageGetBitsPerPixel(imageMask),
                                        CGImageGetBytesPerRow(imageMask),
                                        CGImageGetDataProvider(imageMask), NULL, true);
    CGImageRef imageWithAlpha = imageSource;
    if (CGImageGetAlphaInfo(imageSource) == kCGImageAlphaNone) {
        imageWithAlpha = [self fomartToAlphaChannel :imageSource];
    }
    
    CGImageRef resultMaskedImage = CGImageCreateWithMask(imageWithAlpha, mask);
    
    if (imageSource != imageWithAlpha) {
        CGImageRelease(imageWithAlpha);
    }
    
    CGImageRelease(mask);
    
    UIImage *retMImage = [UIImage imageWithCGImage:resultMaskedImage];
    CGImageRelease(resultMaskedImage);
    
    return retMImage;
}
//巨大的移动开发人员储备资源，让你轻轻松松就能找到项目终结者
//提倡本地开发，寻找你周边的开发者，更有效沟通
//

//移动外包网，非传统威客模式，此平台完全向开发者和发任务者免费，对于开发者，绝对不收20%的中间佣金。
//传统威客模式，会抽取开发者项目资金的20%，甚至更高。
//高等级会员每天最多只能与4个项目进行交流。低等级会员无此限制。这样的目的是防止大企业或者大团队垄断市场的情况。
//针对与等级低的开发者提供专有保护，限制高级开发者团队，杜绝高级开发者团队垄断市场的情况。
//众多轻项目、微项目，即使你是在上班，用闲暇时间也可以搞定


#pragma mark ---- 合并Image
- (UIImage *)addImage:(UIImage *)image1 toImage:(UIImage *)image2 {
    UIGraphicsBeginImageContext(image1.size);
    
    // Draw image1
    [image1 drawInRect:CGRectMake(0, 0, image1.size.width, image1.size.height)];
    
    // Draw image2
    [image2 drawInRect:CGRectMake(0, 0, image2.size.width, image2.size.height)];
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return resultingImage;
}


#pragma mark - 裁剪图片
- (UIImage *)imageFromImage:(UIImage *)image inRect:(CGRect)rect {
    CGImageRef sourceImageRef = [image CGImage];
    CGImageRef newImageRef = CGImageCreateWithImageInRect(sourceImageRef, rect);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    CGImageRelease(newImageRef);
    return newImage;
}


-(CGImageRef) fomartToAlphaChannel :(CGImageRef) sourceImage
{
    CGImageRef retVal = NULL;
    size_t width = CGImageGetWidth(sourceImage);
    size_t height = CGImageGetHeight(sourceImage);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef context = CGBitmapContextCreate(NULL, width, height,8, 0, colorSpace, kCGBitmapAlphaInfoMask);
    
    if (context != NULL) {
        CGContextDrawImage(context, CGRectMake(0, 0, width, height), sourceImage);
        
        retVal = CGBitmapContextCreateImage(context);
        
        CGContextRelease(context);
    }
    
    CGColorSpaceRelease(colorSpace);
    return retVal;
}
#pragma mark --- 删除图片4周的空白透明像素  返回新的UIImage  和 对应clip区域的Rect数据---
-(UIImage *)ee_clipTransparentPiexlAndRecutCGRect:(CGRect *)clipRect{
    int sw = self.size.width;
    int sh = self.size.height;
    long sall = sw*sh;
    
    long ss = sw*sh*4;
    unsigned char *sourceImgData = calloc(ss, sizeof(char));
    
    NSAssert(sourceImgData!=NULL, @"分配内存区域出错");
    
    CGColorSpaceRef colorspage = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(sourceImgData, sw, sh, 8, sw*4, colorspage, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGContextDrawImage(context, CGRectMake(0, 0, sw,sh), self.CGImage);
    
    if (colorspage) {
        CGColorSpaceRelease(colorspage);
    }
    
    float leftx = 0;
    float lefty = 0;
    float rightx = 0;
    float righty = 0;
    
    
    for (int i=1; i<=sall; i++) {
        float f = sourceImgData[i*4-1];
        if (f!=0.0) {
            lefty = trunc((i*1.0/sw));
            NSLog(@"上 %d   lefty:%f",i,lefty);
            break;
        }
    }
    
    for (int i=sall-1; i>=0; i-=4) {
        float f = (float)sourceImgData[i*4-1];
        if (f!=0.0) {
            righty = trunc(((i*1.0))/sw)+1;
            NSLog(@"下 %d   righty:%f",i,righty);
            break;
        }
    }
    
    int flag = 0;
    for (int i=1;i<=sw;i++) {
        for (int n=0; n<sh; n++) {
            float f = (float)sourceImgData[n*sw*4+i*4-1];
            if (f!=0.0) {
                leftx = i-1;
                leftx<0?leftx=0:leftx;
                flag = 1;
                NSLog(@"左 %d   leftx:%f",n*sw*4+i*4-1,leftx);
                break;
            }
        }
        if (flag==1) {
            break;
        }
    }
    
    flag = 0;
    for (int i=sw; i>0; i--) {
        for (int n=sh-1; n>=0; n--) {
            float f = (float)sourceImgData[n*sw*4+i*4-1];
            if (f!=0.0) {
                rightx = i;
                rightx>sw?rightx=sw:rightx;
                flag = 1;
                NSLog(@"右 %d   rightx:%f",n*sw*4+i*4-1,rightx);
                break;
            }
        }
        if (flag==1) {
            break;
        }
    }
    CGRect avalidRect = CGRectMake(leftx, lefty, rightx-leftx, righty-lefty);
    NSLog(@"%@",[NSValue valueWithCGRect:avalidRect]);
    CGContextRelease(context);
    
    CGImageRef rr = CGImageCreateWithImageInRect(self.CGImage, avalidRect);
    
    free(sourceImgData);
    sourceImgData = NULL;
    
    if (clipRect!=nil) {
        clipRect->origin.x = avalidRect.origin.x;
        clipRect->origin.y = avalidRect.origin.y;
        clipRect->size.width = avalidRect.size.width;
        clipRect->size.height = avalidRect.size.height;
    }
    
    return [UIImage imageWithCGImage:rr];
}
/*
 * @brief rotate image 90 with CounterClockWise
 */
- (UIImage*)rotate90CounterClockwise
{
    UIImage *image = nil;
    switch (self.imageOrientation) {
        case UIImageOrientationUp:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeft];
            break;
        }
        case UIImageOrientationDown:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRight];
            break;
        }
        case UIImageOrientationLeft:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDown];
            break;
        }
        case UIImageOrientationRight:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUp];
            break;
        }
        case UIImageOrientationUpMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRightMirrored];
            break;
        }
        case UIImageOrientationDownMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeftMirrored];
            break;
        }
        case UIImageOrientationLeftMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUpMirrored];
            break;
        }
        case UIImageOrientationRightMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDownMirrored];
            break;
        }
        default:
            break;
    }
    
    return image;
}

/*
 * @brief rotate image 90 with Clockwise
 */
- (UIImage*)rotate90Clockwise
{
    UIImage *image = nil;
    switch (self.imageOrientation) {
        case UIImageOrientationUp:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRight];
            break;
        }
        case UIImageOrientationDown:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeft];
            break;
        }
        case UIImageOrientationLeft:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUp];
            break;
        }
        case UIImageOrientationRight:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDown];
            break;
        }
        case UIImageOrientationUpMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeftMirrored];
            break;
        }
        case UIImageOrientationDownMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRightMirrored];
            break;
        }
        case UIImageOrientationLeftMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDownMirrored];
            break;
        }
        case UIImageOrientationRightMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUpMirrored];
            break;
        }
        default:
            break;
    }
    
    return image;
}

/*
 * @brief rotate image 180 degree
 */
- (UIImage*)rotate180
{
    UIImage *image = nil;
    switch (self.imageOrientation) {
        case UIImageOrientationUp:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDown];
            break;
        }
        case UIImageOrientationDown:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUp];
            break;
        }
        case UIImageOrientationLeft:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRight];
            break;
        }
        case UIImageOrientationRight:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeft];
            break;
        }
        case UIImageOrientationUpMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDownMirrored];
            break;
        }
        case UIImageOrientationDownMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUpMirrored];
            break;
        }
        case UIImageOrientationLeftMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRightMirrored];
            break;
        }
        case UIImageOrientationRightMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeftMirrored];
            break;
        }
        default:
            break;
    }
    
    return image;
}

/*
 * @brief rotate image to default orientation
 */
- (UIImage*)rotateImageToOrientationUp
{
    CGSize size = CGSizeMake(self.size.width * self.scale, self.size.height * self.scale);
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextClearRect(context, CGRectMake(0, 0, size.width, size.height));
    
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/*
 * @brief flip horizontal
 */
- (UIImage*)flipHorizontal
{
    UIImage *image = nil;
    switch (self.imageOrientation) {
        case UIImageOrientationUp:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUpMirrored];
            break;
        }
        case UIImageOrientationDown:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDownMirrored];
            break;
        }
        case UIImageOrientationLeft:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRightMirrored];
            break;
        }
        case UIImageOrientationRight:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeftMirrored];
            break;
        }
        case UIImageOrientationUpMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUp];
            break;
        }
        case UIImageOrientationDownMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDown];
            break;
        }
        case UIImageOrientationLeftMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRight];
            break;
        }
        case UIImageOrientationRightMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeft];
            break;
        }
        default:
            break;
    }
    
    return image;
}

/*
 * @brief flip vertical
 */
- (UIImage*)flipVertical
{
    UIImage *image = nil;
    switch (self.imageOrientation) {
        case UIImageOrientationUp:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDownMirrored];
            break;
        }
        case UIImageOrientationDown:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUpMirrored];
            break;
        }
        case UIImageOrientationLeft:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeftMirrored];
            break;
        }
        case UIImageOrientationRight:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRightMirrored];
            break;
        }
        case UIImageOrientationUpMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationDown];
            break;
        }
        case UIImageOrientationDownMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationUp];
            break;
        }
        case UIImageOrientationLeftMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationLeft];
            break;
        }
        case UIImageOrientationRightMirrored:
        {
            image = [UIImage imageWithCGImage:self.CGImage scale:1 orientation:UIImageOrientationRight];
            break;
        }
        default:
            break;
    }
    
    return image;
}

/*
 * @brief flip horizontal and vertical
 */
- (UIImage*)flipAll
{
    return [self rotate180];
}

@end
