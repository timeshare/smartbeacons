//
//  Tools_math.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-25.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tools_math : NSObject

+(BOOL)ee_isPointInRect:(CGPoint)point pints:(NSArray *)rectPoints;
+(BOOL)ee_isPointInRect2:(CGPoint)point pints:(NSArray *)rectPoints;


+(CGPoint)getShapeCenter:(CGPoint)p1 :(CGPoint)p2 :(CGPoint)p3 :(CGPoint)p4;

@end
