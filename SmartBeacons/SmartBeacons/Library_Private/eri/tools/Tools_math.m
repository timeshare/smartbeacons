//
//  Tools_math.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-25.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "Tools_math.h"

@implementation Tools_math


/**
 *  判断一点是否在凸矩形内
 *  如果在矩形外，那么矩形的相邻点与目标点形成的三角形中一定有钝角三角形。 而且一定有两个！那么关键的算法只要两行就解决了
 *
 *  @param point      点坐标
 *  @param rectPoints 四边形4个点坐标
 *
 *  @return yes or no
 */
+(BOOL)ee_isPointInRect2:(CGPoint)point pints:(NSArray *)rectPoints{
    
    double x1=[[[rectPoints objectAtIndex:0] objectAtIndex:0] floatValue];
    double y1=[[[rectPoints objectAtIndex:0] objectAtIndex:1] floatValue];
    
    double x2=[[[rectPoints objectAtIndex:1] objectAtIndex:0] floatValue];
    double y2=[[[rectPoints objectAtIndex:1] objectAtIndex:1] floatValue];
    
    double x3=[[[rectPoints objectAtIndex:2] objectAtIndex:0] floatValue];
    double y3=[[[rectPoints objectAtIndex:2] objectAtIndex:1] floatValue];
    
    double x4=[[[rectPoints objectAtIndex:3] objectAtIndex:0] floatValue];
    double y4=[[[rectPoints objectAtIndex:3] objectAtIndex:1] floatValue];

    float x[4] = {0};
    float y[4] = {0};
    x[0] = x1;
    x[1] = x2;
    x[2] = x3;
    x[3] = x4;
    
    y[0] = y1;
    y[1] = y2;
    y[2] = y3;
    y[3] = y4;
    
    float fx = point.x;
    float fy = point.y;//目标点的坐标
    int i;//循环用变量

    
    for(i=0;i<=3;i++)
    {
        if( (pow((x[i]-x[(1+i)%4]),2) +pow((y[i]-y[(1+i)%4]),2) +pow((x[i]-fx),2) +pow((y[i]-fy),2) ) < (pow((fy-y[(1+i)%4]),2) +pow((fx-x[(1+i)%4]),2) )) //根据勾股定理判断是否为钝角三角形 。因为有两个，所以不用判断长短，只要找出一个就可以解决问题，而且矩形边一定是短边 ，pow是乘方函数。
        { 
            break; 
        } 
    } 
    if(i==4) 
    {
        return YES;
        //cout<<"在矩形内"<<endl;
    } 
    else 
    {
        return NO;
        //cout<<"在矩形外"<<endl;
    }
}

/**
 *  判断一点是否在凸矩形内
 *
 *  @param point      点坐标
 *  @param rectPoints 四边形4个点坐标
 *
 *  @return yes or no
 */
+(BOOL)ee_isPointInRect:(CGPoint)point pints:(NSArray *)rectPoints{
//    double x1=142,y1=162;
//    double x2=248,y2=96;
//    double x3=298,y3=151;
//    double x4=221,y4=187;,y1=162;
    double x1=[[[rectPoints objectAtIndex:0] objectAtIndex:0] floatValue];
    double y1=[[[rectPoints objectAtIndex:0] objectAtIndex:1] floatValue];
    
    double x2=[[[rectPoints objectAtIndex:1] objectAtIndex:0] floatValue];
    double y2=[[[rectPoints objectAtIndex:1] objectAtIndex:1] floatValue];
    
    double x3=[[[rectPoints objectAtIndex:2] objectAtIndex:0] floatValue];
    double y3=[[[rectPoints objectAtIndex:2] objectAtIndex:1] floatValue];
    
    double x4=[[[rectPoints objectAtIndex:3] objectAtIndex:0] floatValue];
    double y4=[[[rectPoints objectAtIndex:3] objectAtIndex:1] floatValue];
    
    double x=point.x,y=point.y;
    double a[8] = {0};
    a[0] = atan2((y-y1),(x-x1));
    a[1] = atan2((y2-y1),(x2-x1));
    a[2] = atan2((y-y2),(x-x2));
    a[3] = atan2((y3-y2),(x3-x2));
    a[4] = atan2((y-y3),(x-x3));
    a[5] = atan2((y4-y3),(x4-x3));
    a[6] = atan2((y-y4),(x-x4));
    a[7] = atan2((y1-y4),(x1-x4));
    int i = 0;
    for(i = 0; i < 8; i++)
    {
        if(a[i]<0) a[i] = 2*3.1415926 + a[i];
    }
    if(a[0]>a[1]&&a[2]>a[3]&&a[4]>a[5]&&a[6]>a[7]){
        //printf("在内部\n");
        return YES;
    }
    else{
//        printf("在外部\n");
        return NO;
    }
}

//四边形对角线交叉点  这里简单的返回1条对角线的中点
+(CGPoint)getShapeCenter:(CGPoint)p1 :(CGPoint)p2 :(CGPoint)p3 :(CGPoint)p4 {
    return CGPointMake((p1.x+p3.x)*0.5, (p1.y+p3.y)*0.5);
}

@end
