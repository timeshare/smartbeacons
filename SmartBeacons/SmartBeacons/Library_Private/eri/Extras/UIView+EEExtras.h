//
//  UIView+convenience.h
//
//  Created eri.hongpai
//  Copyright (c) 2011 Vurig Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (EEExtras)

@property (nonatomic) CGPoint origin;
@property (nonatomic) CGSize size;

@property (nonatomic) CGFloat x;
@property (nonatomic) CGFloat y;




@property (nonatomic) CGFloat right;
@property (nonatomic) CGFloat bottom;

@property (nonatomic) CGFloat width;
@property (nonatomic) CGFloat height;



-(BOOL) ee_containsSubView:(UIView *)subView;




- (void)ee_wobble:(BOOL)wobble;



-(void)ee_fadeIn:(NSTimeInterval)dur;
-(void)ee_fadeOut:(NSTimeInterval)dur;
-(void)ee_fadeIn:(NSTimeInterval)dur delegate:(id)dgate sel:(SEL)function;
-(void)ee_fadeOut:(NSTimeInterval)dur delegate:(id)dgate sel:(SEL)function;

-(void)ee_tween:(NSTimeInterval)dur to:(NSString *)paramTo;
-(void)ee_tween:(NSTimeInterval)dur to:(NSString *)paramTo delegate:(id)dgate sel:(SEL)function;
-(void)ee_tween:(NSTimeInterval)dur from:(NSString *)paramFrom to:(NSString *)paramTo;
-(void)ee_tween:(NSTimeInterval)dur from:(NSString *)paramFrom to:(NSString *)paramTo delegate:(id)dgate sel:(SEL)function;
//[v ee_tween:1 from:@"{x:100,y:100,alpha:0}" to:@"{x:300,y:300,alpha:1}"];







-(void)ee_viewToUp:(int)space;
-(void)ee_viewToDown:(int)space;
-(void)ee_viewToLeft:(int)space;
-(void)ee_viewToRight:(int)space;
-(void)ee_viewToPosition:(CGPoint)toPoint;

-(void)ee_removeAllChildren;
    
@end
