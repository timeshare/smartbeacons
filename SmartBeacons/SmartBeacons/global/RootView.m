//
//  RootView.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "RootView.h"

@implementation RootView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //[self setBackgroundColor:[UIColor grayColor]];
    }
    return self;
}

-(void)addSubview:(UIView *)view{
    if (SystemVersion>=7.0) {
        [view setFrame:CGRectMake(0, StatusHeight, View_Size.width, View_Size.height)];
        [super addSubview:view];
    }else{
        [view setFrame:CGRectMake(0, 0, View_Size.width, View_Size.height)];
        [super addSubview:view];
    }
}

-(void)add:(UIView *)view1{
    
    if (SystemVersion>=7.0) {
        view1.y +=StatusHeight;
    }else{
    }
    [super addSubview:view1];
    
}


@end
