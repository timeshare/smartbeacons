//
//  Created by Chen Chong on 2/28/13.
//  Copyright (c) 2013 Chen Chong. All rights reserved.
//

@protocol UISViewDelegate


@end


@interface UISView : UIView <UISViewDelegate>{

@public
	NSTimeInterval animationInterval;
@protected
    NSTimer *animationTimer;
}


@property NSTimeInterval animationInterval;
@property (assign) NSTimer *animationTimer;

+(id)instance;

- (void) initIphoView;
- (void) initIpadView;

- (void) freeIphoView;
- (void) freeIpadView;

- (void) animationLooper:(NSTimer *)timer;
- (void) startAnimation;
- (void) stopAnimation;
- (void) setAnimationTimer:(NSTimer *)newTimer;
- (void) setAnimationInterval:(NSTimeInterval)interval;

- (void) layoutSubviews:(UIDeviceOrientation) ori;

@end


@interface GameView : UISView
{

}

@end
