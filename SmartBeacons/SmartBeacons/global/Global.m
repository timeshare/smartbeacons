//
//  Global.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "Global.h"
#import "NSDate+convenience.h"
static Global *_global = nil;

@implementation Global

id tmpObject	= NULL;

EEImageView *tmpImgView	= NULL;
EEButton *tmpButtom	= NULL;
EELabel *tmpLabel	= NULL;
EEView *tmpView	= NULL;

+ (Global*) sharedGlobal
{
	if (!_global) {
		_global = [[Global alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) ;
        NSString *document = [paths objectAtIndex:0];
        NSLog(@"%@",document);
        
        if (iPhone5) {
            [Global sharedGlobal].AppSize = CGSizeMake(320, 568);
            [Global sharedGlobal].ViewSize = CGSizeMake(320, 548);
        }else if([[UIScreen mainScreen] currentMode].size.width>=1536){
            [Global sharedGlobal].AppSize = CGSizeMake(320, 480);
            [Global sharedGlobal].ViewSize = CGSizeMake(320, 460);
        }else{
            [Global sharedGlobal].AppSize = CGSizeMake(320, 480);
            [Global sharedGlobal].ViewSize = CGSizeMake(320, 460);
        }

    }
	return _global;
}





-(void) log:(NSString *)s level:(int)lev
{
    
    if (lev < logLevelTrace) {
        return;
    }
    
    //控制台输出
    trace(@"--log-- %@",s);
    
    NSString *logFilePath = [NSString stringWithFormat:@"%@/Documents/log.log",NSHomeDirectory()];
    
    //    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
    BOOL isExit = [[NSFileManager defaultManager] fileExistsAtPath:logFilePath isDirectory:NO];
    if (!isExit) {
        NSLog(@"%@",@"文件不存在");
        NSString *begin = [NSString stringWithFormat:@"--------->log开始记录:\r"];
        [begin writeToFile:logFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    if ([[[NSFileManager defaultManager] attributesOfItemAtPath:logFilePath error:nil] fileSize] > 5242880) {
        //    if ([[[NSFileManager defaultManager] attributesOfItemAtPath:logFilePath error:nil] fileSize] > 100) {
        NSString *newlogFilePath = [NSString stringWithFormat:@"%@/Documents/log%@.log",NSHomeDirectory(),[NSDate date]];
        
        [[NSFileManager defaultManager] moveItemAtPath:logFilePath
                                                toPath:newlogFilePath error:NULL];
        NSString *begin = [NSString stringWithFormat:@"开始了:\r"];
        [begin writeToFile:logFilePath atomically:YES encoding:NSUTF8StringEncoding error:nil];
    }
    
    NSFileHandle  *outFile;
    NSData *buffer;
    
    outFile = [NSFileHandle fileHandleForWritingAtPath:logFilePath];
    
    if(outFile == nil)
    {
        NSLog(@"Open of file for writing failed");
    }
    
    //找到并定位到outFile的末尾位置(在此后追加文件)
    [outFile seekToEndOfFile];
    
    //读取inFile并且将其内容写到outFile中
    NSString *bs = [NSString stringWithFormat:@"%@ %@\n",[NSDate ee_getDateString],s];
    buffer = [bs dataUsingEncoding:NSUTF8StringEncoding];
    
    [outFile writeData:buffer];
    
    //关闭读写文件
    [outFile closeFile];
    
}

+(EEView *)createViewEmpty{
    EEView *eeview = [[EEView alloc] init];
    return eeview;
}

+(EEView *)createViewByFrame:(CGRect)frame color:(UIColor *)col{
    EEView *eeview = [[EEView alloc] initWithFrame:frame];
    [eeview setBackgroundColor:col];
    return eeview;
}


+(EEImageView *)createImage:(NSString *)filePath frame:(CGRect)frame{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    [imgview setFrame:frame];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath point:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    [imgview setFrame:CGRectMake(point.x, point.y, img.size.width, img.size.height)];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath center:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    imgview.center = point;
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath fitWidth:(int)fwidth point:(CGPoint)point height:(NSNumber *)resultHeight{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    
    float fheight = img.size.height*fwidth/img.size.width;
    resultHeight = [NSNumber numberWithFloat:fheight];
    
    [imgview setFrame:CGRectMake(point.x, point.y, fwidth, fheight)];
    return imgview;
}

+(EEImageView *)createImage:(NSString *)filePath fitSize:(CGSize)fsize center:(CGPoint)point{
    UIImage *img = [Global loadImg:filePath];
    EEImageView *imgview = [[EEImageView alloc] initWithImage:img];
    
    CGSize targetSize = [Global fitSize:img.size inSize:fsize];
    
    [imgview setFrame:CGRectMake(0, 0, targetSize.width, targetSize.height)];
    
    imgview.center = point;
    
    return imgview;
}



+ (EELabel  *) createLabel:(CGRect)frame label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)size fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align
{
	if (num == 1) {
		if (frame.size.height<size*1.4) {
			frame.size.height = size*1.4;
			frame.origin.y -= 0.2*size;
		}
	}
	
    EELabel *lbl = [[EELabel alloc] initWithFrame:frame];
    
    lbl.text = text;
    lbl.numberOfLines = num;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    //CGFloat* colors = ;
    
	lbl.font = [UIFont fontWithName:fontName size:size];
    lbl.font = [UIFont systemFontOfSize:size];
    
    lbl.alpha = CGColorGetAlpha(textcolor.CGColor) ;
	lbl.adjustsFontSizeToFitWidth = TRUE;
	//lbl.minimumFontSize = size*0.5;
    lbl.minimumScaleFactor = 0.5f;
    lbl.backgroundColor = [UIColor clearColor];
    [lbl setTextAlignment:align];
    [lbl setTextColor:textcolor];
    lbl.opaque = FALSE;
    return lbl;
}


+ (EELabel  *) createLabelFit:(CGSize)size point:(CGPoint)point label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)fontSize fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align resultSize:(CGSize *)resultSize
{
    EELabel *lbl = [[EELabel alloc] init];
    
    
    lbl.text = text;
    lbl.numberOfLines = num;
    lbl.clipsToBounds = FALSE;
    lbl.contentMode = UIViewContentModeCenter;
    
	lbl.font = [UIFont fontWithName:fontName size:fontSize];
    lbl.font = [UIFont systemFontOfSize:fontSize];
    
    lbl.alpha = CGColorGetAlpha(textcolor.CGColor) ;
	lbl.adjustsFontSizeToFitWidth = TRUE;
    
    [lbl setTextAlignment:align];
    [lbl setTextColor:textcolor];

    CGSize resultSize1 = [lbl sizeThatFits:size];
    resultSize = &resultSize1;
    
    [lbl setFrame:CGRectMake(point.x, point.y, resultSize1.width, resultSize1.height)];

    return lbl;
}


+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName target:(id)target action:(SEL)act{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgSelect = [Global loadImg:dName];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    [button setImage:imgNormal forState:UIControlStateNormal];
    [button setImage:imgSelect forState:UIControlStateSelected];
    [button setImage:imgSelect forState:UIControlStateHighlighted];
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName target:(id)target action:(SEL)act{
    return [Global createBtn:frame normal:nName down:nil target:target action:act];
}

+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col size:(int)size target:(id)target  action:(SEL)act{
    UIImage *imgNormal = [Global loadImg:nName];
    UIImage *imgDown = [Global loadImg:nDown];
    
    EEButton *button = [[EEButton alloc] initWithFrame:frame];
    [button setBackgroundImage:imgNormal forState:UIControlStateNormal];
    [button setBackgroundImage:imgDown forState:UIControlStateHighlighted];
    [button setBackgroundImage:imgDown forState:UIControlStateSelected];
    
    [button addTarget:target action:act forControlEvents:UIControlEventTouchUpInside];
    
    [button setTitle:_title forState:UIControlStateNormal];
    
    [button setTitleColor:_col forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:size];
    
    return button;
}


    

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    return [Global createFitButton:contentEdge center:_center original:NullPoint imageName:imgName imageEdge:imgEdge title:text size:_size delegate:_delegate action:selector];
}

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    return [Global createFitButton:contentEdge center:NullPoint original:_original imageName:imgName imageEdge:imgEdge title:text size:_size delegate:_delegate action:selector];
}

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector{
    
    
    UIImage *img = [Global loadImg:imgName];
    img = [img resizableImageWithCapInsets:imgEdge];
    
    EEButton *button = [[EEButton alloc] init];
    
    button.titleLabel.font = [UIFont systemFontOfSize:_size];
    
    [button setTitle:text forState:UIControlStateNormal];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    
    [button.titleLabel sizeToFit];
    
    //[button setFrame:CGRectMake(0, 0, 200, 21)];
    
    [button sizeToFit];
    if (contentEdge.top==0 && contentEdge.bottom==0) { //为 0 的话，说明纵向不拉伸，那么就是图片的高度
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, img.size.height)];
    }else if(contentEdge.right==0 && contentEdge.left==0){
        [button setFrame:CGRectMake(0,0, img.size.width, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }else{
        [button setFrame:CGRectMake(0,0, button.titleLabel.width+contentEdge.left+contentEdge.right, button.titleLabel.height+contentEdge.top+contentEdge.bottom)];
    }
    [button addTarget:_delegate action:selector forControlEvents:UIControlEventTouchUpInside];
    
    if (!CGPointEqualToPoint(NullPoint, _original)) {
        [button setFrame:CGRectMake(_original.x,_original.y, button.width, button.height)];
    }
    
    if (!CGPointEqualToPoint(NullPoint, _center)) {
        button.center = _center;
    }
    
    return button;
}



+(UIImage *)loadImg:(NSString *)fname{
    if (fname==nil) {
        return nil;
    }
    NSString *path = [[NSBundle mainBundle] pathForResource:[fname stringByDeletingPathExtension] ofType:[fname pathExtension]];
    
    //    if ( [[[UIDevice currentDevice] systemVersion] intValue] >=4 && [[UIScreen mainScreen] scale] == 2.0 ) {
    NSString *path2x = [[path stringByDeletingLastPathComponent]
                        stringByAppendingPathComponent:[NSString stringWithFormat:@"%@@2x.%@",
                                                        [[path lastPathComponent] stringByDeletingPathExtension],
                                                        [path pathExtension]]];
    if (path2x==nil) {
        path2x = path = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@@2x",[fname stringByDeletingPathExtension]] ofType:[fname pathExtension]];
    }
    if ( [[NSFileManager defaultManager] fileExistsAtPath:path2x] ) {
        UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path2x]] CGImage] scale:2.0 orientation:UIImageOrientationUp];
        return img;
    }else{
        if ( [[NSFileManager defaultManager] fileExistsAtPath:path]) {
            UIImage *img= [UIImage imageWithCGImage:[[UIImage imageWithData:[NSData dataWithContentsOfFile:path]] CGImage] scale:1.0 orientation:UIImageOrientationUp];
            return img;
        }else{
            traceException(@"%@ 文件不存在",fname);
        }
    }
    return nil;
}


+(UIImage *)loadImgCache:(NSString *)fname {
    UIImage *img = [UIImage imageNamed:fname];
    if (img==nil) {
        traceException(@"%@ 文件不存在",fname);
        return nil;
    }
    return img;
}


+(NSString *)assets:(NSString *)fileName{
    return  [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
}


#pragma mark -- 工具方法
+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize
{
	CGFloat scale;
	CGSize newsize;
	
	if(thisSize.width<aSize.width && thisSize.height < aSize.height)
	{
		newsize = thisSize;
	}
	else
	{
		if(thisSize.width >= thisSize.height)
		{
			scale = aSize.width/thisSize.width;
			newsize.width = aSize.width;
			newsize.height = thisSize.height*scale;
		}
		else
		{
			scale = aSize.height/thisSize.height;
			newsize.height = aSize.height;
			newsize.width = thisSize.width*scale;
		}
	}
	return newsize;
}



//全局弹出信息窗口
-(void)alert:(NSString *)content {
    trace(@"%@",content);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:content delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    [alert show];
}

@end
