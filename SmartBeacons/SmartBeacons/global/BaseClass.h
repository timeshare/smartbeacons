//
//  BaseClass.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseClass : NSObject

@end


@interface EEView : UIView{
    BOOL isRemoveAfterAction;
}

-(void)initView;
-(void)initView:(CGRect)frame;

+(id)instance;
+(id)instanceByFrame:(CGRect)frame;
+(id)instanceByData:(id)configData;

-(void)eeRemove;
-(void)eeRemoveRight;
    
- (void) addTransition:(NSInteger)type;

@end



@interface EEButton : UIButton
@end

@interface EELabel : UILabel
@end

@interface EEScrollView : UIScrollView
@end

@interface EEImageView : UIImageView
@end
