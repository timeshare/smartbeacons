#import "RootView.h"
#import "BaseClass.h"
#import "UIView+EEExtras.h"
#import "RootController.h"


#define InvalidTrace  1
#define logLevelTrace  1 //infor:1    debug:2    Error:3
#define is_show_data_exception_log YES //设置当程序数据出现错误的时候界面弹出  错误的信息。


//
//
////在真机中是否输出日志YES NO
//#define is_open_log_in_real_machine YES  // YES:在真机上的话，NSLog会保存如文件，不在控制台输出
//
//

#ifdef InvalidTrace //----------------------------

#define trace(fmt, ...) do {                                            \
    NSLog((@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__); \
}while(0)
#  define track() NSLog(@"%s", __func__)

#else//--------------------------------------------

#define trace(fmt, ...)
#define track()

#endif//-------------------------------------------




#define traceInfor(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__];\
[[Global sharedGlobal] log:discript level:1] ;}while(0)\

#define traceDebug(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:2] ;}while(0)\

#define traceError(fmt, ...) do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:3] ;}while(0)\

#define traceException(fmt, ...) do{ \
NSArray *syms = [NSThread  callStackSymbols];\
NSString* discript = [[NSString alloc] initWithFormat:(@"<%s(%d)> " fmt), __func__, __LINE__, ##__VA_ARGS__] ;\
[[Global sharedGlobal] log:discript level:2] ;\
if ([syms count] > 1) {\
    for (int i=0;i<[syms count];i++) {\
        NSString *dis = [NSString stringWithFormat:@"<%@ %p> %@ - caller: %@ ", [self class], self, NSStringFromSelector(_cmd),[syms objectAtIndex:i]];\
        [[Global sharedGlobal] log:dis level:4] ;\
    }\
} else {\
    NSString *dis = [NSString stringWithFormat:@"<%@ %p> %@", [self class], self, NSStringFromSelector(_cmd)];\
    [[Global sharedGlobal] log:dis level:4] ;\
}\
}while(0)\

#define traceFoot() do{ \
NSString* discript = [[NSString alloc] initWithFormat:(@"%s(%d) "), __func__, __LINE__] ;\
[[Global sharedGlobal] log:discript level:3] ;}while(0)\



//是真机还是模拟器
#if TARGET_IPHONE_SIMULATOR
#define SIMULATOR 1
#elif TARGET_OS_IPHONE
#define SIMULATOR 0
#endif


//程序唤醒的时候请求网络，检测更新
//#define CheckUpdateTimeGap 86400*0.5
#define CheckUpdateTimeGap 3600


//是否是iphone5
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

//系统消息提示宏
#define EEAlert(str) [[Global sharedGlobal] alert:str]


#define DeviceW     [[UIScreen mainScreen] currentMode].size.width
#define DeviceH     [[UIScreen mainScreen] currentMode].size.height

#define StatusHeight 20
#define AppTopHeight 45
#define AppButtomHeight 45
#define App_Size    [Global sharedGlobal].AppSize
#define View_Size   [Global sharedGlobal].ViewSize

//CGSizeMake([[UIScreen mainScreen] currentMode].size.width/[[UIScreen mainScreen] scale],[[UIScreen mainScreen] currentMode].size.height/[[UIScreen mainScreen] scale]-StatusHeight)

#define SystemVersion [[[UIDevice currentDevice] systemVersion] floatValue]


#define GO(str) [[RootController sharedRootViewController] replaceView:str]



#define EE_UserInformation [NSString stringWithFormat:@"%@/Library/Caches/userinfor.plist",NSHomeDirectory()]
#define EE_UserAllConfigurators [NSString stringWithFormat:@"%@/Library/Caches/configurators.plist",NSHomeDirectory()]

#define EE_SqlitePath [NSString stringWithFormat:@"%@/Documents/appData/APPDATA.db",NSHomeDirectory()]

#define EE_homePath [NSString stringWithFormat:@"%@/Documents/appData",NSHomeDirectory()]

//Documents文件夹
#define EE_DocumentsPath [NSString stringWithFormat:@"%@/Documents",NSHomeDirectory()]


//系统配置信息文件
#define EE_ZumeSysConfig_Path [NSString stringWithFormat:@"%@/Library/Caches/Config.plist",NSHomeDirectory()]


#define ee_AppSize [[UIScreen mainScreen] bounds].size
#pragma mark - 颜色值

#define color_noraml_black         0x121212
#define color_noraml_gray         0x808080
#define color_txt_gray [UIColor colorWithRed:165*1.0/255 green:171*1.0/255 blue:174*1.0/255 alpha:1]
#define RGB(r,g,b) [UIColor colorWithRed:r*1.0/255 green:g*1.0/255 blue:b*1.0/255 alpha:1]

#define color_pageControl_circle 0x4F494C;

#define color_app_black 0x191919

#define CWhite  [UIColor whiteColor]
#define CGray   [UIColor grayColor]
#define CRed    [UIColor redColor]
#define CBlack  [UIColor blackColor]

#define R(x, y, width, height) CGRectMake(x, y, width, height)
#define ccp(x,y) CGPointMake(x,y)
#define size2Point05(size) CGPointMake(size.width*0.5,size.height*0.5)
#define NullPoint CGPointMake(-1,-1)


#define appNotice @"appnoticetag"

#define MapNotice @"mapnotice"

