//
//  BaseClass.m
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import "BaseClass.h"

@implementation BaseClass

@end


@implementation EEView
-(void)initView{
}

-(void)initView:(CGRect)frame{
}

+(id)instance{
    Class class = [self class];
    id ins = [[class alloc] init];
    [ins initView];
    return ins;
}

+(id)instanceByFrame:(CGRect)frame{
    Class class = [self class];
    id ins = [[class alloc] init];
    [ins initView:frame];
    return ins;
}

+(id)instanceByData:(id)configData{
    Class class = [self class];
    id ins = [[class alloc] init];
    return ins;
}

-(void)eeRemove{
    [self removeFromSuperview];
}

-(void)eeRemoveRight{
    isRemoveAfterAction = YES;
    [self addTransition:2];
    [self ee_viewToPosition:CGPointMake(View_Size.width, 20)];
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    track();
    if (isRemoveAfterAction) {
        isRemoveAfterAction = NO;
        [self eeRemove];
    }
}

- (void) addTransition:(NSInteger)type;

{
    CATransition *transition = [CATransition animation];
	// Animate over 3/4 of a second
	transition.duration = 0.35f;
	// using the ease in/out timing function
	transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
	
    switch (type) {
        case 0:
            // Now to set the type of transition. Since we need to choose at random, we'll setup a couple of arrays to help us.
            
            transition.type = kCATransitionFade;
            
            break;
            
        case 1:
            // Now to set the type of transition. Since we need to choose at random, we'll setup a couple of arrays to help us.
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromRight;
            
            break;
        case 2:
            
            transition.type = kCATransitionPush;
            transition.subtype = kCATransitionFromLeft;
            
            break;
        default:
            
            break;
    }
    
	// Finally, to avoid overlapping transitions we assign ourselves as the delegate for the animation and wait for the
	// -animationDidStop:finished: message. When it comes in, we will flag that we are no longer transitioning.
	transition.delegate = self;
	
	// Next add it to the containerView's layer. This will perform the transition based on how we change its contents.
	[self.layer addAnimation:transition forKey:nil];
}

@end


@implementation EEButton
@end


@implementation EELabel
@end


@implementation EEScrollView
@end

@implementation EEImageView
@end

