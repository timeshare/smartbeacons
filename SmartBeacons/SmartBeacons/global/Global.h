//
//  Global.h
//  SmartBeacons
//
//  Created by 洪湃 on 14-8-18.
//  Copyright (c) 2014年 洪湃. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseClass.h"
#import "AppView.h"

extern id tmpObject;
extern EEImageView *tmpImgView;
extern EEButton *tmpButtom;
extern EELabel *tmpLabel;
extern EEView *tmpView;

@interface Global : NSObject{
    
}

@property(nonatomic,weak)AppView *appView;

@property(nonatomic)CGSize AppSize;
@property(nonatomic)CGSize ViewSize;

+ (Global*) sharedGlobal;
+ (UIImage	  *) loadImg:(NSString*)fname;
+(UIImage *)loadImgCache:(NSString *)fname ;

+(EEView *)createViewEmpty;
+(EEView *)createViewByFrame:(CGRect)frame color:(UIColor *)col;

+(EEImageView *)createImage:(NSString *)filePath frame:(CGRect)frame;
+(EEImageView *)createImage:(NSString *)filePath point:(CGPoint)point;
+(EEImageView *)createImage:(NSString *)filePath center:(CGPoint)point;
+(EEImageView *)createImage:(NSString *)filePath fitWidth:(int)fwidth point:(CGPoint)point height:(NSNumber *)resultHeight;
+(EEImageView *)createImage:(NSString *)filePath fitSize:(CGSize)fsize center:(CGPoint)point;
    
+ (EELabel  *) createLabel:(CGRect)frame label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)size fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align;
+ (EELabel  *) createLabelFit:(CGSize)size point:(CGPoint)point label:(NSString*)text lines:(NSInteger)num fontSize:(CGFloat)fontSize fontName:(NSString*)fontName textcolor:(UIColor*)textcolor align:(NSTextAlignment)align resultSize:(CGSize *)resultSize;


+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)dName target:(id)target action:(SEL)act;
+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName target:(id)target action:(SEL)act;
+(EEButton *)createBtn:(CGRect)frame normal:(NSString *)nName down:(NSString *)nDown title:(NSString *)_title col:(UIColor *)_col size:(int)size target:(id)target  action:(SEL)act;

+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;
+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;
+(EEButton *)createFitButton:(UIEdgeInsets)contentEdge center:(CGPoint)_center original:(CGPoint)_original imageName:(NSString *)imgName imageEdge:(UIEdgeInsets)imgEdge title:(NSString *)text size:(int)_size delegate:(id)_delegate action:(SEL)selector;
-(void) log:(NSString *)s level:(int)lev;


+ (CGSize) fitSize: (CGSize)thisSize inSize: (CGSize) aSize;

+(NSString *)assets:(NSString *)fileName;

-(void)alert:(NSString *)content ;
@end


