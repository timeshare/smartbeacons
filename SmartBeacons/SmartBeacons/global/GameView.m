//
//  Created by Chen Chong on 2/28/13.
//  Copyright (c) 2013 Chen Chong. All rights reserved.
//

#import "Global.h"

@implementation UISView

@synthesize animationInterval;
@synthesize animationTimer;


- (id) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (!self) {
        return nil;
    }

    self.opaque = true;
    animationTimer = nil;

    return self;
}
+(id)instance{
    return nil;
}

- (void) animationLooper{}


// animation
- (void) startAnimation 
{
	animationTimer = [NSTimer scheduledTimerWithTimeInterval:animationInterval target:self selector:@selector(animationLooper:) userInfo:nil repeats:YES];
}

- (void) stopAnimation 
{
	[animationTimer invalidate];
    animationTimer = nil;
}


//- (void) addSubview:(UIView *)view
//{
//	[super addSubview:view];
//}
- (void) initIphoView{}
- (void) initIpadView{}

- (void) freeIphoView{}
- (void) freeIpadView{}
- (void) animationLooper:(NSTimer *)timer{};
- (void) layoutSubviews:(UIDeviceOrientation) ori{}

- (void)dealloc {
    if(animationTimer != nil)
    {
        [self stopAnimation];
    }
    [super dealloc];
}
@end

@implementation GameView



@end

